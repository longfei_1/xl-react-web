import React from 'react';
import { render, mount } from 'enzyme';
import jest from 'jest';
import toJson from 'enzyme-to-json';
import Icon, { addIcon } from '../index';
import * as defaultList from '../icons';

const defaultListKey = Object.keys(defaultList);

describe('Icon', () => {

  // 渲染所以的 Icon
  it('renders all Icons', () => {
    const wrapper = render(
      <div>
        {defaultListKey.map((item) => <Icon key={item} theme="primary" type={item}/>)}
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  // 样式案例
  it('renders all of different theme', () => {
    const primarys = ['primary', 'success', 'warning', 'danger'];
    const wrapper = render(
      <div>
        {primarys.map((item) => <Icon key={item} theme={item} type="add"/>)}
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  // 字体大小案例
  it('renders all of different fontSize', () => {
    const fontSizes = [12, 30, 24];
    const wrapper = render(
      <div>
        {fontSizes.map((item) => <Icon key={item} theme='primary' fontSize={item} type="add"/>)}
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  // 大小案例
  it('renders all of different size', () => {
    const sizes = ['sm', 'md', 'lg'];
    const wrapper = render(
      <div>
        {sizes.map((item) => <Icon key={item} theme='primary' size={item} type="add"/>)}
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  // 大小案例
  it('add icon and renders with icon', () => {
    addIcon(() => (
      <symbol id="icon-test" viewBox="0 0 1385 1024">
        <path
          d="M843.294118 561.543529l178.085647 178.085647 49.543529-49.543529L892.837647 512l178.085647-178.085647-49.543529-49.543529L843.294118 462.456471l-178.085647-178.085647-49.54353 49.543529L793.750588 512l-178.085647 178.085647 49.54353 49.543529L843.294118 561.543529zM1142.302118 90.352941C1226.511059 90.352941 1295.058824 157.906824 1295.058824 240.941176v542.117648c0 83.034353-68.517647 150.588235-152.756706 150.588235H572.235294a154.563765 154.563765 0 0 1-110.20047-46.320941L107.911529 534.166588l-0.572235-0.602353-0.572235-0.572235a29.063529 29.063529 0 0 1-8.854588-21.022118c0-5.571765 1.536-13.763765 8.854588-21.022117l0.572235-0.572236 0.572235-0.602353L462.034824 136.643765A154.503529 154.503529 0 0 1 572.235294 90.292706h570.066824V90.352941z m0-90.352941H596.600471A244.133647 244.133647 0 0 0 421.647059 73.667765L43.158588 426.827294a119.657412 119.657412 0 0 0 0 170.37553L421.647059 950.362353a244.133647 244.133647 0 0 0 174.953412 73.667765h545.701647c134.264471 0 243.109647-107.881412 243.109647-240.941177v-542.117647C1385.411765 107.881412 1276.566588 0 1142.302118 0z"></path>
      </symbol>
    ));
    const iconTest = document.body.querySelector('#icon-test');
    expect(iconTest)
      .not
      .toBeNull();
    debugger;
    const wrapper = render(
      <div>
        <Icon theme='primary' type="test"/>
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  // 大小案例
  it('renders with children', () => {
    const wrapper = render(
      <div>
        <Icon theme='primary'>
          <svg width="1em" height="1em" fill="currentColor" viewBox="0 0 32 32">
            <symbol id="icon-test2" viewBox="0 0 1385 1024">
              <path
                d="M843.294118 561.543529l178.085647 178.085647 49.543529-49.543529L892.837647 512l178.085647-178.085647-49.543529-49.543529L843.294118 462.456471l-178.085647-178.085647-49.54353 49.543529L793.750588 512l-178.085647 178.085647 49.54353 49.543529L843.294118 561.543529zM1142.302118 90.352941C1226.511059 90.352941 1295.058824 157.906824 1295.058824 240.941176v542.117648c0 83.034353-68.517647 150.588235-152.756706 150.588235H572.235294a154.563765 154.563765 0 0 1-110.20047-46.320941L107.911529 534.166588l-0.572235-0.602353-0.572235-0.572235a29.063529 29.063529 0 0 1-8.854588-21.022118c0-5.571765 1.536-13.763765 8.854588-21.022117l0.572235-0.572236 0.572235-0.602353L462.034824 136.643765A154.503529 154.503529 0 0 1 572.235294 90.292706h570.066824V90.352941z m0-90.352941H596.600471A244.133647 244.133647 0 0 0 421.647059 73.667765L43.158588 426.827294a119.657412 119.657412 0 0 0 0 170.37553L421.647059 950.362353a244.133647 244.133647 0 0 0 174.953412 73.667765h545.701647c134.264471 0 243.109647-107.881412 243.109647-240.941177v-542.117647C1385.411765 107.881412 1276.566588 0 1142.302118 0z"></path>
            </symbol>
          </svg>
        </Icon>
      </div>,
    );
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });


});

