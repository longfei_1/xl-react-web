

export default interface BasePropsType {
  type?: string;
  theme?: 'default' | 'primary' | 'success' | 'warning' | 'danger';
  size?: 'sm' | 'md' | 'lg';
  fontSize?:number;
  viewBox?: string;
}

