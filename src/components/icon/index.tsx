import React, {PureComponent, HTMLAttributes} from 'react';
import classnames from 'classnames';
import ReactDOM from 'react-dom';
import BasePropsType from './PropsType'
import * as ICONS from './icons'
// import './style'

const ICON_CACHE = new Set<React.ReactNode>()
let iconContainer: HTMLElement
export const addIcon = function (Icon: React.ReactNode): void {
  if (ICON_CACHE.has(Icon)) return
  ICON_CACHE.add(Icon)
  if (!iconContainer) {
    iconContainer = document.createElement("div");
    document.body.insertBefore(iconContainer, document.body.firstElementChild)
  }
  const container = document.createElement("div")
  container.style.height = '0px';
  container.style.overflow = "hidden"
  document.body.appendChild(container)
  const {render} = ReactDOM
  // @ts-ignore
  render(<svg><Icon/></svg>, container)
  iconContainer.appendChild(container)

}


const prefixCls = `${__PREFIX__}-icon`;

export type IconPropsType = BasePropsType & HTMLAttributes<HTMLElement>;

export default class Icon extends PureComponent<IconPropsType, {}> {
  static defaultProps = {
    theme: 'default',
    size: 'md'
  }

  constructor(props) {
    super(props)
    const {type} = props
    const Icon = ICONS[type]
    if (!ICON_CACHE.has(Icon) && Icon) addIcon(Icon)
  }

  render(): React.ReactNode {
    const {
      children,
      type,
      theme,
      size,
      className,
      style = {},
      fontSize,
      ...rest
    } = this.props
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--${type}`]: !!type,
      [`${prefixCls}--${theme}`]: !!theme,
      [`${prefixCls}--${size}`]: !!size,
    })
    if (fontSize) style.fontSize = fontSize + 'px'
    const props = {
      className: cls,
      type,
      style: {...style},
      ...rest
    }

    if (children) {
      return (
        <i {...props}>{children}</i>
      )
    } else {
      return (
        <i {...props}>
          <svg width="1em" height="1em" fill="currentColor" viewBox="0 0 32 32">
            <use xlinkHref={`#icon-${type}`}/>
          </svg>
        </i>
      )
    }
  }

}
