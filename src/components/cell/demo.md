# Cell 列表项

## 基本用法

```jsx
import { Cell } from 'wxl';

class Demo extends React.Component {
  state = {
    title: 'title'
  };
  render() {
    const { title } = this.state;
    return (
      <aside>
        <Cell title={title} />
        <Cell icon={title} title={title} />
        <Cell>test</Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 带有图标、箭头

```jsx
import { Cell, Icon } from 'wxl';

class Demo extends React.Component {
  state = {
    title: 'title'
  };
  render() {
    const { title } = this.state;
    return (
      <aside>
        <Cell title={title} icon={<Icon type="userDefault" />} />
        <Cell hasArrow title={title} icon={<Icon type="userDefault" />} />
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 列表项样式调整

```jsx
import { Cell, Icon } from 'wxl';

class Demo extends React.Component {
  state = {
    title: 'title'
  };
  render() {
    const { title } = this.state;
    return (
      <aside>
        <Cell headerNoBorder hasArrow title={'底部线展示'} icon={<Icon type="userDefault" />} />
        <Cell footerNoBorder hasArrow title={'底部线部分展示'} icon={<Icon type="userDefault" />} />
        <Cell footerNoBorder headerNoBorder hasArrow title={title} icon={<Icon type="userDefault" />} />
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 可操作列表项

```jsx
import { Cell, Icon } from 'wxl';

class Demo extends React.Component {
  state = {
    title: 'title'
  };
  render() {
    const { title } = this.state;
    return (
      <aside>
        <Cell
          onClick={() => {
            alert('点出弹框');
          }}
          hasArrow
          title={title}
          icon={<Icon type="userDefault" />}
        />
        <Cell option={<div style={{ width: 110 }}>sfa</div>} icon={<Icon type="userDefault" />} title={'滑动拖拽'} />
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性           | 类型             | 默认值 | 说明           |
| :------------- | :--------------- | :----- | :------------- |
| autoHeight     | boolean          | false  | 高度不固定     |
| hasArrow       | boolean          | false  | 是否有箭头     |
| headerNoBorder | boolean          | false  | icon 底部线    |
| footerNoBorder | boolean          | false  | icon 底部线    |
| icon           | component/icon   | -      | 前面部分       |
| title          | component/string | -      | 中间前面       |
| option         | component        | -      | 可操作组件部分 |
