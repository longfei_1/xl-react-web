import React, { PureComponent, HTMLAttributes } from 'react';
import classnames from 'classnames';
import PropsType from './PropsType';
import Icon from '../icon';
import Util from '../util';

export type HTMLDivProps = Omit<HTMLAttributes<HTMLDivElement>, 'title'>;

const prefixCls = `${__PREFIX__}-cell`;

export interface CellProps extends HTMLDivProps, PropsType {
  className?: string;
}

export default class Cell extends PureComponent<CellProps, {}> {
  public inner;
  public extra;
  public MOVE_ING;
  public TouchInfo: any;
  static defaultProps = {
    hasArrow: false,
    headerNoBorder: false,
    footerNoBorder: false,
    autoHeight: false,
    option: null,
    disabled: false
  };

  start = e => {
    if(!this.props.option) return
    this.TouchInfo = {};
    let touch = e.touches && e.touches[0];
    if(!touch) touch = e
    const { pageX, pageY } = touch;
    const maxMove = this.extra.offsetWidth;
    this.TouchInfo = {
      sX: pageX,
      maxMove,
      move: null,
      sY: pageY
    };
    this.MOVE_ING = true
  };
  move = e => {
    if(!this.props.option || !this.MOVE_ING) return
    let touch = e.touches && e.touches[0];
    if(!touch) touch = e
    const { sX, sY, maxMove, move } = this.TouchInfo;
    const { pageX, pageY } = touch;
    let mx = pageX - sX;
    let my = pageY - sY;
    let slideX: any;
    if ((Math.abs(mx) > Math.abs(my) && move === null) || move) {
      this.TouchInfo.move = true;
      slideX = Util.getTranslateInfo(this.inner, 1);
      slideX = slideX + mx;
      if (slideX < -maxMove) slideX = -maxMove;
      else if (slideX > 0) slideX = 0;
      Util.setTranslateInfo(this.inner, {
        x: slideX
      });
      document.body.classList.add(`${prefixCls}__ovhide`);
    } else {
      this.TouchInfo.move = false;
    }
    this.TouchInfo = {
      ...this.TouchInfo,
      ...{
        sX: pageX,
        sY: pageY
      }
    };
  };
  end = () => {
    if(!this.props.option) return
    document.body.classList.remove(`${prefixCls}__ovhide`);
    const { maxMove } = this.TouchInfo;
    let slideX = Util.getTranslateInfo(this.inner, 1);
    if (slideX < -maxMove / 2) slideX = -maxMove;
    else slideX = 0;
    this.TouchInfo.move = null;
    if (slideX != 0) {
      const resetDom = e => {
        if (!Util.checkInParent(e.target, this.inner)) {
          Util.setTranslateInfo(this.inner, {
            x: 0,
            time: 150
          });
        }
        document.body.removeEventListener('touchstart', resetDom);
      };
      document.body.addEventListener('touchstart', resetDom);
    }
    Util.setTranslateInfo(this.inner, {
      x: slideX,
      time: 150
    });
    this.MOVE_ING = false
  };

  render(): React.ReactNode {
    const {
      children,
      className,
      hasArrow,
      icon,
      title,
      headerNoBorder,
      footerNoBorder,
      autoHeight,
      disabled,
      onClick,
      option,
      ...rest
    } = this.props;

    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--link`]: !disabled && !!onClick,
      [`${prefixCls}--arrow`]: hasArrow,
      [`${prefixCls}--hnb`]: !headerNoBorder,
      [`${prefixCls}--f-n-b`]: footerNoBorder
    });

    const titleCls = classnames(`${prefixCls}__title`, {
      [`${prefixCls}__title--label`]: !!children
    });
    const headerCls = classnames(`${prefixCls}__header`, {
      [`${prefixCls}__header--nb`]: !headerNoBorder
    });
    const footerCls = classnames(`${prefixCls}__footer`, {
      [`${prefixCls}__footer--nb`]: footerNoBorder
    });
    const innerCls = classnames(`${prefixCls}__inner`, {
      [`${prefixCls}__inner--autoHeight`]: autoHeight,
    });
    const iconRender = icon && <div className={`${prefixCls}__icon`}>{icon}</div>;
    const titleRender = title && <div className={titleCls}>{title}</div>;
    const contentRender = children && <div className={`${prefixCls}__content`}>{children}</div>;
    const arrowRender = hasArrow && <Icon type={'arrowDownOneLeft'} />;
    return (
      <div className={cls} onClick={onClick} {...rest}>
        <div
          className={innerCls}
          ref={dom => {
            this.inner = dom;
          }}
          onTouchStart={this.start}
          onMouseDown={this.start}
          onTouchMove={this.move}
          onMouseMove={this.move}
          onTouchEnd={this.end}
          onMouseUp={this.end}
        >
          <div className={headerCls}>{iconRender}</div>
          <div className={`${prefixCls}__body`}>
            {titleRender}
            {contentRender}
          </div>
          <div className={footerCls}>{arrowRender}</div>
          {option && (
            <div ref={ref => (this.extra = ref)} className={`${prefixCls}__option`}>
              {option}
            </div>
          )}
        </div>
      </div>
    );
  }
}
