import React from 'react';
import { render, mount } from 'enzyme';
import jest from 'jest';
import toJson from 'enzyme-to-json';
import Cell from '../index';
import Icon from '../../icon';



describe('Cell', () => {
  // Cell 常规展示
  it('renders  Cell', () => {
    const wrapper = render(
      <div>
        <Cell title={'text'} />
      </div>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  // Cell 带图标合箭头的
  it('renders  Icons with icon arrow children', () => {
    const wrapper = render(
      <div>
        <Cell title={'text'} hasArrow icon={<Icon type="wrong" />} />
        <Cell hasArrow icon={<Icon type="wrong" />}>
          test2
        </Cell>
      </div>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  // Cell 带图标合箭头的,border 线显示
  it('renders  Icons with icon headerNoBorder footerNoBorder', () => {
    const wrapper = render(
      <div>
        <Cell title={'text'} headerNoBorder footerNoBorder hasArrow icon={<Icon type="wrong" />} />
      </div>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  it('renders  Icons with icon headerNoBorder footerNoBorder', () => {
    const wrapper = render(
      <div>
        <Cell title={'text'} headerNoBorder footerNoBorder option={(<div style={{width:110}}>sfa</div>)} />
      </div>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

});

