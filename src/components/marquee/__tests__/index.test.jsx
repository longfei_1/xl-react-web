import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Marquee from '../index';
import { getVertical, setAnimation, getHorizontal } from '../utils';

const prefixCls = `${__PREFIX__}-marquee`;

describe('Marquee', () => {
  // Marquee 常规展示
  it('renders  Marquee', () => {
    const wrapper = render(
      <div>
        <Marquee delay={300} style={{height:150,width:'100%'}} loop={false}>
          test
        </Marquee>
        <Marquee direction={'right'} loop={true} className={'test'}>
          <div>test 123test 123</div>
          <div>test 123test 123</div>
          <div>test 123test 123</div>
        </Marquee>
        <Marquee direction={'down'} height={500}>
          test 123 test 123 test 123 test 123 test 123
        </Marquee>
        <Marquee direction={'up'} width={500} type={'slide'}>
          <p>testtest</p>
          <p>testtest</p>
          <p>testtest</p>
        </Marquee>
      </div>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('utils of  Marquee', () => {
    const key = `${prefixCls}--key-${new Date().getTime()}`;
    const horizontal = getHorizontal(100, key);
    expect(horizontal).toEqual(`
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(100px, 0, 0);
      transform: translate3d(100px, 0, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(100px, 0, 0);
      transform: translate3d(100px, 0, 0);
    }
  }`);
    const vertical = getVertical(10, key);
    expect(vertical).toEqual(`
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, 10px, 0);
      transform: translate3d(0, 10px, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, 10px, 0);
      transform: translate3d(0, 10px, 0);
    }
  }`);
    const animation = setAnimation(10, false, 300, key);
    expect(animation).toEqual(`10ms ${key} 300ms linear 1`);
    const animation2 = setAnimation(50, true, 300, key);
    expect(animation2).toEqual(`50ms ${key} 300ms linear infinite`);
  });

  it('click of  Marquee', () => {
    const onClick = jest.fn();
    const wrapper = mount(
      <Marquee onClick={onClick}>test</Marquee>
    );
    wrapper.simulate('click');
    expect(onClick)
      .toBeCalled();
  });
});
