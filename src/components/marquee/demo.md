# Marquee 跑马灯

## 基本用法

```jsx
import { Marquee, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell>
          <Marquee hover direction={'left'}>
            <div style={{ lineHeight: '40px' }}>这是一个向左的滑动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
            <div style={{ lineHeight: '40px' }}>自动滚动</div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'right'}>
            <div style={{ lineHeight: '40px' }}>这是一个向右的滑动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'up'} style={{ height: '40px' }}>
            <div style={{ lineHeight: '40px' }}>这是一个向上的滑动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'down'} style={{ height: '40px' }}>
            <div style={{ lineHeight: '40px' }}>这是一个向下的滑动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
          </Marquee>
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 延时、速度、循环

```jsx
import { Marquee, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell>
          <Marquee direction={'left'} delay={2000}>
            <div style={{ lineHeight: '40px' }}>
              这是一个向左的滑动，里面可以是多个标签，标签都设置为行内块元素，自动滚动
            </div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'left'} speed={100} delay={1000}>
            <div style={{ lineHeight: '40px' }}>
              这个速度比较快，里面可以是多个标签，标签都设置为行内块元素，自动滚动
            </div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'left'} delay={1000} loop={false}>
            <div style={{ lineHeight: '40px' }}>
              这个速度比较快，里面可以是多个标签，标签都设置为行内块元素，自动滚动
            </div>
          </Marquee>
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 上下滚动效果（滚动间隔时间、滚动时间）

```jsx
import { Marquee, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell>
          <Marquee direction={'up'} style={{ height: '40px' }} type={'slide'}>
            <div style={{ lineHeight: '40px' }}>这是一个向上的滑动，</div>
            <div style={{ lineHeight: '40px' }}>这个效果为滚动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'up'} loop={6000} style={{ height: '40px' }} type={'slide'}>
            <div style={{ lineHeight: '40px' }}>这是一个向上的滑动，</div>
            <div style={{ lineHeight: '40px' }}>这个效果为滚动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
          </Marquee>
        </Cell>
        <Cell>
          <Marquee direction={'up'} loop={2000} speed={1000} style={{ height: '40px' }} type={'slide'}>
            <div style={{ lineHeight: '40px' }}>这是一个向上的滑动，</div>
            <div style={{ lineHeight: '40px' }}>这个效果为滚动，</div>
            <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
            <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
          </Marquee>
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性      | 类型           | 默认值    | 说明                        |
| :-------- | :------------- | :-------- | :-------------------------- |
| direction | string         | left      | 滑动方向 left/right/down/up |
| type      | string         | scroll    | 滑动或者滚动 scroll/slide   |
| delay     | number         | 100       | 延时时间                    |
| loop      | boolean/number | true/3000 | 是否循环/切换的间隔时间     |
| hover     | boolean        | false     | hover 停止滚动              |
| speed     | number         | 60/300    | 滑动速度/切换速度           |
| onClick   | -              | -         | 点击事件                    |
| style     | -              | -         | 组件样式                    |
