
export const getHorizontal = (distance: number, key: string) => {
  return `
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(${distance}px, 0, 0);
      transform: translate3d(${distance}px, 0, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(${distance}px, 0, 0);
      transform: translate3d(${distance}px, 0, 0);
    }
  }`;
};
export const getVertical = (distance: number, key: string) => {
  return `
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, ${distance}px, 0);
      transform: translate3d(0, ${distance}px, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, ${distance}px, 0);
      transform: translate3d(0, ${distance}px, 0);
    }
  }`;
};
export const setAnimation = (duration: number, loop: boolean, delay: number, key: string) => {
  const infinite = loop ? 'infinite' : 1;
  return `${duration}ms ${key} ${delay}ms linear ${infinite}`;
};


