import * as scroll from './scroll';
import * as device from './device';
import * as date from './date';
import * as number from './number';
const IS_IN_JEST = global && global.process;
const { IS_IOS } = device;
const { getScrollTop, getScrollEventTarget, preventDefaultStopPropagation } = scroll;

const getVendorPrefix = () => {
  var body = document.body || document.documentElement;

  var style = body.style;

  var vendor = ['webkit', 'khtml', 'moz', 'ms', 'o'];

  var i = 0;
  while (i < vendor.length) {
    if (typeof style[vendor[i] + 'Transition'] === 'string') {
      return vendor[i];
    }
    i++;
  }
};
const prefix = getVendorPrefix();
const getTranslateInfo = (dom, type) => {
  const t = dom.style[`${prefix}Transform`];
  const reg = /translate([X|Y])?\((.+)?\)/;
  const rst = reg.exec(t);
  let x = 0;
  let y = 0;
  if (rst) {
    if (rst[1] === 'X') x = parseFloat(rst[2]);
    if (rst[1] === 'Y') y = parseFloat(rst[2]);
    if (!rst[1]) {
      const xy = rst[2].split(',');
      x = parseFloat(xy[0]);
      y = parseFloat(xy[1]);
    }
  }
  if (!type) return { x, y };
  else if (type === 1) return x;
  else return y;
};
const setTranslateInfo = (dom, option) => {
  const { x = 0, y = 0, time = null, type = 'linear' } = option;
  if (time !== null) {
    dom.style[`${prefix}Transition`] = `all ${time / 1000}s ${type}`;
    setTimeout(() => {
      dom.style[`${prefix}Transition`] = `all 0s`;
    }, time);
  }
  dom.style[`${prefix}Transform`] = `translate(${x}px,${y}px)`;
};
const checkInParent = (son, parent) => {
  if (son == parent) return true;
  else {
    if (son.parentNode) {
      return checkInParent(son.parentNode, parent);
    } else return false;
  }
};

const isArray = input => {
  return Object.prototype.toString.apply(input) === '[object array]';
};
let globalInfo: any = {};
const getBodyWidth = (type = false) => {
  if (globalInfo.width && type) return globalInfo.width;
  const width = document.body.clientWidth;
  globalInfo = { ...globalInfo, ...{ width } };
  return width;
};
const getBodyHeight = (type = false) => {
  if (globalInfo.height && !type) return globalInfo.height;
  const height = document.body.clientHeight;
  globalInfo = { ...globalInfo, ...{ height } };
  return height;
};

const PADDING = 5;
const isInClient = (pageX, pageY) => {
  const bodyHeight = getBodyHeight(true);
  const bodyWidth = getBodyWidth(true);
  if (IS_IN_JEST) return true;
  if (pageX < PADDING || pageX > bodyWidth - PADDING || pageY < PADDING || pageY > bodyHeight - PADDING) {
    return false;
  }
  return true;
};

const TOUCH_EVENT_NAME = (() => {
  if ('ontouchstart' in window || IS_IN_JEST) {
    return {
      start: 'touchstart',
      move: 'touchmove',
      end: 'touchend',
      option: { passive: false }
    };
  } else {
    return {
      start: 'mousedown',
      move: 'mousemove',
      end: 'mouseup',
      option: {}
    };
  }
})();
const PREFIX = prefix ? `${prefix.substr(0, 1).toUpperCase()}${prefix.substring(1)}` : '';

const disableIosBounce = element => {
  let startY: any;
  let scoller: any;
  if (!IS_IOS) return () => {};
  document.addEventListener('touchstart', start, { passive: false });
  document.addEventListener('touchmove', move, { passive: false });
  function start(e) {
    let touch = e.touches && e.touches[0];
    const { pageY } = touch;
    startY = pageY;
    scoller = null;
  }
  function move(e) {
    const { touches, target } = e || {};
    let touch = touches && touches[0];
    if (!scoller) {
      scoller = target && getScrollEventTarget(target, element);
    }
    const scrollTop = getScrollTop(scoller);
    const { pageY } = touch;
    const { scrollHeight, offsetHeight } = scoller;
    if (
      scrollHeight <= offsetHeight ||
      (scrollTop <= 0 && pageY - startY >= 0) ||
      (scrollHeight - offsetHeight <= scrollTop && pageY - startY <= 0) ||
      scoller === element
    ) {
      preventDefaultStopPropagation(e);
    }
    startY = pageY;
  }
  return () => {
    // @ts-ignore
    document.removeEventListener('touchstart', start, { passive: false });
    // @ts-ignore
    document.removeEventListener('touchmove', move, { passive: false });
  };
};

export default {
  ...{ scroll },
  ...{ device },
  ...{ date },
  // ...{ number },
  TOUCH_EVENT_NAME,
  IS_IN_JEST,
  disableIosBounce,
  number,
  getVendorPrefix,
  transform: prefix ? `${PREFIX}Transform` : 'transform',
  transition: prefix ? `${PREFIX}Transition` : 'transition',
  setTranslateInfo,
  checkInParent,
  isArray,
  getBodyWidth,
  getBodyHeight,
  isInClient,
  getTranslateInfo
};
