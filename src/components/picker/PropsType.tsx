export default interface PropsType {
  className?: string;
  data: any[];
  value?: any;
  keyValue?: any;
  keyName: string;
  renderKey: string;
  disable?: boolean;
  preventDefault?: boolean;
  onChange?: (item: any) => {};
}
