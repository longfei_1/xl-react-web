export default interface PropsType {
  direction: 'left' | 'right';
  height?: number | string;
  className?: string;
  activeIndex: number;
  loop?: boolean;
  bounce?: boolean;
  touchSlide?: boolean;
  overflow?: boolean;
  preventDefault?: boolean;
  able?: boolean;
  autoPlay?: boolean;
  autoPlayIntervalTime?: number;
  animationDuration?: number;
  gap?: number;
  gapPadding?: number;
  showPagination?: boolean;
  onChange?: (e: any) => {};
  onChangeEnd?: (e: any) => {};
}
