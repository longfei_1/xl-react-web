import React, { PureComponent } from 'react';
import classnames from 'classnames';
import PropsType from './PropsType';

import Util from '../util';

const prefixCls = `${__PREFIX__}-swiper`;
let children: any = null;

export default class Swiper extends PureComponent<PropsType, any> {
  public contain;
  public pagination;
  public MOVE_ING;
  public MOVE_ING_END;
  public disableIosBounce: any;
  public MOVE_ING_OUT_END;
  public TouchInfo: any;
  public startInfo: any;
  public width: any;
  public length: any;
  public loop: any;
  static defaultProps = {
    direction: 'left',
    activeIndex: 1,
    loop: true,
    bounce: true,
    able: true,
    overflow: false,
    preventDefault: false,
    autoPlay: false,
    touchSlide: true,
    showPagination: false,
    gap: 0,
    gapPadding: 0,
    autoPlayIntervalTime: 4000,
    animationDuration: 300
  };

  constructor(props) {
    super(props);
    this.state = {
      children,
      style: {},
      activeIndex: 1
    };
  }

  componentDidMount() {
    this.getChildren();
  }
  componentDidUpdate(prevProps) {
    if ((this.props, prevProps !== this.props)) this.getChildren();
  }

  componentWillUnmount() {
    this.disableIosBounce && this.disableIosBounce();
    this.loop && clearInterval(this.loop);
  }

  getChildren() {
    let { children, activeIndex, loop, gap, gapPadding } = this.props;
    let { style } = this.state;
    if (children && Array.isArray(children))
      children = children.filter(item => {
        if (item) return item;
      });
    if (children && Array.isArray(children) && children.length > 1) {
      const firstChildren = children[0];
      const listChildren = children[children.length - 1];
      loop && (children = [listChildren, ...children, firstChildren]);
      const width = this.contain.parentNode.offsetWidth + gap;
      const padding = gap || 0;
      const paddingGap = gapPadding || 0;
      this.width = width;
      // @ts-ignore
      const length = (this.length = children.length);
      children = React.Children.map(children, (item, index) => {
        // @ts-ignore
        const props = { ...item.props };
        if (!props.style) props.style = {};
        if (padding > 0) {
          if (!loop && index === 0) props.style.marginLeft = -paddingGap;
          if (!loop && index === this.length - 1) props.style.marginRight = -paddingGap;

          return React.createElement(
            'div',
            {
              style: {
                width,
                paddingRight: padding
              }
            },
            item
          );
        }
        props.style.width = width;
        // @ts-ignore
        return React.cloneElement(item, props);
      });
      if (!loop) activeIndex = activeIndex - 1;
      Util.setTranslateInfo(this.contain, {
        x: -width * activeIndex
      });
      style = {
        ...style,
        ...{
          width: length * width
        }
      };
    }
    this.setState({
      children,
      activeIndex,
      style
    });
    this.autoPlay();
  }
  autoPlay() {
    const { autoPlayIntervalTime, autoPlay, loop, animationDuration, direction } = this.props;
    if (autoPlay && loop && this.length > 1) {
      this.loop && clearInterval(this.loop);
      this.loop = setInterval(() => {
        if (this.MOVE_ING) return;
        let { activeIndex } = this.state;
        if (direction === 'right') activeIndex -= 1;
        else activeIndex += 1;
        this.moveTo(activeIndex, animationDuration);
      }, autoPlayIntervalTime);
    }
  }
  start = e => {
    const { able } = this.props;
    if (!able) return;
    this.TouchInfo = {};
    let touch = e.touches && e.touches[0];
    if (!touch) touch = e;
    const { pageX, pageY } = touch;
    this.startInfo = {
      sX0: pageX,
      sY0: pageY
    };
    this.TouchInfo = {
      sX: pageX,
      move: null,
      sY: pageY
    };
    this.MOVE_ING = true;
    this.MOVE_ING_OUT_END = false;
    this.disableIosBounce && this.disableIosBounce();
  };
  move = e => {
    const { preventDefault, able, loop, bounce, touchSlide } = this.props;

    if (!this.MOVE_ING || this.MOVE_ING_END || !able) return;
    let touch = e.touches && e.touches[0];
    if (!touch) touch = e;
    const { sX, sY, move } = this.TouchInfo;
    const { pageX, pageY, clientY } = touch;
    const inBody = Util.isInClient(pageX, clientY);
    if (!inBody) {
      this.end();
      this.MOVE_ING_OUT_END = true;
      return;
    }
    let mx = pageX - sX;
    let my = pageY - sY;
    let slideX: any;
    if (touchSlide) {
      slideX = Util.getTranslateInfo(this.contain, 1);
      if (!loop) {
        const max = this.width / 3;
        if (slideX > 0) mx = ((max - slideX) * mx) / max;
        if (slideX < -(this.length - 1) * this.width) mx = (((this.length - 1) * this.width + max + slideX) * mx) / max;
      }
      slideX = slideX + mx;
      if (!loop && !bounce) {
        if (slideX > 0) slideX = 0;
        if (slideX < -(this.length - 1) * this.width) slideX = -(this.length - 1) * this.width;
      }
      if (preventDefault) {
        if ((Math.abs(mx) > Math.abs(my) && move === null) || move) {
          this.TouchInfo.move = true;
          Util.setTranslateInfo(this.contain, {
            x: slideX
          });
          this.disableIosBounce = Util.disableIosBounce(this.contain);
          document.body.classList.add(`${prefixCls}__ovhide`);
        } else {
          this.TouchInfo.move = false;
        }
      } else {
        Util.setTranslateInfo(this.contain, {
          x: slideX
        });
      }
    }
    this.TouchInfo = {
      ...this.TouchInfo,
      ...{
        sX: pageX,
        sY: pageY
      }
    };
  };
  end = () => {
    const { able, loop } = this.props;
    if (!able || this.MOVE_ING_OUT_END) return;
    const { sX } = this.TouchInfo;
    const { sX0 } = this.startInfo;
    let { activeIndex } = this.state;
    this.MOVE_ING_END = true;
    if (Math.abs((sX - sX0) / this.width) > 0.33) {
      if (sX - sX0 < 0) activeIndex++;
      else activeIndex--;
      if (!loop) {
        if (activeIndex < 0) activeIndex = 0;
        if (activeIndex > this.length - 1) activeIndex = this.length - 1;
      }
    }
    this.disableIosBounce && this.disableIosBounce();
    document.body.classList.remove(`${prefixCls}__ovhide`);
    this.moveTo(activeIndex);
  };

  moveTo(activeIndex, time = 150) {
    const { loop, touchSlide, onChange } = this.props;
    Util.setTranslateInfo(this.contain, {
      x: -this.width * activeIndex,
      time: touchSlide ? time : 0
    });
    this.setState({ activeIndex });

    if ((activeIndex === 0 || activeIndex === this.length - 1) && loop) {
      setTimeout(() => {
        if (activeIndex === 0) activeIndex = this.length - 2;
        else activeIndex = 1;
        Util.setTranslateInfo(this.contain, {
          x: -this.width * activeIndex
        });
        onChange && onChange(activeIndex);
        this.setState({ activeIndex });
      }, time + 50);
    } else {
      onChange && onChange(activeIndex);
    }
    setTimeout(() => {
      this.MOVE_ING = false;
      this.MOVE_ING_END = false;
    }, time);
  }

  render() {
    const { className, direction, showPagination, loop, overflow } = this.props;
    let { children, activeIndex, style } = this.state;
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--overshow`]: overflow
    });

    const contain = classnames(`${prefixCls}__contain`, `${prefixCls}__contain--${direction}`);
    let dots = [];
    let dotIndex = activeIndex;
    if (children && children.slice) {
      if (loop) {
        dots = children.slice(1, children.length - 1);
        if (dotIndex < 1) dotIndex = 1;
        if (dotIndex > dots.length) dotIndex = dots.length;
      } else {
        dots = children;
      }
    }
    return (
      <div className={cls}>
        <div
          className={contain}
          style={style}
          ref={dom => {
            this.contain = dom;
          }}
          onTouchStart={this.start}
          onMouseDown={this.start}
          onTouchMove={this.move}
          onMouseMove={this.move}
          onTouchEnd={this.end}
          onMouseUp={this.end}
        >
          {children}
        </div>
        {showPagination && dots.length > 0 && (
          <div ref={ref => (this.pagination = ref)} className={`${prefixCls}__pagination`}>
            {dots.map((item, index) => {
              const { key } = item;
              return (
                <p
                  key={key}
                  onClick={() => {
                    this.moveTo(index + 1);
                  }}
                  className={classnames(`${prefixCls}__dot`, {
                    [`${prefixCls}__dot--active`]: dotIndex === index + (loop ? 1 : 0)
                  })}
                ></p>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}
