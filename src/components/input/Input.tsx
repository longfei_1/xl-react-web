import React, {PureComponent} from 'react';
import {InputBase, TextBase, NumberBase} from './PropsType'
import Base from './Base'
import Text from './Text'
import Number from './Number'

export type InputProps = InputBase | NumberBase | TextBase

export default class Input extends PureComponent<InputProps, {}> {
  static defaultProps = {
    type: 'text',
  };
  private input;

  focus() {
    if (this.input) {
      this.input.focus();
    }
  }

  blur() {
    if (this.input) {
      this.input.blur();
    }
  }

  render() {
    const {type} = this.props;
    switch (type) {
      case 'price':
      case 'number':
      case 'card':
        return (
          <Number
            {...this.props as InputProps}
            type={type}
            ref={(ele) => {
              this.input = ele;
            }}
          />
        );
      case 'textarea':
        return (
          <Text
            {...this.props as InputProps}
            type={type}
            ref={(ele) => {
              this.input = ele;
            }}
          />
        )
      default :
        return (
          <Base
            {...this.props as InputProps}
            type={type}
            ref={(ele) => {
              this.input = ele;
            }}
          />
        )
    }
  }

}
