import React, {PureComponent} from 'react'
import reactDom from 'react-dom'
import classnames from 'classnames'
import {NumberBase} from './PropsType'
import Icon from "../icon";

const prefixCls = `${__PREFIX__}-input`;


export default class Number extends PureComponent<NumberBase, any> {
  static defaultProps = {
    type: 'Number',
    clearable: false,
  }
  private input;
  private timeOut = false;
  private modalContainer;

  constructor(props) {
    super(props)
    let value = props.defaultValue || props.value
    if (props.type === 'card') {
      value = value.replace(/[^\d|x]/g, "")
    } else {
      if (props.type === 'number') value = value.replace(/\./g, "")
      value = parseFloat(value)
      if (isNaN(value)) value = ''
    }
    value += ''
    this.state = {
      value,
      focused: false,
    }
    document.getElementsByTagName("html")[0].classList.add(`${prefixCls}__common-input-document`);
  }

  componentWillUnmount() {
    document.getElementsByTagName("html")[0].classList.remove(`${prefixCls}__common-input-document`);
    this.close()
  }

  componentDidMount() {
    const {props} = this
    const {
      autoFocus,
    } = props
    if (autoFocus) {
      this.onClick(null)
    }
  }

  get value() {
    const {value} = this.state
    return value
  }


  onClick = (e) => {
    const {onFocus, onBlur, onPaste, showKeyBox} = this
    e && e.preventDefault()
    e && e.stopPropagation()
    if (this.timeOut) return;
    this.timeOut = true
    setTimeout(() => {
      this.timeOut = false
    }, 250)
    const {focused} = this.state
    if (focused) return
    onFocus(e)
    showKeyBox()
    document.addEventListener('click', onBlur)
    document.addEventListener('paste', onPaste)
  };
  onPaste = (e) => {
    const {onPaste} = this.props
    let {value} = this.state
    e.preventDefault()
    e.stopPropagation()
    if (!(e.clipboardData && e.clipboardData.items)) return;
    const {items = []} = e.clipboardData
    Object.keys(items).forEach(key => {
      const item = items[key];
      if (item.kind === 'string' && item.type === "text/plain") {
        item.getAsString((str) => {
          if (onPaste) str = onPaste(str)
          value = value + str
          this.setState({
            value
          })
        })
      }
    })
  }

  showKeyBox = () => {
    const modalContainer = document.createElement('div');
    this.modalContainer = modalContainer
    modalContainer.setAttribute('class', `${prefixCls}__types-key-box`);
    document.body.appendChild(modalContainer)
    const _this = this
    const {type} = this.props
    const Com = (
      <div className={`${prefixCls}__key-box`}>
        <p onClick={(e) => _this.onKeyUp('', e)}><span>-</span></p>
        <p onClick={(e) => _this.onKeyUp('1', e)}><span>1</span></p>
        <p onClick={(e) => _this.onKeyUp('2', e)}><span>2</span></p>
        <p onClick={(e) => _this.onKeyUp('3', e)}><span>3</span></p>
        <p onClick={(e) => _this.onKeyUp('del', e)}>
          <Icon type='deletekey' fontSize={20}/>
        </p>
        <p onClick={(e) => _this.onKeyUp('', e)}><span>@</span></p>
        <p onClick={(e) => _this.onKeyUp('4', e)}><span>4</span></p>
        <p onClick={(e) => _this.onKeyUp('5', e)}><span>5</span></p>
        <p onClick={(e) => _this.onKeyUp('6', e)}><span>6</span></p>
        <p onClick={(e) => _this.onKeyUp('.', e)}><span style={{fontSize: 20}}>.</span></p>
        <p onClick={(e) => _this.onKeyUp('', e)}><span>#</span></p>
        <p onClick={(e) => _this.onKeyUp('7', e)}><span>7</span></p>
        <p onClick={(e) => _this.onKeyUp('8', e)}><span>8</span></p>
        <p onClick={(e) => _this.onKeyUp('9', e)}><span>9</span></p>
        <p onClick={(e) => _this.onKeyUp('enter', e)}><span>跳转</span></p>
        <p onClick={(e) => _this.onKeyUp('', e)}><span>?!$</span></p>
        <p onClick={(e) => _this.onKeyUp(type === 'card' ? 'x' : '', e)}><span>{type === 'card' ? 'x' : ''}</span></p>
        <p onClick={(e) => _this.onKeyUp('0', e)}><span>0</span></p>
        <p onClick={(e) => _this.onKeyUp('blur', e)}><Icon type='keyboard' fontSize={20}/>
        </p>
      </div>
    )
    reactDom.render(Com, modalContainer, () => {
      setTimeout(() => modalContainer.classList.add("action"), 50)
    })
  }
  close = () => {
    const {modalContainer} = this
    modalContainer.classList.remove('action');
    setTimeout(() => {
      reactDom.unmountComponentAtNode(modalContainer)
      modalContainer.parentNode && modalContainer.parentNode.removeChild(modalContainer)
    }, 250)
  }
  onChange = (value) => {
    const {onChange, proxy} = this.props
    if (proxy) {
      value = proxy(value)
    }
    this.setState({
      value
    })
    if (onChange) onChange(value)
  }
  onEnter = (e) => {
    const {onEnter} = this.props
    this.onBlur(e)
    if (onEnter && typeof onEnter === 'function') onEnter(this.value)
  }
  onKeyUp = (key, e) => {
    e.nativeEvent.stopImmediatePropagation();
    e.preventDefault()
    e.stopPropagation()
    if (!key) return
    const {onKeyUp, type = 'price'} = this.props
    let {value} = this.state
    switch (key) {
      case 'del':
        if (value.length > 0) value = value.substring(0, value.length - 1)
        break;
      case 'enter':
        this.onEnter(e)
        break;
      case 'blur':
        this.onBlur(e)
        break;
      case '.':
        if (type === 'price' && value.indexOf('.') === -1 && value.length > 0) value += key
        break;
      case 'x':
        if (type === 'card' && value.indexOf('x') === -1 && value.length > 0) value += key
        break;
      default:
        if (!length || length > value.length) value = `${value}${key}`
        value = value.replace(/^0+/, 0)
        value = value.replace(/^0(\d)/, '$1')
        break;
    }
    if (onKeyUp && typeof onKeyUp === 'function') onKeyUp(key)
    this.onChange(value)
  }
  onBlur = (e) => {
    const {onBlur} = this.props
    const {target} = e
    this.setState({
      focused: false
    })
    document.removeEventListener('click', this.onBlur)
    document.removeEventListener('paste', this.onPaste)
    this.close()
    document.getElementsByTagName("html")[0].style.top = 0 + 'px';
    if (onBlur) onBlur(this.value, target)
  }
  onFocus = (e) => {
    const {onFocus} = this.props
    const {target = this.input} = e || {}
    const Rect = target.getBoundingClientRect()
    const {y, height} = Rect
    const top = (window.innerHeight - 200) - y - height
    if (top < 0) document.getElementsByTagName("html")[0].style.top = `${top - 15}px`;
    this.setState({
      focused: true
    })
    if (onFocus && typeof onFocus === 'function') onFocus(this.value, target)
  }

  onClear = (e) => {
    e.nativeEvent && e.nativeEvent.stopImmediatePropagation && e.nativeEvent.stopImmediatePropagation();
    e.preventDefault()
    e.stopPropagation()
    this.setState({
      value: ''
    })
  }


  render(): React.ReactNode {
    const {
      className,
      clearable,
      // ...rest
    } = this.props
    const {focused, value} = this.state
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--clearable`]: clearable,
    })
    const IconClass = classnames(`${prefixCls}__clear`, `${prefixCls}__clear--textarea`, {
      [`${prefixCls}__clear--show`]: focused && value && value.length > 0,
    })
    const placeHolder = classnames(`${prefixCls}__value`, {
      [`${prefixCls}__value--hide`]: !focused,
    })
    return (
      <div className={cls}>
        <p
          className={placeHolder}
          ref={(input) => this.input = input}
          onClick={(e) => this.onClick(e)}
        >
          <span>{value}</span>
        </p>
        {clearable && <Icon type='wrongRoundFill' className={IconClass} onClick={(e) => {
          this.onClear(e)
        }}/>}
      </div>
    )
  }

}
