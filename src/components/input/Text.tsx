import React, {PureComponent} from 'react'
import classnames from 'classnames'
import {TextBase} from './PropsType'
import Icon from "../icon";

const prefixCls = `${__PREFIX__}-input`;

const regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]|\n/g;

const countSymbols = (text = '') => {
  return text.replace(regexAstralSymbols, '_').length;

};
export default class Text extends PureComponent<TextBase, any> {
  static defaultProps = {
    type: 'text',
    rows: 3,
    clearable: false,
    readOnly: false,
    disabled: false,
  }
  private input;

  constructor(props) {
    super(props)
    this.state = {
      value: props.defaultValue || props.value || '',
      focused: false,
      lineHeight: null,
    }
  }

  componentDidMount() {
    const {props, input} = this
    const {
      autoFocus,
      autoHeight,
      onFocus,
      maxLength,
      rows = 3
    } = props
    if (autoFocus) {
      input.focus()
      input.setSelectionRange(input.value.length, input.value.length)
      if (typeof onFocus === 'function') {
        onFocus(input.value);
      }
    }
    if (autoHeight) {
      const hang = input.value.match(/\n/g)
      let lineHeight = input.scrollHeight / rows
      if (hang && hang.length + 1 > rows) {
        lineHeight = input.scrollHeight / (hang.length + 1)
      }
      this.setState({
        lineHeight
      })
      input.style.height = input.scrollHeight + 'px'
    }
    if(maxLength){
      const length = countSymbols(input.value);
      this.setState({length});
    }
  }

  componentDidUpdate() {
    const {props, input, state} = this
    const {autoHeight, rows = 3} = props
    const {lineHeight} = state
    if (autoHeight) {
      let hang = input.value.match(/\n/g)
      let row = rows
      if (hang && hang.length + 1 > rows) row = hang.length + 1
      input.style.height = row * lineHeight + 'px'
    }
  }


  handleComposition = (e) => {
    const {onCompositionStart, onCompositionUpdate, onCompositionEnd, onChange} = this.props;
    if (e.type === 'compositionstart') {
      if (typeof onCompositionStart === 'function') {
        onCompositionStart(e);
      }
    }
    if (e.type === 'compositionupdate') {
      if (typeof onCompositionUpdate === 'function') {
        onCompositionUpdate(e);
      }
    }

    if (e.type === 'compositionend') {
      const {value} = e.target;
      if (typeof onCompositionEnd === 'function') {
        onCompositionEnd(e);
      }
      if (typeof onChange === 'function') {
        onChange(value);
      }
    }
  };

  onFocus = (e) => {
    setTimeout(() => {
      this.setState({
        focused: true,
      });
    }, 0)
    const {onFocus} = this.props;
    if (typeof onFocus === 'function') {
      onFocus(e.target.value, e);
    }
  };

  onBlur = (e) => {
    setTimeout(() => {
      this.setState({
        focused: false,
      });
    }, 200)
    const {onBlur} = this.props;
    if (typeof onBlur === 'function') {
      onBlur(e.target.value, e);
    }
  };

  onClear = (e) => {
    this.input.value = ''
    this.setState({length: 0, value: ''});
    this.input.focus();
    const {onClear} = this.props;
    if (typeof onClear === 'function') {
      onClear(e.target.value, e);
    }
  }

  onChange = (e) => {
    const {onChange} = this.props;
    const {value} = e.target;
    const length = countSymbols(value);
    this.setState({length, value});
    if (typeof onChange === 'function') {
      onChange(value, e);
    }
  };

  focus() {
    this.input.focus();
  }

  blur() {
    this.input.blur();
  }


  render(): React.ReactNode {
    const {
      className,
      disabled,
      readOnly,
      onClear,
      autoHeight,
      maxLength,
      clearable,
      ...rest
    } = this.props
    const {focused, value,length} = this.state
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--clearable`]: clearable,
      [`${prefixCls}--readonly`]: readOnly,
    })
    const IconClass = classnames(`${prefixCls}__clear`, `${prefixCls}__clear--textarea`, {
      [`${prefixCls}__clear--show`]: focused && value && value.length > 0,
    })
    return (
      <div className={cls}>
        <textarea
          {...rest}
          ref={(ele) => {
            this.input = ele;
          }}
          maxLength={maxLength}
          disabled={disabled}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onCompositionStart={(e) => {
            this.handleComposition(e);
          }}
          onCompositionUpdate={(e) => {
            this.handleComposition(e);
          }}
          onCompositionEnd={(e) => {
            this.handleComposition(e);
          }}
        ></textarea>
        {clearable && <Icon type='wrongRoundFill' className={IconClass} onClick={(e) => {
          this.onClear(e)
        }}/>}
        {maxLength && <div className={`${prefixCls}__length`}>{`${length}/${maxLength}`}</div>}
      </div>
    )
  }

}
