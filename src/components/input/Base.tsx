import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {InputBase} from './PropsType';
import Icon from '../icon'

const prefixCls = `${__PREFIX__}-input`;

export default class Base extends PureComponent<InputBase, any> {
  static defaultProps = {
    type: 'text',
    clearable: true,
    readOnly: false,
    disabled: false,
  };
  private input;
  private onBlurTimeout;
  private blurFromClear;

  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      isOnComposition: false,
      value: props.defaultValue || props.value || '',
    };
  }

  componentDidMount() {
    const {autoFocus, onFocus} = this.props;
    const {focused} = this.state;
    if (autoFocus || focused) {
      this.input.focus();
      if (typeof onFocus === 'function') {
        onFocus(this.input.value);
      }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: Readonly<InputBase>) {
    if ('value' in nextProps) {
      this.setState({value: nextProps.value,});
    }
  }

  componentWillUnmount() {
    if (this.onBlurTimeout) {
      clearTimeout(this.onBlurTimeout);
      this.onBlurTimeout = null;
    }
  }

  onFocus = (e) => {
    this.setState({
      focused: true,
    });
    const {onFocus} = this.props;
    if (typeof onFocus === 'function') {
      onFocus(e.target.value);
    }
  }

  focus = () => {
    this.input.focus();
  };

  blur = () => {
    this.input.blur();
  };

  onBlur = (e) => {
    const {onBlur} = this.props;
    const {value} = e.target;
    this.onBlurTimeout = setTimeout(() => {
      if (!this.blurFromClear && document.activeElement !== this.input) {
        this.setState({
          focused: false,
        });
        if (typeof onBlur === 'function') {
          onBlur(value);
        }
      }
      this.blurFromClear = false;
    }, 200);
  };

  onChange = (e) => {
    const {onChange} = this.props;
    if (!this.state.focused) {
      this.setState({
        focused: true,
      });
    }
    this.setState({
      value: e.target.value,
    });
    if (onChange) {
      onChange(e.target.value);
    }
  };

  onClear = () => {
    const {isOnComposition} = this.state;
    const {onChange, onClear} = this.props;
    this.blurFromClear = true;
    this.setState({
      value: '',
    });

    !isOnComposition && this.focus();
    typeof onChange === 'function' && onChange('');
    typeof onClear === 'function' && onClear('');
  };

  handleComposition = (e) => {
    const {onCompositionStart, onCompositionUpdate, onCompositionEnd, onChange} = this.props;

    if (e.type === 'compositionstart') {
      this.setState({
        isOnComposition: true,
      });
      if (typeof onCompositionStart === 'function') {
        onCompositionStart(e);
      }
    }

    if (e.type === 'compositionupdate') {
      if (typeof onCompositionUpdate === 'function') {
        onCompositionUpdate(e);
      }
    }

    if (e.type === 'compositionend') {
      const {value} = e.target;
      // composition is end
      this.setState({
        isOnComposition: false,
      });
      if (typeof onCompositionEnd === 'function') {
        onCompositionEnd(e);
      }
      if (typeof onChange === 'function') {
        onChange(value);
      }
    }
  };

  render() {
    const {
      className,
      disabled,
      clearable,
      readOnly,
      type,
      onClear,
      ...rest
    } = this.props;
    const {value, focused} = this.state;

    const showClearIcon = clearable && ('value' in this.props);

    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--focus`]: focused,
      [`${prefixCls}--clearable`]: showClearIcon,
      [`${prefixCls}--readonly`]: readOnly,
    });

    const valueProps: any = {};
    if ('value' in this.props) {
      valueProps.value = value;
    }

    const IconClass = classnames(`${prefixCls}__clear`, {
      [`${prefixCls}__clear--show`]: focused && value && value.length > 0,
    })

    return (
      <div className={cls}>
        <input
          {...rest}
          {...valueProps}
          autoComplete="off"
          ref={ref => this.input = ref}
          type={type}
          disabled={disabled}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          onCompositionStart={(e) => {
            this.handleComposition(e);
          }}
          onCompositionUpdate={(e) => {
            this.handleComposition(e);
          }}
          onCompositionEnd={(e) => {
            this.handleComposition(e);
          }}
        />
        { <Icon type='wrongRoundFill' className={IconClass} onClick={() => this.onClear()}/>}
      </div>
    )
  }

}
