import React, { Fragment, PureComponent } from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import PropsType from './PropsType';

import Util from '../util';

const prefixCls = `${__PREFIX__}-popup`;

export default class Popup extends PureComponent<PropsType, any> {
  public container: any;
  public main: any;
  public box: any;
  public setProps: any;
  public disableIosBounce: any;
  static defaultProps = {
    direction: 'bottom',
    animationType: 'fade-scale',
    mask: true,
    animationDuration: 250
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.initEvent(true);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.visible !== this.props.visible) this.initEvent(false);
    else if (this.props.visible && this.props.children !== prevProps.children) {
      this.setProps({
        children: this.props.children
      });
    }
  }
  componentWillUnmount() {
    if (this.disableIosBounce) this.disableIosBounce();
    this.close()
  }

  initEvent(type) {
    const { className, visible } = this.props;
    if (!this.container) {
      const cls = classnames(prefixCls, className);
      const container = (this.container = document.createElement('div'));
      container.setAttribute('class', cls);
      visible && this.show(type);
    } else {
      if (visible) this.show(false);
      else this.close();
    }
  }

  show = type => {
    const _this = this;
    const { container } = this;
    const { children, direction, width, height, mask, open, style, animationType } = this.props;
    let { animationDuration } = this.props;
    if (animationType === 'none') animationDuration = 1;
    const mainCls = classnames(`${prefixCls}__main`, `${prefixCls}__${direction}`);
    document.body.appendChild(container);
    this.disableIosBounce = Util.disableIosBounce(container);
    class Main extends PureComponent<any, any> {
      public mainBox: any;
      constructor(props) {
        super(props);
        const { animationDuration, children, mainClass = '' } = props;
        _this.setProps = obj => {
          this.setState(obj);
        };
        this.state = {
          animationDuration,
          mainClass,
          children
        };
      }
      componentDidMount() {
        this.setState({
          animationDuration: type ? 0 : animationDuration
        });
        setTimeout(() => {
          this.setState({
            mainClass: `${prefixCls}__${direction}--show`
          });
        });
        open && open();
        document.body.classList.add(`${prefixCls}__ovhide`);
      }
      componentWillUnmount() {
        if (_this.disableIosBounce) _this.disableIosBounce();
      }

      render() {
        const { mainClass, children } = this.state;
        let { animationDuration } = this.state;
        if (animationType === 'none') animationDuration = 1;
        const { onMaskClick } = this.props;
        return (
          <Fragment>
            <div
              onClick={() => {
                onMaskClick && onMaskClick();
              }}
              style={{
                [Util.transition]:
                  animationType === 'none' ? '' : `opacity ${animationDuration}ms,opacity ${animationDuration}ms`
              }}
              className={classnames(`${prefixCls}__mask`, mainClass ? `${prefixCls}__mask--show` : '', {
                [`${prefixCls}__mask--opacity`]: !mask
              })}
            ></div>
            <div
              ref={ref => (this.mainBox = ref)}
              style={{
                ...{
                  [Util.transition]:
                    animationType === 'none' ? '' : `transform ${animationDuration}ms,opacity ${animationDuration}ms`,
                  width,
                  height
                },
                ...style
              }}
              className={classnames(mainCls, mainClass, {
                [`${prefixCls}__center--${animationType}`]: direction === 'center'
              })}
            >
              {children}
            </div>
          </Fragment>
        );
      }
    }
    ReactDOM.render(<Main {...this.props}>{children}</Main>, container);
  };
  close = () => {
    const { container } = this;
    const { animationType, close } = this.props;
    let { animationDuration } = this.props;
    if (animationType === 'none') animationDuration = 1;
    this.setProps({
      mainClass: ''
    });
    if (container) {
      setTimeout(() => {
        container.parentNode.removeChild(container);
        this.container = null;
        close && close();
        if (this.disableIosBounce) this.disableIosBounce();
        document.body.classList.remove(`${prefixCls}__ovhide`);
      }, animationDuration);
    }
  };

  render() {
    return <div></div>;
  }
}
