import React, { useState } from 'react';
import { Picker } from 'wxl';

const dataTwo = [
  { name: '1', id: 0 },
  { name: '2', id: 1 },
  { name: '3', id: 2 },
  { name: '4', id: 3 },
  { name: '5', id: 4 }
];
const dataOne = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const dataThree = [
  { title: '第一项', key: 0 },
  { title: '第二项', key: 1 },
  { title: '第三项', key: 2 },
  { title: '第四项', key: 3 },
  { title: '第五项', key: 4 }
];

const test = function() {
  const [datas, setDatas] = useState(dataThree);
  setTimeout(() => {
    setDatas([
      { title: '1', key: 0 },
      { title: '2', key: 1 },
      { title: '3', key: 2 }
    ]);
  }, 3500);
  return (
    <div>
      <div style={{ height: 50 }}></div>
      <Picker disable data={dataTwo} />
      <div style={{ height: 50 }}></div>
      <Picker
        value="第三项"
        onChange={data => {
          console.log(data);
        }}
        className={'test_123'}
        renderKey="title"
        data={datas}
      />
      <div style={{ height: 50 }}></div>
      <Picker className={'test_123'} data={dataOne} />
      <div style={{ height: 50 }}></div>
      <Picker className={'test_123'} data={dataOne} value={2} />
      <div style={{ height: 500 }}></div>
    </div>
  );
};
export default test;
