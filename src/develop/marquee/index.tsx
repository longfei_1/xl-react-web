import React from 'react';
import { Marquee, Cell } from 'wxl';

const test = function() {

  return (
    <aside>
      <Cell>
        <Marquee direction={'left'} speed={100}>
          <div style={{ lineHeight: '40px' }}>这是一个向左的滑动，</div>
          <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
          <div style={{ lineHeight: '40px' }}>自动滚动</div>
        </Marquee>
      </Cell>
      <Cell>
        <Marquee direction={'right'}>
          <div style={{ lineHeight: '40px' }}>这是一个向右的滑动，</div>
          <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
          <div style={{ lineHeight: '40px' }}>标签都设置为行内块元素，</div>
        </Marquee>
      </Cell>
      <Cell>
        <Marquee direction={'up'} style={{ height: '40px' }}>
          <div style={{ lineHeight: '40px' }}>这是一个向上的滑动，</div>
          <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
          <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
        </Marquee>
      </Cell>
      <Cell>
        <Marquee direction={'down'} style={{ height: '40px' }}>
          <div style={{ lineHeight: '40px' }}>这是一个向下的滑动，</div>
          <div style={{ lineHeight: '40px' }}>里面可以是多个标签，</div>
          <div style={{ lineHeight: '40px' }}>Marquee必须给要给高度，</div>
        </Marquee>
      </Cell>
    </aside>
  );
};
export default test;
