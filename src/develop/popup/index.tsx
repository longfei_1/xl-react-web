import React from 'react';
// import { useState } from 'react';
import { Popup, Cell } from 'wxl';
class Demo extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      oneVisible: false,
      twoVisible: false,
      threeVisible: false,
      fourVisible: false,
      fiveVisible: false
    };
  }
  switchVisible = attr => {
    this.setState({
      [`${attr}Visible`]: !this.state[`${attr}Visible`]
    });
  };
  render() {
    const { oneVisible, twoVisible, threeVisible, fourVisible, fiveVisible } = this.state;
    return (
      <aside>
        <Cell
          onClick={() => {
            this.switchVisible('one');
          }}
          hasArrow
          title="宽高不固定"
        />
        <Cell
          onClick={() => {
            this.switchVisible('five');
          }}
          hasArrow
          title="无动效"
        />
        <Cell
          onClick={() => {
            this.switchVisible('four');
          }}
          hasArrow
          title="动效(fade-scale)"
        />
        <Cell
          onClick={() => {
            this.switchVisible('two');
          }}
          hasArrow
          title="动效(fade)"
        />
        <Cell
          onClick={() => {
            this.switchVisible('three');
          }}
          hasArrow
          title="动效(fade-slide)"
        />

        <Popup direction="center" visible={oneVisible}>
          <div style={{ padding: '10px' }}>
            <span onClick={() => this.switchVisible('one')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          animationType="none"
          visible={fiveVisible}
          onMaskClick={() => this.switchVisible('five')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('five')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          animationDuration={1000}
          animationType="fade"
          visible={twoVisible}
          onMaskClick={() => this.switchVisible('two')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('two')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          animationType="fade-slide"
          direction="center"
          visible={threeVisible}
          onMaskClick={() => this.switchVisible('three')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('three')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          visible={fourVisible}
          animationType="fade-scale"
          onMaskClick={() => this.switchVisible('four')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('four')}>关闭弹框</span>
          </div>
        </Popup>
      </aside>
    );
  }
}
export default Demo;
