import React from "react";
import { Input } from 'wxl';



const test = function () {
  return (
    <div>
      <div style={{height:500}}></div>
      <Input
        type={'card'}
        clearable ={'textarea'}
        maxLength ={300}
        value={`854e54x`}
        autoFocus={`502.52.2`}
      />
      <Input
        type={'textarea'}
        autoHeight ={'textarea'}
        rows ={2}
        maxLength ={10}
        defaultValue={`521555x`}
      />
    </div>
  )
};
export default test
