import React from 'react';
import { Swiper } from 'wxl';

const test = function() {
  // const [test, setTest] = useState(false);
  // setTimeout(() => {
  //   setTest(true);
  // }, 3000);
  return (
    <div style={{overflow:'hidden'}}>
      {/*<div style={{ height: 500 }}></div>*/}
      <div style={{ padding: 10 }} >
        <Swiper preventDefault={true}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
      <div className="item-swiper-one">
        <Swiper  overflow gap={20}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动adfasdfasdfasdf，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规adfasdfasdfasdsaf滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个adfasdfsadfsadfasdfasd常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
      <div className="item-swiper-two">
        <Swiper loop={false} overflow gap={20} gapPadding={40}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规asdfasdasdfasdfasdfsadffasdf滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规asdfasdasdfsadfsadfsafsfasdfasdfas滚动，滚动的第二页</div>
        </Swiper>
      </div>
      <div style={{ padding: 10 }}>
        <Swiper direction="right" loop={false}  gap={20}  showPagination activeIndex={2}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规asdfasdasdfasdfasdfsadffasdf滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规asdfasdasdfasdfasdfsadffasdf滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
      <div style={{ width: 240, marginLeft: 40, overflow: 'visible' }}>
        <Swiper style gap={20} loop={false}  preventDefault={true} direction={'right'}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>
            11234<br/>
            56789<br/>
            12345678912<br/>
            34567891234567891<br/>
            23456789<br/>
          </div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#fff' }}>
            11234567891234567
            89
            123456789123456789123456789
          </div>
        </Swiper>
      </div>
      <div style={{ height: 500 }}></div>
    </div>
  );
};
export default test;
