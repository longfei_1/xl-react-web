import React from 'react';
import ReactDOM from 'react-dom';
import Demo from './index'


export default class Markdown extends React.Component {
  constructor(props) {
    super(props);
    this.components = new Map();
    this.nodeList = [];
  }

  componentDidMount() {
    this.renderDOM();
  }

  componentDidUpdate() {
    this.renderDOM();
  }

  componentWillUnmount() {
    this.nodeList.forEach((node) => {
      ReactDOM.unmountComponentAtNode(node);
    });
  }

  renderDOM() {
    // eslint-disable-next-line
    for (const [id, component] of this.components) {
      const div = document.getElementById(id);
      this.nodeList.push(div);
      if (div instanceof HTMLElement) {
        ReactDOM.render(component, div);
      }
    }
  }

  render() {
    const { document, className, match, md } = this.props;
    const { component } = match.params;
    if (typeof md === 'string') {
      const list = [];
      md.replace(/## 自定义 Iconfont 图标\s?([^]+)/g, '')
        .replace(/## API\s?([^]+)/g, '')
        .replace(/##\s?([^]+?)((?=##)|$)/g, (_match, p1) => {
          const id = parseInt(Math.random() * 1e9, 10)
            .toString(36);
          this.components.set(id, React.createElement(Demo, this.props, p1));
          list.push(<div id={id} key={id}></div>);
        })
      return (
        <div>{list}</div>
      );
    }

    return <span/>;
  }
}
