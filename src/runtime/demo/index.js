import React from 'react';
import ReactDOM from 'react-dom';
import { transform } from '@babel/standalone';


export default class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.containerId = `${parseInt(Math.random() * 1e9, 10)
      .toString(36)}`;
    this.document = props.children.match(/([^]*)\n?(```[^]+```)/);
    this.title = String(this.document[1]);
    this.source = this.document[2].match(/```(.*)\n?([^]+)```/);
  }

  componentDidMount() {
    this.renderSource(this.source[2]);
  }

  componentWillUnmount() {
    if (this.containerElem) {
      ReactDOM.unmountComponentAtNode(this.containerElem);
    }
  }

  renderSource(value) {
    import('@src/components').then((Element) => {
      console.log(Element)
      const args = ['context', 'React', 'ReactDOM', 'wxl'];
      const argv = [this, React, ReactDOM, Element];

      return {
        args,
        argv,
      };
    })
      .then(({ args, argv }) => {
        value = value
          .replace(/import\s+\{\s+(.*)\s+\}\s+from\s+'wxl';/, 'const { $1 } = wxl;')
          .replace(/ReactDOM.render\(\s?([^]+?)(,\s?mountNode\s?\))/g, `
          ReactDOM.render(
            <div>
              $1
            </div>,
            document.getElementById('${this.containerId}'),
          )
        `);

        const { code } = transform(value, {
          presets: ['es2015', 'react'],
          plugins: ['proposal-class-properties'],
        });
        console.log(code)
        args.push(code);
        // eslint-disable-next-line
        new Function(...args)(...argv);

        this.source[2] = value;
      })
      .catch((err) => {
        if (process.env.NODE_ENV !== 'production') {
          throw err;
        }
      });
  }

  render() {
    const { location } = this.props;
    return <div id={this.containerId} ref={(elem) => {
      this.containerElem = elem;
    }}/>;
  }
}
