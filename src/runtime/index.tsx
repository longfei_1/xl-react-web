import React, {Component, Suspense, lazy} from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={require('@site/web/pages/Index').default} />
        <Route path="/components/:component" render={require('@site/web/pages/Components').default} />
        <Route path="/demo/:component" render={require('@site/web/pages/Demo').default} />
        <Route path="/develop/:component" render={(props) => {
          const {match} = props
          const {component} = match.params
          const Com = lazy(() => import(`@src/develop/${component}`));
          return <Suspense fallback={null}><Com {...props}/></Suspense>
        }}/>
      </Switch>
    );
  }
}

export default withRouter(App);
