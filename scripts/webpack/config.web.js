const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const config = require('./config.lib');

config.mode = 'production';
config.output.filename = 'js/[name].[chunkhash:8].js';
config.output.path = path.join(process.cwd(), 'web');
config.output.publicPath = './';
config.plugins.push(
  new HtmlWebpackPlugin({
  template: './scripts/tpl/index.html',
  // chunks: ['manifest', 'index'],
  filename: 'index.html'
  // inject: false,
})
);

module.exports = config;
