const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const safePostCssParser = require('postcss-safe-parser');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const config = require('./config.base');

const env = process.env.NODE_ENV;
const version = process.env.VERSION || require('../../package.json').version;
config.mode = 'production';
config.devtool = 'source-map';

config.entry = {
  wxl: [
    './src/components/umd.js',
    './src/components/index.tsx',
  ],
};

config.output = {
  library: 'wxl',
  libraryTarget: 'umd',
  path: path.join(process.cwd(), 'dist'),
  filename: '[name].js',
};

config.externals = {
  react: {
    root: 'React',
    commonjs2: 'react',
    commonjs: 'react',
    amd: 'react',
  },
  'react-dom': {
    root: 'ReactDOM',
    commonjs2: 'react-dom',
    commonjs: 'react-dom',
    amd: 'react-dom',
  },
};
config.optimization = {
  minimizer: [
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      sourceMap: true,
      uglifyOptions: {
        output: {
          comments: false,
        },
      },
    }),
    new OptimizeCSSAssetsPlugin({
      cssProcessorOptions: {
        parser: safePostCssParser,
      },
      cssProcessorPluginOptions: {
        preset: ['default', {
          reduceTransforms: false,
          discardComments: {
            removeAll: true,
          },
        }],
      },
    }),
  ],
};
config.plugins.push(
  new MiniCssExtractPlugin({
    filename: '[name].min.css',
  }),
  new webpack.BannerPlugin(`
    Wxl v${version}
  `),
);









module.exports = config;
