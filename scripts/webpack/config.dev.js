const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const config = require('./config.lib');

config.mode = 'development';
config.devtool = 'cheap-module-eval-source-map';
config.output.filename = 'js/[name].js';
config.output.publicPath = '/';
config.optimization.minimize = false;
config.plugins.push(
    new webpack.HotModuleReplacementPlugin(), //热替换
    new ForkTsCheckerWebpackPlugin({
        async: false
    }),
    new HtmlWebpackPlugin({
        template: './scripts/tpl/index.html',
        filename: 'index.html',
        debug: true
      // inject: false,
    }),
);

// hot-loader
Object.keys(config.entry).forEach((key) => {
    config.entry[key].unshift('react-hot-loader/patch');
});
config.module.rules[0].use[0].options.plugins.push('react-hot-loader/babel');

// dev-server
config.devServer = {
    host: '0.0.0.0',
    port: 3330,
    compress: true,
    noInfo: true,
    inline: true,
    hot: true,
    progress: true,
};

module.exports = config;
