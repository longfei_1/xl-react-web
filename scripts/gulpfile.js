const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass');
const through2 = require('through2');
const Autoprefixer = require('gulp-autoprefixer');

sass.compiler = require('node-sass');

gulp.task('sass', () => {
  return gulp
    .src(path.resolve(__dirname, '../src/components/**/index.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(
      Autoprefixer({
        overrideBrowserslist: ['last 4 versions', 'Android 4.1', 'iOS 7.1', 'Chrome > 31'],
        cascade: true,
        remove: true
      })
    )
    .pipe(gulp.dest(path.resolve(__dirname, '../lib')))
    .pipe(gulp.dest(path.resolve(__dirname, '../es')));
});

gulp.task('css', () => {
  return gulp
    .src(path.resolve(__dirname, '../lib/**/style/index.js'))
    .pipe(
      through2.obj(function z(file, encoding, next) {
        this.push(file.clone());
        const content = file.contents.toString(encoding);
        file.contents = Buffer.from(((content) => {
          return content
            .replace(/\/style\/?'/g, "/style/css'")
            .replace(/\/style\/?"/g, '/style/css"')
            .replace(/\.scss/g, '.css');
        })(content));
        file.path = file.path.replace(/index\.js/, 'css.js');
        this.push(file);
        next();
      })
    )
    .pipe(gulp.dest(path.resolve(__dirname, '../lib')))
    .pipe(gulp.dest(path.resolve(__dirname, '../es')));
});

gulp.task('default', gulp.series(['sass', 'css']));
