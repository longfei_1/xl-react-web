module.exports = {
  presets: [
    ['@babel/preset-env', {
      modules: false,
    }],
    '@babel/preset-react',
    '@babel/preset-typescript',
  ],
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
    'babel-plugin-jsx-control-statements',
    '@babel/plugin-proposal-class-properties',
    ['@babel/plugin-proposal-decorators', {legacy: true}],
    ['import', { libraryName: 'wxl', libraryDirectory: '', style: true }],
    ['@babel/plugin-transform-runtime', { corejs: 2 }]
  ],
  env: {
    test: {
      presets: [
        ['@babel/preset-env', {
          modules: false,
          targets: {
            node: "current"
          }
        }],
        '@babel/preset-react',
        '@babel/preset-typescript',
      ],
      plugins: [
        '@babel/plugin-transform-modules-commonjs',
      ],
    },
  },
};
