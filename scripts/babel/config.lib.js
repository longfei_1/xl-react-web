module.exports = {
  presets: ['@babel/env', '@babel/react', '@babel/typescript'],
  plugins: [
    'babel-plugin-jsx-control-statements',
    ['./utils', { __PREFIX__: 'xl'}],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['@babel/plugin-proposal-decorators', { legacy: true }]
  ]
};
