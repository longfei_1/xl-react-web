module.exports = {
  presets: [
    '@babel/react',
    '@babel/typescript',
  ],
  plugins: [
    ['./utils', { __PREFIX__: 'xl'}],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    '@babel/plugin-proposal-object-rest-spread',
  ],
};
