const babelJest = require('babel-jest');
const babelConfig = require('../babel/config.dev');
module.exports = babelJest.createTransformer(babelConfig);
