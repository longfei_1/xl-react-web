module.exports = {
  rootDir: process.cwd(),
  roots: [
    '<rootDir>/src/components',
  ],
  setupFiles: [
    require.resolve('./environment.js'),
  ],
  globals: {
    "ts-jest": {
      "tsConfig": "tsconfig.json"
    }
  },
  // moduleNameMapper: {
  //   "^wxl(.*)$": "<rootDir>/src/components$1",
  //   "\\.(s?css|less)$": "identity-obj-proxy"
  // },
  setupTestFrameworkScriptFile: require.resolve('./setup.js'),
  // setupFilesAfterEnv:[
  //   '<rootDir>/scripts/jest/setup.js',
  //   ],
  testRegex: '/__tests__/[^.]+\\.test(\\.jsx|[^d]\\.ts)$',
  transform: {
    '^.+\\.jsx?$': require.resolve('./preprocessor'),
    '^.+\\.tsx?$': 'ts-jest',
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
  ],
  collectCoverageFrom: [
    'src/components/**/*.{ts,tsx}',
    '!src/components/*/PropsType.{ts,tsx}',
    '!src/components/**/style/*.{ts,tsx}',
    '!src/components/style/**/*',
  ],
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node',
  ],
  testURL: 'http://localhost',
};
