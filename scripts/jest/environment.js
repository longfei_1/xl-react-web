global.requestAnimationFrame = jest.fn();
global.SVGElement = jest.fn();
global.__PREFIX__ = 'xl';
global.wait = async (time, bak) => {
  return new Promise(resolve => {
    setTimeout(() => {
      if (bak) bak();
      resolve();
    }, time);
  });
};
