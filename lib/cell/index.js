"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames6 = _interopRequireDefault(require("classnames"));

var _icon = _interopRequireDefault(require("../icon"));

var _util = _interopRequireDefault(require("../util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-cell");

var Cell =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Cell, _PureComponent);

  function Cell() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Cell);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Cell)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.inner = void 0;
    _this.extra = void 0;
    _this.MOVE_ING = void 0;
    _this.TouchInfo = void 0;

    _this.start = function (e) {
      if (!_this.props.option) return;
      _this.TouchInfo = {};
      var touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      var _touch = touch,
          pageX = _touch.pageX,
          pageY = _touch.pageY;
      var maxMove = _this.extra.offsetWidth;
      _this.TouchInfo = {
        sX: pageX,
        maxMove: maxMove,
        move: null,
        sY: pageY
      };
      _this.MOVE_ING = true;
    };

    _this.move = function (e) {
      if (!_this.props.option || !_this.MOVE_ING) return;
      var touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      var _this$TouchInfo = _this.TouchInfo,
          sX = _this$TouchInfo.sX,
          sY = _this$TouchInfo.sY,
          maxMove = _this$TouchInfo.maxMove,
          move = _this$TouchInfo.move;
      var _touch2 = touch,
          pageX = _touch2.pageX,
          pageY = _touch2.pageY;
      var mx = pageX - sX;
      var my = pageY - sY;
      var slideX;

      if (Math.abs(mx) > Math.abs(my) && move === null || move) {
        _this.TouchInfo.move = true;
        slideX = _util.default.getTranslateInfo(_this.inner, 1);
        slideX = slideX + mx;
        if (slideX < -maxMove) slideX = -maxMove;else if (slideX > 0) slideX = 0;

        _util.default.setTranslateInfo(_this.inner, {
          x: slideX
        });

        document.body.classList.add("".concat(prefixCls, "__ovhide"));
      } else {
        _this.TouchInfo.move = false;
      }

      _this.TouchInfo = _objectSpread({}, _this.TouchInfo, {}, {
        sX: pageX,
        sY: pageY
      });
    };

    _this.end = function () {
      if (!_this.props.option) return;
      document.body.classList.remove("".concat(prefixCls, "__ovhide"));
      var maxMove = _this.TouchInfo.maxMove;

      var slideX = _util.default.getTranslateInfo(_this.inner, 1);

      if (slideX < -maxMove / 2) slideX = -maxMove;else slideX = 0;
      _this.TouchInfo.move = null;

      if (slideX != 0) {
        var resetDom = function resetDom(e) {
          if (!_util.default.checkInParent(e.target, _this.inner)) {
            _util.default.setTranslateInfo(_this.inner, {
              x: 0,
              time: 150
            });
          }

          document.body.removeEventListener('touchstart', resetDom);
        };

        document.body.addEventListener('touchstart', resetDom);
      }

      _util.default.setTranslateInfo(_this.inner, {
        x: slideX,
        time: 150
      });

      _this.MOVE_ING = false;
    };

    return _this;
  }

  _createClass(Cell, [{
    key: "render",
    value: function render() {
      var _classnames,
          _this2 = this;

      var _this$props = this.props,
          children = _this$props.children,
          className = _this$props.className,
          hasArrow = _this$props.hasArrow,
          icon = _this$props.icon,
          title = _this$props.title,
          headerNoBorder = _this$props.headerNoBorder,
          footerNoBorder = _this$props.footerNoBorder,
          autoHeight = _this$props.autoHeight,
          disabled = _this$props.disabled,
          onClick = _this$props.onClick,
          option = _this$props.option,
          rest = _objectWithoutProperties(_this$props, ["children", "className", "hasArrow", "icon", "title", "headerNoBorder", "footerNoBorder", "autoHeight", "disabled", "onClick", "option"]);

      var cls = (0, _classnames6.default)(prefixCls, className, (_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls, "--disabled"), disabled), _defineProperty(_classnames, "".concat(prefixCls, "--link"), !disabled && !!onClick), _defineProperty(_classnames, "".concat(prefixCls, "--arrow"), hasArrow), _defineProperty(_classnames, "".concat(prefixCls, "--hnb"), !headerNoBorder), _defineProperty(_classnames, "".concat(prefixCls, "--f-n-b"), footerNoBorder), _classnames));
      var titleCls = (0, _classnames6.default)("".concat(prefixCls, "__title"), _defineProperty({}, "".concat(prefixCls, "__title--label"), !!children));
      var headerCls = (0, _classnames6.default)("".concat(prefixCls, "__header"), _defineProperty({}, "".concat(prefixCls, "__header--nb"), !headerNoBorder));
      var footerCls = (0, _classnames6.default)("".concat(prefixCls, "__footer"), _defineProperty({}, "".concat(prefixCls, "__footer--nb"), footerNoBorder));
      var innerCls = (0, _classnames6.default)("".concat(prefixCls, "__inner"), _defineProperty({}, "".concat(prefixCls, "__inner--autoHeight"), autoHeight));

      var iconRender = icon && _react.default.createElement("div", {
        className: "".concat(prefixCls, "__icon")
      }, icon);

      var titleRender = title && _react.default.createElement("div", {
        className: titleCls
      }, title);

      var contentRender = children && _react.default.createElement("div", {
        className: "".concat(prefixCls, "__content")
      }, children);

      var arrowRender = hasArrow && _react.default.createElement(_icon.default, {
        type: 'arrowDownOneLeft'
      });

      return _react.default.createElement("div", _extends({
        className: cls,
        onClick: onClick
      }, rest), _react.default.createElement("div", {
        className: innerCls,
        ref: function ref(dom) {
          _this2.inner = dom;
        },
        onTouchStart: this.start,
        onMouseDown: this.start,
        onTouchMove: this.move,
        onMouseMove: this.move,
        onTouchEnd: this.end,
        onMouseUp: this.end
      }, _react.default.createElement("div", {
        className: headerCls
      }, iconRender), _react.default.createElement("div", {
        className: "".concat(prefixCls, "__body")
      }, titleRender, contentRender), _react.default.createElement("div", {
        className: footerCls
      }, arrowRender), option && _react.default.createElement("div", {
        ref: function ref(_ref) {
          return _this2.extra = _ref;
        },
        className: "".concat(prefixCls, "__option")
      }, option)));
    }
  }]);

  return Cell;
}(_react.PureComponent);

exports.default = Cell;
Cell.defaultProps = {
  hasArrow: false,
  headerNoBorder: false,
  footerNoBorder: false,
  autoHeight: false,
  option: null,
  disabled: false
};