"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var scroll = _interopRequireWildcard(require("./scroll"));

var device = _interopRequireWildcard(require("./device"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var IS_IN_JEST = global && global.process;
var IS_IOS = device.IS_IOS;
var getScrollTop = scroll.getScrollTop,
    getScrollEventTarget = scroll.getScrollEventTarget,
    preventDefaultStopPropagation = scroll.preventDefaultStopPropagation;

var getVendorPrefix = function getVendorPrefix() {
  var body = document.body || document.documentElement;
  var style = body.style;
  var vendor = ['webkit', 'khtml', 'moz', 'ms', 'o'];
  var i = 0;

  while (i < vendor.length) {
    if (typeof style[vendor[i] + 'Transition'] === 'string') {
      return vendor[i];
    }

    i++;
  }
};

var prefix = getVendorPrefix();

var getTranslateInfo = function getTranslateInfo(dom, type) {
  var t = dom.style["".concat(prefix, "Transform")];
  var reg = /translate([X|Y])?\((.+)?\)/;
  var rst = reg.exec(t);
  var x = 0;
  var y = 0;

  if (rst) {
    if (rst[1] === 'X') x = parseFloat(rst[2]);
    if (rst[1] === 'Y') y = parseFloat(rst[2]);

    if (!rst[1]) {
      var xy = rst[2].split(',');
      x = parseFloat(xy[0]);
      y = parseFloat(xy[1]);
    }
  }

  if (!type) return {
    x: x,
    y: y
  };else if (type === 1) return x;else return y;
};

var setTranslateInfo = function setTranslateInfo(dom, option) {
  var _option$x = option.x,
      x = _option$x === void 0 ? 0 : _option$x,
      _option$y = option.y,
      y = _option$y === void 0 ? 0 : _option$y,
      _option$time = option.time,
      time = _option$time === void 0 ? null : _option$time,
      _option$type = option.type,
      type = _option$type === void 0 ? 'linear' : _option$type;

  if (time !== null) {
    dom.style["".concat(prefix, "Transition")] = "all ".concat(time / 1000, "s ").concat(type);
    setTimeout(function () {
      dom.style["".concat(prefix, "Transition")] = "all 0s";
    }, time);
  }

  dom.style["".concat(prefix, "Transform")] = "translate(".concat(x, "px,").concat(y, "px)");
};

var checkInParent = function checkInParent(son, parent) {
  if (son == parent) return true;else {
    if (son.parentNode) {
      return checkInParent(son.parentNode, parent);
    } else return false;
  }
};

var isArray = function isArray(input) {
  return Object.prototype.toString.apply(input) === '[object array]';
};

var globalInfo = {};

var getBodyWidth = function getBodyWidth() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  if (globalInfo.width && type) return globalInfo.width;
  var width = document.body.clientWidth;
  globalInfo = _objectSpread({}, globalInfo, {}, {
    width: width
  });
  return width;
};

var getBodyHeight = function getBodyHeight() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  if (globalInfo.height && !type) return globalInfo.height;
  var height = document.body.clientHeight;
  globalInfo = _objectSpread({}, globalInfo, {}, {
    height: height
  });
  return height;
};

var PADDING = 5;

var isInClient = function isInClient(pageX, pageY) {
  var bodyHeight = getBodyHeight(true);
  var bodyWidth = getBodyWidth(true);
  if (IS_IN_JEST) return true;

  if (pageX < PADDING || pageX > bodyWidth - PADDING || pageY < PADDING || pageY > bodyHeight - PADDING) {
    return false;
  }

  return true;
};

var TOUCH_EVENT_NAME = function () {
  if ('ontouchstart' in window || IS_IN_JEST) {
    return {
      start: 'touchstart',
      move: 'touchmove',
      end: 'touchend',
      option: {
        passive: false
      }
    };
  } else {
    return {
      start: 'mousedown',
      move: 'mousemove',
      end: 'mouseup',
      option: {}
    };
  }
}();

var PREFIX = prefix ? "".concat(prefix.substr(0, 1).toUpperCase()).concat(prefix.substring(1)) : '';

var disableIosBounce = function disableIosBounce(element) {
  var startY;
  var scoller;
  if (!IS_IOS) return function () {};
  document.addEventListener('touchstart', start, {
    passive: false
  });
  document.addEventListener('touchmove', move, {
    passive: false
  });

  function start(e) {
    var touch = e.touches && e.touches[0];
    var pageY = touch.pageY;
    startY = pageY;
    scoller = null;
  }

  function move(e) {
    var _ref = e || {},
        touches = _ref.touches,
        target = _ref.target;

    var touch = touches && touches[0];

    if (!scoller) {
      scoller = target && getScrollEventTarget(target, element);
    }

    var scrollTop = getScrollTop(scoller);
    var pageY = touch.pageY;
    var _scoller = scoller,
        scrollHeight = _scoller.scrollHeight,
        offsetHeight = _scoller.offsetHeight;

    if (scrollHeight <= offsetHeight || scrollTop <= 0 && pageY - startY >= 0 || scrollHeight - offsetHeight <= scrollTop && pageY - startY <= 0 || scoller === element) {
      preventDefaultStopPropagation(e);
    }

    startY = pageY;
  }

  return function () {
    // @ts-ignore
    document.removeEventListener('touchstart', start, {
      passive: false
    }); // @ts-ignore

    document.removeEventListener('touchmove', move, {
      passive: false
    });
  };
};

var _default = _objectSpread({}, {
  scroll: scroll
}, {}, {
  device: device
}, {
  TOUCH_EVENT_NAME: TOUCH_EVENT_NAME,
  IS_IN_JEST: IS_IN_JEST,
  disableIosBounce: disableIosBounce,
  getVendorPrefix: getVendorPrefix,
  transform: prefix ? "".concat(PREFIX, "Transform") : 'transform',
  transition: prefix ? "".concat(PREFIX, "Transition") : 'transition',
  setTranslateInfo: setTranslateInfo,
  checkInParent: checkInParent,
  isArray: isArray,
  getBodyWidth: getBodyWidth,
  getBodyHeight: getBodyHeight,
  isInClient: isInClient,
  getTranslateInfo: getTranslateInfo
});

exports.default = _default;