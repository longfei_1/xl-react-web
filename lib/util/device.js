"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IS_ANDROID = exports.IS_IOS = void 0;
var u = navigator.userAgent; // app = navigator.appVersion;

var IS_IOS = function () {
  return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
}();

exports.IS_IOS = IS_IOS;

var IS_ANDROID = function () {
  return u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
}();

exports.IS_ANDROID = IS_ANDROID;