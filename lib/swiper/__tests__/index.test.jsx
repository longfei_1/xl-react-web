import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Swiper from '../index';

const prefixCls = `${__PREFIX__}-swiper`;

describe('Swiper', () => {
  // Cell 常规展示
  it('renders  Swiper', () => {
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <Swiper className={'test_123'}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
        <Swiper autoPlay showPagination>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>
            这是个自动滚动（带有pagination），滚动的第一页
          </div>
          <div style={{ height: 100, background: 'yellow', color: '#fff' }}>
            这是个自动滚动（带有pagination），滚动的第二页
          </div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>
            这是个自动滚动（带有pagination），滚动的第三页
          </div>
        </Swiper>
        <Swiper gap={30}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
    jest.advanceTimersByTime(300);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  it('activeIndex', () => {
    jest.useFakeTimers();
    const wrapper = mount(
      <Swiper activeIndex={2}>
        <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
        <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
        <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
      </Swiper>
    );
    jest.advanceTimersByTime(300);
    expect(toJson(wrapper)).toMatchSnapshot();
    expect(wrapper.state('activeIndex')).toEqual(2);
  });
  it('autoPlay', () => {
    jest.useFakeTimers();
    const onChange = jest.fn();
    const wrapper = mount(
      <Swiper autoPlay onChange={onChange} autoPlayIntervalTime={3000} animationDuration={200}>
        <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
        <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
        <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
      </Swiper>
    );
    jest.advanceTimersByTime(3000);
    expect(wrapper.state('activeIndex')).toEqual(2);
    expect(onChange).toBeCalledWith(2);
    expect(toJson(wrapper)).toMatchSnapshot();
    jest.advanceTimersByTime(3000);
    expect(wrapper.state('activeIndex')).toEqual(3);
    expect(onChange).toBeCalledWith(3);
    expect(toJson(wrapper)).toMatchSnapshot();
    jest.advanceTimersByTime(3000);
    expect(wrapper.state('activeIndex')).toEqual(4);
    jest.advanceTimersByTime(250);
    expect(wrapper.state('activeIndex')).toEqual(1);
    expect(onChange).toBeCalledWith(1);
  });
  it('touch', () => {
    global.innerWidth = 320;
    jest.useFakeTimers();
    const onChange = jest.fn();
    const wrapper = mount(
      <Swiper autoPlay onChange={onChange} activeIndex={2} autoPlayIntervalTime={3000} animationDuration={200}>
        <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
        <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
        <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
      </Swiper>
    );
    jest.advanceTimersByTime(500);
    expect(wrapper.state('activeIndex')).toEqual(2);
    const contain = wrapper.find(`div.${prefixCls}__contain`);
    contain
      .simulate('touchStart', {
        touches: [
          {
            pageX: 15,
            pageY: 10
          }
        ]
      })
      .simulate('touchMove', {
        touches: [
          {
            pageX: 250,
            pageY: 15
          }
        ]
      })
      .simulate('touchMove', {
        touches: [
          {
            pageX: 150,
            pageY: 150
          }
        ]
      })
      .simulate('touchMove', {
        touches: [
          {
            pageX: 310,
            pageY: 140
          }
        ]
      })
      .simulate('touchEnd');
    jest.advanceTimersByTime(500);
    expect(wrapper.state('activeIndex')).toEqual(1);
    contain.simulate('touchStart', {
      touches: [
        {
          pageX: 250,
          pageY: 10
        }
      ]
    });
    contain.simulate('touchMove', {
      touches: [
        {
          pageX: 150,
          pageY: 15
        }
      ]
    });
    contain.simulate('touchMove', {
      touches: [
        {
          pageX: 300,
          pageY: 150
        }
      ]
    });
    contain.simulate('touchMove', {
      touches: [
        {
          pageX: 25,
          pageY: 140
        }
      ]
    });
    contain.simulate('touchEnd');
    jest.advanceTimersByTime(300);
    expect(wrapper.state('activeIndex')).toEqual(2);
  });
  it('showPagination', () => {
    global.innerWidth = 320;
    jest.useFakeTimers();
    const onChange = jest.fn();
    const wrapper = mount(
      <Swiper autoPlay onChange={onChange} activeIndex={2} showPagination>
        <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
        <div style={{ height: 100, background: 'yellow', color: '#fff' }}>这是个常规滚动，滚动的第二页</div>
        <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
      </Swiper>
    );
    jest.advanceTimersByTime(500);
    expect(wrapper.state('activeIndex')).toEqual(2);
    expect(toJson(wrapper)).toMatchSnapshot();
    const pagination = wrapper.find(`div.${prefixCls}__pagination`);
    const dot = pagination.find(`.${prefixCls}__dot`);
    dot.at(0).simulate('click');
    jest.advanceTimersByTime(250);
    expect(toJson(wrapper)).toMatchSnapshot();
    expect(wrapper.state('activeIndex')).toEqual(1);
    expect(onChange).toBeCalledWith(1);
  });
});
