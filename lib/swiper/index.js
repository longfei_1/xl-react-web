"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames3 = _interopRequireDefault(require("classnames"));

var _util = _interopRequireDefault(require("../util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-swiper");
var children = null;

var Swiper =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Swiper, _PureComponent);

  function Swiper(props) {
    var _this;

    _classCallCheck(this, Swiper);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Swiper).call(this, props));
    _this.contain = void 0;
    _this.pagination = void 0;
    _this.MOVE_ING = void 0;
    _this.MOVE_ING_END = void 0;
    _this.disableIosBounce = void 0;
    _this.MOVE_ING_OUT_END = void 0;
    _this.TouchInfo = void 0;
    _this.startInfo = void 0;
    _this.width = void 0;
    _this.length = void 0;
    _this.loop = void 0;

    _this.start = function (e) {
      var able = _this.props.able;
      if (!able) return;
      _this.TouchInfo = {};
      var touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      var _touch = touch,
          pageX = _touch.pageX,
          pageY = _touch.pageY;
      _this.startInfo = {
        sX0: pageX,
        sY0: pageY
      };
      _this.TouchInfo = {
        sX: pageX,
        move: null,
        sY: pageY
      };
      _this.MOVE_ING = true;
      _this.MOVE_ING_OUT_END = false;
      _this.disableIosBounce && _this.disableIosBounce();
    };

    _this.move = function (e) {
      var _this$props = _this.props,
          preventDefault = _this$props.preventDefault,
          able = _this$props.able,
          loop = _this$props.loop,
          bounce = _this$props.bounce,
          touchSlide = _this$props.touchSlide;
      if (!_this.MOVE_ING || _this.MOVE_ING_END || !able) return;
      var touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      var _this$TouchInfo = _this.TouchInfo,
          sX = _this$TouchInfo.sX,
          sY = _this$TouchInfo.sY,
          move = _this$TouchInfo.move;
      var _touch2 = touch,
          pageX = _touch2.pageX,
          pageY = _touch2.pageY,
          clientY = _touch2.clientY;

      var inBody = _util.default.isInClient(pageX, clientY);

      if (!inBody) {
        _this.end();

        _this.MOVE_ING_OUT_END = true;
        return;
      }

      var mx = pageX - sX;
      var my = pageY - sY;
      var slideX;

      if (touchSlide) {
        slideX = _util.default.getTranslateInfo(_this.contain, 1);

        if (!loop) {
          var max = _this.width / 3;
          if (slideX > 0) mx = (max - slideX) * mx / max;
          if (slideX < -(_this.length - 1) * _this.width) mx = ((_this.length - 1) * _this.width + max + slideX) * mx / max;
        }

        slideX = slideX + mx;

        if (!loop && !bounce) {
          if (slideX > 0) slideX = 0;
          if (slideX < -(_this.length - 1) * _this.width) slideX = -(_this.length - 1) * _this.width;
        }

        if (preventDefault) {
          if (Math.abs(mx) > Math.abs(my) && move === null || move) {
            _this.TouchInfo.move = true;

            _util.default.setTranslateInfo(_this.contain, {
              x: slideX
            });

            _this.disableIosBounce = _util.default.disableIosBounce(_this.contain);
            document.body.classList.add("".concat(prefixCls, "__ovhide"));
          } else {
            _this.TouchInfo.move = false;
          }
        } else {
          _util.default.setTranslateInfo(_this.contain, {
            x: slideX
          });
        }
      }

      _this.TouchInfo = _objectSpread({}, _this.TouchInfo, {}, {
        sX: pageX,
        sY: pageY
      });
    };

    _this.end = function () {
      var _this$props2 = _this.props,
          able = _this$props2.able,
          loop = _this$props2.loop;
      if (!able || _this.MOVE_ING_OUT_END) return;
      var sX = _this.TouchInfo.sX;
      var sX0 = _this.startInfo.sX0;
      var activeIndex = _this.state.activeIndex;
      _this.MOVE_ING_END = true;

      if (Math.abs((sX - sX0) / _this.width) > 0.33) {
        if (sX - sX0 < 0) activeIndex++;else activeIndex--;

        if (!loop) {
          if (activeIndex < 0) activeIndex = 0;
          if (activeIndex > _this.length - 1) activeIndex = _this.length - 1;
        }
      }

      _this.disableIosBounce && _this.disableIosBounce();
      document.body.classList.remove("".concat(prefixCls, "__ovhide"));

      _this.moveTo(activeIndex);
    };

    _this.state = {
      children: children,
      style: {},
      activeIndex: 1
    };
    return _this;
  }

  _createClass(Swiper, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getChildren();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props, prevProps !== this.props) this.getChildren();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.disableIosBounce && this.disableIosBounce();
      this.loop && clearInterval(this.loop);
    }
  }, {
    key: "getChildren",
    value: function getChildren() {
      var _this2 = this;

      var _this$props3 = this.props,
          children = _this$props3.children,
          activeIndex = _this$props3.activeIndex,
          loop = _this$props3.loop,
          gap = _this$props3.gap,
          gapPadding = _this$props3.gapPadding;
      var style = this.state.style;
      if (children && Array.isArray(children)) children = children.filter(function (item) {
        if (item) return item;
      });

      if (children && Array.isArray(children) && children.length > 1) {
        var firstChildren = children[0];
        var listChildren = children[children.length - 1];
        loop && (children = [listChildren].concat(_toConsumableArray(children), [firstChildren]));
        var width = this.contain.parentNode.offsetWidth + gap;
        var padding = gap || 0;
        var paddingGap = gapPadding || 0;
        this.width = width; // @ts-ignore

        var length = this.length = children.length;
        children = _react.default.Children.map(children, function (item, index) {
          // @ts-ignore
          var props = _objectSpread({}, item.props);

          if (!props.style) props.style = {};

          if (padding > 0) {
            if (!loop && index === 0) props.style.marginLeft = -paddingGap;
            if (!loop && index === _this2.length - 1) props.style.marginRight = -paddingGap;
            return _react.default.createElement('div', {
              style: {
                width: width,
                paddingRight: padding
              }
            }, item);
          }

          props.style.width = width; // @ts-ignore

          return _react.default.cloneElement(item, props);
        });
        if (!loop) activeIndex = activeIndex - 1;

        _util.default.setTranslateInfo(this.contain, {
          x: -width * activeIndex
        });

        style = _objectSpread({}, style, {}, {
          width: length * width
        });
      }

      this.setState({
        children: children,
        activeIndex: activeIndex,
        style: style
      });
      this.autoPlay();
    }
  }, {
    key: "autoPlay",
    value: function autoPlay() {
      var _this3 = this;

      var _this$props4 = this.props,
          autoPlayIntervalTime = _this$props4.autoPlayIntervalTime,
          autoPlay = _this$props4.autoPlay,
          loop = _this$props4.loop,
          animationDuration = _this$props4.animationDuration,
          direction = _this$props4.direction;

      if (autoPlay && loop && this.length > 1) {
        this.loop && clearInterval(this.loop);
        this.loop = setInterval(function () {
          if (_this3.MOVE_ING) return;
          var activeIndex = _this3.state.activeIndex;
          if (direction === 'right') activeIndex -= 1;else activeIndex += 1;

          _this3.moveTo(activeIndex, animationDuration);
        }, autoPlayIntervalTime);
      }
    }
  }, {
    key: "moveTo",
    value: function moveTo(activeIndex) {
      var _this4 = this;

      var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 150;
      var _this$props5 = this.props,
          loop = _this$props5.loop,
          touchSlide = _this$props5.touchSlide,
          onChange = _this$props5.onChange;

      _util.default.setTranslateInfo(this.contain, {
        x: -this.width * activeIndex,
        time: touchSlide ? time : 0
      });

      this.setState({
        activeIndex: activeIndex
      });

      if ((activeIndex === 0 || activeIndex === this.length - 1) && loop) {
        setTimeout(function () {
          if (activeIndex === 0) activeIndex = _this4.length - 2;else activeIndex = 1;

          _util.default.setTranslateInfo(_this4.contain, {
            x: -_this4.width * activeIndex
          });

          onChange && onChange(activeIndex);

          _this4.setState({
            activeIndex: activeIndex
          });
        }, time + 50);
      } else {
        onChange && onChange(activeIndex);
      }

      setTimeout(function () {
        _this4.MOVE_ING = false;
        _this4.MOVE_ING_END = false;
      }, time);
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var _this$props6 = this.props,
          className = _this$props6.className,
          direction = _this$props6.direction,
          showPagination = _this$props6.showPagination,
          loop = _this$props6.loop,
          overflow = _this$props6.overflow;
      var _this$state = this.state,
          children = _this$state.children,
          activeIndex = _this$state.activeIndex,
          style = _this$state.style;
      var cls = (0, _classnames3.default)(prefixCls, className, _defineProperty({}, "".concat(prefixCls, "--overshow"), overflow));
      var contain = (0, _classnames3.default)("".concat(prefixCls, "__contain"), "".concat(prefixCls, "__contain--").concat(direction));
      var dots = [];
      var dotIndex = activeIndex;

      if (children && children.slice) {
        if (loop) {
          dots = children.slice(1, children.length - 1);
          if (dotIndex < 1) dotIndex = 1;
          if (dotIndex > dots.length) dotIndex = dots.length;
        } else {
          dots = children;
        }
      }

      return _react.default.createElement("div", {
        className: cls
      }, _react.default.createElement("div", {
        className: contain,
        style: style,
        ref: function ref(dom) {
          _this5.contain = dom;
        },
        onTouchStart: this.start,
        onMouseDown: this.start,
        onTouchMove: this.move,
        onMouseMove: this.move,
        onTouchEnd: this.end,
        onMouseUp: this.end
      }, children), showPagination && dots.length > 0 && _react.default.createElement("div", {
        ref: function ref(_ref) {
          return _this5.pagination = _ref;
        },
        className: "".concat(prefixCls, "__pagination")
      }, dots.map(function (item, index) {
        var key = item.key;
        return _react.default.createElement("p", {
          key: key,
          onClick: function onClick() {
            _this5.moveTo(index + 1);
          },
          className: (0, _classnames3.default)("".concat(prefixCls, "__dot"), _defineProperty({}, "".concat(prefixCls, "__dot--active"), dotIndex === index + (loop ? 1 : 0)))
        });
      })));
    }
  }]);

  return Swiper;
}(_react.PureComponent);

exports.default = Swiper;
Swiper.defaultProps = {
  direction: 'left',
  activeIndex: 1,
  loop: true,
  bounce: true,
  able: true,
  overflow: false,
  preventDefault: false,
  autoPlay: false,
  touchSlide: true,
  showPagination: false,
  gap: 0,
  gapPadding: 0,
  autoPlayIntervalTime: 4000,
  animationDuration: 300
};