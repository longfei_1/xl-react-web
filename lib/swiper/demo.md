# Swiper 列表项

## 基本用法 (移动端查看，效果会好些)

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div style={{ padding: 10 }}>
        <Swiper>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 自动滚动、显示 pagination、设置 activeIndex

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div style={{ padding: 10 }}>
        <Swiper autoPlay showPagination activeIndex={3}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 滚动方向、带有缝隙、禁止 touch

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div style={{ padding: 10 }}>
        <Swiper direction="right" autoPlay gap={20} able={false} showPagination activeIndex={2}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 不循环、带有 bounce 效果 (移动端查看，效果会好些)

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div style={{ padding: 10 }}>
        <Swiper bounce loop={false} showPagination activeIndex={2}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 不循环、去掉拖拽

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div style={{ padding: 10 }}>
        <Swiper touchSlide={false} loop={false} showPagination activeIndex={2}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 空隙、露出两边、轮播

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div className="item-swiper-one">
        <Swiper autoPlay overflow gap={20}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 空隙、露出两边、不轮播

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div className="item-swiper-two">
        <Swiper loop={false} overflow gap={20} gapPadding={40}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
          <div style={{ height: 100, background: 'black', color: '#fff' }}>这是个常规滚动，滚动的第三页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 空隙、不轮播

```jsx
import { Swiper, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <div className="item-swiper-two">
        <Swiper loop={false} overflow gap={20} gapPadding={40}>
          <div style={{ height: 100, background: 'blue', color: '#fff' }}>这是个常规滚动，滚动的第一页</div>
          <div style={{ height: 100, background: 'DarkOrange', color: '#000' }}>这是个常规滚动，滚动的第二页</div>
        </Swiper>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性                 | 类型    | 默认值 | 说明                           |
| :------------------- | :------ | :----- | :----------------------------- |
| direction            | string  | left   | 方向 left/right                |
| className            | string  | -      | 类名称                         |
| activeIndex          | number  | 1      | 初始位置                       |
| loop                 | boolean | true   | 是否循环                       |
| bounce               | boolean | false  | 是否有反弹效果(loop 为 false)  |
| touchSlide           | boolean | false  | 不能拖拽(直接切换)             |
| overflow             | boolean | false  | 超出是否展示                   |
| preventDefault       | boolean | false  | 横向拖拽禁止竖直拖拽(反之亦然) |
| able                 | boolean | false  | 禁止拖拽                       |
| autoPlay             | boolean | false  | 自动播放                       |
| showPagination       | boolean | false  | 显示 pagination                |
| gap                  | number  | 0      | 缝隙的值                       |
| gapPadding           | number  | 0      | 两边的距离(loop 为 false)      |
| autoPlayIntervalTime | number  | 4000   | 自动轮播间隔时间               |
| animationDuration    | number  | 300    | 自动轮播滑动时间               |
| onChange             | -       | -      | 切换回调函数                   |
