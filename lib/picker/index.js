"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _util = _interopRequireDefault(require("../util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-picker");

var Picker =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Picker, _PureComponent);

  function Picker(props) {
    var _this2;

    _classCallCheck(this, Picker);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Picker).call(this, props));
    _this2.contain = void 0;
    _this2.list = void 0;
    _this2.target = void 0;
    _this2.TouchInfo = void 0;
    _this2.height = void 0;
    _this2.hasBind = false;
    _this2.state = {
      index: 0,
      position: 0
    };
    return _this2;
  }

  _createClass(Picker, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.initEvent();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props, prevProps !== this.props) this.initEvent();
    }
  }, {
    key: "initEvent",
    value: function initEvent() {
      var _this3 = this;

      var contain = this.contain,
          list = this.list;
      var _Util$TOUCH_EVENT_NAM = _util.default.TOUCH_EVENT_NAME,
          start = _Util$TOUCH_EVENT_NAM.start,
          move = _Util$TOUCH_EVENT_NAM.move,
          end = _Util$TOUCH_EVENT_NAM.end,
          option = _Util$TOUCH_EVENT_NAM.option;
      var activeIndex = this.getIndex();
      this.moveTo(activeIndex);
      setTimeout(function () {
        if (_this3.hasBind) return;
        _this3.hasBind = true;
        contain.addEventListener(start, function (e) {
          _this3.TouchInfo = {};
          var touch = e.touches && e.touches[0];
          if (!touch) touch = e;
          var _touch = touch,
              pageY = _touch.pageY;
          _this3.TouchInfo = {
            sY: pageY
          };
          if (_this3.props.disable) return;
          document.addEventListener(move, touchmove, option);
          document.addEventListener(end, touchend, option);
        }, option);
      }, 0);

      var _this = this;

      function touchmove(e) {
        var _this$props = _this.props,
            data = _this$props.data,
            preventDefault = _this$props.preventDefault;
        var touch = e.touches && e.touches[0];
        if (!touch) touch = e;
        var sY = _this.TouchInfo.sY;
        var _touch2 = touch,
            pageX = _touch2.pageX,
            pageY = _touch2.pageY,
            clientY = _touch2.clientY;

        var inBody = _util.default.isInClient(pageX, clientY);

        if (!inBody) {
          touchend();
          return;
        }

        if (preventDefault && e.preventDefault) e.preventDefault();
        var my = pageY - sY;
        var slideY;
        slideY = _util.default.getTranslateInfo(list, 2);
        var max = _this.height * 2;
        var start = -_this.height * 3;
        var last = -(data.length + 2) * _this.height;
        if (slideY > start) my = (start + max - slideY) * my / max;
        if (slideY < last) my = (max + slideY - last) * my / max;
        slideY += my;

        _this.setState({
          position: slideY
        });

        _util.default.setTranslateInfo(list, {
          y: slideY
        });

        _this.TouchInfo = _objectSpread({}, _this.TouchInfo, {}, {
          sY: pageY
        });
      }

      function touchend() {
        var _this$props2 = _this.props,
            data = _this$props2.data,
            onChange = _this$props2.onChange;
        var slideY;
        slideY = _util.default.getTranslateInfo(list, 2);
        var start = -_this.height * 3;
        var last = -(data.length + 2) * _this.height;
        slideY = Math.round(slideY / _this.height) * _this.height;
        if (slideY > start) slideY = start;
        if (slideY < last) slideY = last;
        var index = -slideY / _this.height - 3;
        onChange && onChange(data[index]);

        _this.setState({
          index: index,
          position: slideY
        });

        _util.default.setTranslateInfo(list, {
          y: slideY,
          time: 150
        }); // @ts-ignore


        document.removeEventListener(move, touchmove, option); // @ts-ignore

        document.removeEventListener(end, touchend, option);
      }
    }
  }, {
    key: "moveTo",
    value: function moveTo(index) {
      var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var list = this.list,
          target = this.target;
      this.height = target.offsetHeight;
      if (_util.default.IS_IN_JEST) this.height = 24;
      this.setState({
        index: index,
        position: -this.height * (index + 3)
      });
      index = index + 3;

      _util.default.setTranslateInfo(list, {
        y: -this.height * index,
        time: time
      });
    }
  }, {
    key: "getIndex",
    value: function getIndex() {
      var _this$props3 = this.props,
          _this$props3$value = _this$props3.value,
          value = _this$props3$value === void 0 ? '' : _this$props3$value,
          data = _this$props3.data,
          _this$props3$keyName = _this$props3.keyName,
          keyName = _this$props3$keyName === void 0 ? '' : _this$props3$keyName,
          _this$props3$keyValue = _this$props3.keyValue,
          keyValue = _this$props3$keyValue === void 0 ? '' : _this$props3$keyValue,
          _this$props3$renderKe = _this$props3.renderKey,
          renderKey = _this$props3$renderKe === void 0 ? '' : _this$props3$renderKe;
      var activeIndex = 0;
      data.some(function (item, index) {
        if (_typeof(item) === 'object') {
          if (item[keyName] === keyValue || item[renderKey] === value) {
            activeIndex = index;
            return true;
          }
        } else {
          if (item === value) {
            activeIndex = index;
            return true;
          }
        }
      });
      return activeIndex;
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var _this$props4 = this.props,
          className = _this$props4.className,
          data = _this$props4.data,
          keyName = _this$props4.keyName,
          renderKey = _this$props4.renderKey;
      var position = this.state.position;
      var cls = (0, _classnames.default)(prefixCls, className);
      var contain = (0, _classnames.default)("".concat(prefixCls, "__contain"));
      return _react.default.createElement("div", {
        ref: function ref(dom) {
          _this4.contain = dom;
        },
        className: cls
      }, _react.default.createElement("div", {
        className: "".concat(prefixCls, "__mask ").concat(prefixCls, "__mask--top")
      }), _react.default.createElement("div", {
        ref: function ref(dom) {
          _this4.target = dom;
        },
        className: "".concat(prefixCls, "__mid")
      }), _react.default.createElement("div", {
        className: "".concat(prefixCls, "__mask ").concat(prefixCls, "__mask--bottom")
      }), _react.default.createElement("div", {
        className: contain,
        ref: function ref(dom) {
          _this4.list = dom;
        }
      }, data.map(function (item, index) {
        var key = item;

        if (_typeof(item) === 'object') {
          key = item[keyName] != null ? item[keyName] : item[renderKey] ? item[renderKey] : index;
          item = renderKey ? item[renderKey] : '';
        }

        var rotateX = Math.abs(index * _this4.height + _this4.height * 3 + position) / _this4.height;

        var style = {};

        if (rotateX && rotateX <= 2) {
          style = _defineProperty({}, _util.default.transform, "rotateX(".concat(rotateX * 25, "deg)"));
        }

        return _react.default.createElement("div", {
          key: key,
          style: style
        }, _react.default.createElement("p", null, item));
      })));
    }
  }]);

  return Picker;
}(_react.PureComponent);

exports.default = Picker;
Picker.defaultProps = {
  renderKey: 'name',
  preventDefault: false,
  disable: false,
  keyName: 'id'
};