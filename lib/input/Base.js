"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames3 = _interopRequireDefault(require("classnames"));

var _icon = _interopRequireDefault(require("../icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-input");

var Base =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Base, _PureComponent);

  function Base(props) {
    var _this;

    _classCallCheck(this, Base);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Base).call(this, props));
    _this.input = void 0;
    _this.onBlurTimeout = void 0;
    _this.blurFromClear = void 0;

    _this.onFocus = function (e) {
      _this.setState({
        focused: true
      });

      var onFocus = _this.props.onFocus;

      if (typeof onFocus === 'function') {
        onFocus(e.target.value);
      }
    };

    _this.focus = function () {
      _this.input.focus();
    };

    _this.blur = function () {
      _this.input.blur();
    };

    _this.onBlur = function (e) {
      var onBlur = _this.props.onBlur;
      var value = e.target.value;
      _this.onBlurTimeout = setTimeout(function () {
        if (!_this.blurFromClear && document.activeElement !== _this.input) {
          _this.setState({
            focused: false
          });

          if (typeof onBlur === 'function') {
            onBlur(value);
          }
        }

        _this.blurFromClear = false;
      }, 200);
    };

    _this.onChange = function (e) {
      var onChange = _this.props.onChange;

      if (!_this.state.focused) {
        _this.setState({
          focused: true
        });
      }

      _this.setState({
        value: e.target.value
      });

      if (onChange) {
        onChange(e.target.value);
      }
    };

    _this.onClear = function () {
      var isOnComposition = _this.state.isOnComposition;
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          onClear = _this$props.onClear;
      _this.blurFromClear = true;

      _this.setState({
        value: ''
      });

      !isOnComposition && _this.focus();
      typeof onChange === 'function' && onChange('');
      typeof onClear === 'function' && onClear('');
    };

    _this.handleComposition = function (e) {
      var _this$props2 = _this.props,
          onCompositionStart = _this$props2.onCompositionStart,
          onCompositionUpdate = _this$props2.onCompositionUpdate,
          onCompositionEnd = _this$props2.onCompositionEnd,
          onChange = _this$props2.onChange;

      if (e.type === 'compositionstart') {
        _this.setState({
          isOnComposition: true
        });

        if (typeof onCompositionStart === 'function') {
          onCompositionStart(e);
        }
      }

      if (e.type === 'compositionupdate') {
        if (typeof onCompositionUpdate === 'function') {
          onCompositionUpdate(e);
        }
      }

      if (e.type === 'compositionend') {
        var value = e.target.value; // composition is end

        _this.setState({
          isOnComposition: false
        });

        if (typeof onCompositionEnd === 'function') {
          onCompositionEnd(e);
        }

        if (typeof onChange === 'function') {
          onChange(value);
        }
      }
    };

    _this.state = {
      focused: false,
      isOnComposition: false,
      value: props.defaultValue || props.value || ''
    };
    return _this;
  }

  _createClass(Base, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props3 = this.props,
          autoFocus = _this$props3.autoFocus,
          onFocus = _this$props3.onFocus;
      var focused = this.state.focused;

      if (autoFocus || focused) {
        this.input.focus();

        if (typeof onFocus === 'function') {
          onFocus(this.input.value);
        }
      }
    }
  }, {
    key: "UNSAFE_componentWillReceiveProps",
    value: function UNSAFE_componentWillReceiveProps(nextProps) {
      if ('value' in nextProps) {
        this.setState({
          value: nextProps.value
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.onBlurTimeout) {
        clearTimeout(this.onBlurTimeout);
        this.onBlurTimeout = null;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _classnames,
          _this2 = this;

      var _this$props4 = this.props,
          className = _this$props4.className,
          disabled = _this$props4.disabled,
          clearable = _this$props4.clearable,
          readOnly = _this$props4.readOnly,
          type = _this$props4.type,
          onClear = _this$props4.onClear,
          rest = _objectWithoutProperties(_this$props4, ["className", "disabled", "clearable", "readOnly", "type", "onClear"]);

      var _this$state = this.state,
          value = _this$state.value,
          focused = _this$state.focused;
      var showClearIcon = clearable && 'value' in this.props;
      var cls = (0, _classnames3.default)(prefixCls, className, (_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls, "--disabled"), disabled), _defineProperty(_classnames, "".concat(prefixCls, "--focus"), focused), _defineProperty(_classnames, "".concat(prefixCls, "--clearable"), showClearIcon), _defineProperty(_classnames, "".concat(prefixCls, "--readonly"), readOnly), _classnames));
      var valueProps = {};

      if ('value' in this.props) {
        valueProps.value = value;
      }

      var IconClass = (0, _classnames3.default)("".concat(prefixCls, "__clear"), _defineProperty({}, "".concat(prefixCls, "__clear--show"), focused && value && value.length > 0));
      return _react.default.createElement("div", {
        className: cls
      }, _react.default.createElement("input", _extends({}, rest, valueProps, {
        autoComplete: "off",
        ref: function ref(_ref) {
          return _this2.input = _ref;
        },
        type: type,
        disabled: disabled,
        onChange: this.onChange,
        onBlur: this.onBlur,
        onFocus: this.onFocus,
        onCompositionStart: function onCompositionStart(e) {
          _this2.handleComposition(e);
        },
        onCompositionUpdate: function onCompositionUpdate(e) {
          _this2.handleComposition(e);
        },
        onCompositionEnd: function onCompositionEnd(e) {
          _this2.handleComposition(e);
        }
      })), _react.default.createElement(_icon.default, {
        type: "wrongRoundFill",
        className: IconClass,
        onClick: function onClick() {
          return _this2.onClear();
        }
      }));
    }
  }]);

  return Base;
}(_react.PureComponent);

exports.default = Base;
Base.defaultProps = {
  type: 'text',
  clearable: true,
  readOnly: false,
  disabled: false
};