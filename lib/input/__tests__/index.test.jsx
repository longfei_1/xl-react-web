import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Base from '../index';

const prefix = `.${__PREFIX__}-input`;

describe('Input base', () => {

  it('base input with type = "text", className = "test-input", placeholder = "test" ,style={height:1}', () => {
    const wrapper = render(<Base type='text' className="test-input" placeholder='test' style={{ height: 1 }}/>);
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  it('base input with disable', () => {
    const wrapper = render(<Base type='text' className="test-input" placeholder='test' disabled={true}
                                 style={{ height: 1 }}/>);
    expect(toJson(wrapper))
      .toMatchSnapshot();
  });

  it('base input with value = "test", focus , blur , clear', async () => {
    const id = 'test';
    const value = 'test';
    const wrapper = mount(<Base type='text' id={id} value={value} clearable/>);
    const target = wrapper.find(`input`);
    const instance = target.instance();
    expect(target.prop('value'))
      .toBe(value);
    instance.focus();
    expect(document.activeElement.getAttribute('id'))
      .toBe(id);
    const icon = wrapper.find(`i${prefix}__clear`);
    expect(icon)
      .not
      .toBeNull();
    icon.simulate('click');
    expect(target.instance().value)
      .toEqual('');
  });

  it('base input with value = "test", autoFocus ,change , composition', async () => {
    const onFocus = jest.fn();
    const onChange = jest.fn();
    const onBlur = jest.fn();
    const onClear = jest.fn();
    const onCompositionStart = jest.fn();
    const onCompositionUpdate = jest.fn();
    const onCompositionEnd = jest.fn();
    const wrapper = mount(
      <Base type='text'
            value={'1'}
            onFocus={onFocus}
            onBlur={onBlur}
            onClear={onClear}
            onCompositionStart={onCompositionStart}
            onCompositionUpdate={onCompositionUpdate}
            onCompositionEnd={onCompositionEnd}
            onChange={onChange}
            autoFocus={1}/>
    );
    expect.assertions(7);
    await wait(50);
    expect(onFocus)
      .toBeCalled();
    const input = wrapper.find('input');
    let instance = wrapper.instance();
    input.simulate('change', { target: { value: '2' } });
    expect(onChange).toBeCalled()
    expect(onChange.mock.calls[0][0])
      .toEqual('2');
    wrapper.find(`i${prefix}__clear`).simulate('click');
    expect(onClear).toBeCalled()
    input.simulate('compositionstart');
    expect(onCompositionStart).toBeCalled();
    input.simulate('compositionupdate');
    expect(onCompositionUpdate).toBeCalled();
    input.simulate('compositionend');
    expect(onCompositionEnd).toBeCalled();
  });


});

describe('Input Text', () => {

  it('Text input with value = "test", autoFocus ', async () => {
    const onFocus = jest.fn();
    const wrapper = mount(
      <Base type='textarea'
            value={'125'}
            rows={3}
            onFocus={onFocus}
            autoFocus={1}/>
    );
    expect.assertions(2);
    expect(toJson(wrapper))
      .toMatchSnapshot();
    await wait(50);
    expect(onFocus)
      .toBeCalled();


  });

  it('Text input with clearable,row=2,maxlength, autoHeight,change,clear', async () => {
    const onFocus = jest.fn();
    const onChange = jest.fn();
    const onClear = jest.fn();
    const wrapper = mount(
      <Base type='textarea'
            defaultValue={'125'}
            rows={2}
            clearable={1}
            autoHeight={true}
            onChange={onChange}
            onClear={onClear}
            maxLength={60}
            autoFocus={1}/>
    );
    const input = wrapper.find('textarea');
    let instance  = input.instance();
    expect(toJson(wrapper))
      .toMatchSnapshot();
    expect(onFocus)
      .not
      .toBeCalled();
    input.simulate('change', { target: { value: '562' } });
    expect(onChange)
      .toBeCalled();
    expect(onChange.mock.calls[0][0])
      .toEqual('562');
    let lengthIns = wrapper.find(`div${prefix}__length`);
    lengthIns = lengthIns.instance();
    const test = document.querySelector(`div${prefix}__length`)
    expect(lengthIns.innerHTML)
      .toEqual('3/60');
    wrapper.find(`i${prefix}__clear`)
      .simulate('click');
    expect(onClear)
      .toBeCalled();
    expect(instance.value)
      .toEqual('');

  });

  it('Text input with onCompositionStart onCompositionUpdate onCompositionEnd', async () => {
    const onFocus = jest.fn();
    const onChange = jest.fn();
    const onBlur = jest.fn();
    const onClear = jest.fn();
    const onCompositionStart = jest.fn();
    const onCompositionUpdate = jest.fn();
    const onCompositionEnd = jest.fn();
    const wrapper = mount(
      <Base type='textarea'
            defaultValue={'125'}
            rows={2}
            onFocus={onFocus}
            onBlur={onBlur}
            onClear={onClear}
            onCompositionStart={onCompositionStart}
            onCompositionUpdate={onCompositionUpdate}
            onCompositionEnd={onCompositionEnd}
            onChange={onChange}
            autoFocus={1}/>
    );
    const input = wrapper.find('textarea');
    input.simulate('change', { target: { value: '2' } });
    expect(onChange)
      .toBeCalled();
    expect(onChange.mock.calls[0][0])
      .toEqual('2');
    input.simulate('compositionstart');
    expect(onCompositionStart)
      .toBeCalled();
    input.simulate('compositionupdate');
    expect(onCompositionUpdate)
      .toBeCalled();
    input.simulate('compositionend');
    expect(onCompositionEnd)
      .toBeCalled();

  });


});

describe('Input number/price/card', () => {

  it('Input number with autoFocus,clear', async () => {
    const onFocus = jest.fn();
    const wrapper = mount(
      <Base type='number'
            defaultValue={'524'}
            clearable ={1}
            onFocus={onFocus}
            autoFocus={1}/>
    );
    const valueSpan = wrapper.find(`${prefix}__value span`)
    const clearBtn = wrapper.find(`i${prefix}__clear`)
    const instance = valueSpan.instance()
    expect.assertions(6);
    expect(toJson(wrapper))
      .toMatchSnapshot();
    expect(instance.innerHTML).toBe('524')
    expect(onFocus)
      .toBeCalled();
    await wait(300, () => {
      const keyBox = document.querySelector(`${prefix}__types-key-box`)
      expect(keyBox.classList.contains('action')).toBe(true)
      clearBtn.simulate('click')
      expect(instance.innerHTML).toBe('')
      const keyList = document.querySelectorAll(`${prefix}__key-box p`)[16]
      expect(keyList.querySelector('span').innerHTML).toBe('')
    });
  });

  it('Input price with focus,change,blue,delete', async () => {
    const keyCon = document.querySelector(`${prefix}__types-key-box`)
    if(keyCon){
      keyCon.parentNode.removeChild(keyCon)
    }
    const onFocus = jest.fn();
    const onBlur = jest.fn();
    const onChange = jest.fn();
    const wrapper = mount(
      <Base type='price'
            defaultValue={'524'}
            onFocus={onFocus}
            onBlur={onBlur}
            onChange={onChange}/>
    );
    const valueSpan = wrapper.find(`${prefix}__value span`);
    const target = wrapper.find(`${prefix}__value span`);
    const instance = valueSpan.instance();
    target.simulate('click');
    expect.assertions(7);
    expect(onFocus)
      .toBeCalled();
    await wait(300, async () => {
      const keyBox = document.querySelector(`${prefix}__types-key-box`);
      expect(keyBox.classList.contains('action'))
        .toBe(true);
      const key1 = document.querySelectorAll(`${prefix}__key-box p`)[1];
      const key0 = document.querySelectorAll(`${prefix}__key-box p`)[17];
      const key6 = document.querySelectorAll(`${prefix}__key-box p`)[8];
      const key = document.querySelectorAll(`${prefix}__key-box p`)[9];
      const key8 = document.querySelectorAll(`${prefix}__key-box p`)[12];
      const del = document.querySelectorAll(`${prefix}__key-box p`)[4];
      key1.click();
      key.click();
      key0.click();
      key6.click();
      key8.click();
      expect(instance.innerHTML)
        .toBe('5241.068');
      del.click();
      del.click();
      expect(instance.innerHTML)
        .toBe('5241.0');
      document.body.click();
    });
    await wait(400,()=>{
      const keyBox2 = document.querySelector(`${prefix}__types-key-box`);
      expect(onChange)
        .toBeCalled();
      expect(keyBox2)
        .toBeNull();
      expect(onBlur)
        .toBeCalled();
    })
  });

  it('Input price with focus,onEnter', async () => {
    const keyCon = document.querySelector(`${prefix}__types-key-box`)
    if(keyCon){
      keyCon.parentNode.removeChild(keyCon)
    }
    const onFocus = jest.fn();
    const onEnter = jest.fn();
    const onBlur = jest.fn();
    const onChange = jest.fn();
    const wrapper = mount(
      <Base type='card'
            defaultValue={'854e54x'}
            onFocus={onFocus}
            onEnter={onEnter}
            onBlur={onBlur}
            onChange={onChange}/>
    );
    const valueSpan = wrapper.find(`${prefix}__value span`);
    const target = wrapper.find(`${prefix}__value span`);
    const instance = valueSpan.instance();
    target.simulate('click');
    expect.assertions(7);
    expect(onFocus)
      .toBeCalled();
    await wait(300, async () => {
      const keyBox = document.querySelector(`${prefix}__types-key-box`);
      expect(keyBox.classList.contains('action'))
        .toBe(true);
      const keys = document.querySelectorAll(`${prefix}__key-box p`)
      const key1 = keys[1];
      const key0 = keys[17];
      const key6 = keys[8];
      const key8 = keys[12];
      const keyX = keys[16];
      const keyEnter = keys[14];
      key1.click();
      keyX.click();
      key0.click();
      key6.click();
      key8.click();
      key6.click();
      expect(onChange)
        .toBeCalled();
      expect(instance.innerHTML)
        .toBe('85454x10686');
      keyEnter.click();
    });
    await wait(600,()=>{
      const keyBox2 = document.querySelector(`${prefix}__types-key-box`);
      expect(keyBox2)
        .toBeNull();
      expect(onBlur)
        .toBeCalled();
      expect(onEnter.mock.calls[0][0])
        .toBe('85454x10686');
    })
  });

});
