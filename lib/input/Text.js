"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames3 = _interopRequireDefault(require("classnames"));

var _icon = _interopRequireDefault(require("../icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-input");
var regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]|\n/g;

var countSymbols = function countSymbols() {
  var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return text.replace(regexAstralSymbols, '_').length;
};

var Text =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Text, _PureComponent);

  function Text(props) {
    var _this;

    _classCallCheck(this, Text);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Text).call(this, props));
    _this.input = void 0;

    _this.handleComposition = function (e) {
      var _this$props = _this.props,
          onCompositionStart = _this$props.onCompositionStart,
          onCompositionUpdate = _this$props.onCompositionUpdate,
          onCompositionEnd = _this$props.onCompositionEnd,
          onChange = _this$props.onChange;

      if (e.type === 'compositionstart') {
        if (typeof onCompositionStart === 'function') {
          onCompositionStart(e);
        }
      }

      if (e.type === 'compositionupdate') {
        if (typeof onCompositionUpdate === 'function') {
          onCompositionUpdate(e);
        }
      }

      if (e.type === 'compositionend') {
        var value = e.target.value;

        if (typeof onCompositionEnd === 'function') {
          onCompositionEnd(e);
        }

        if (typeof onChange === 'function') {
          onChange(value);
        }
      }
    };

    _this.onFocus = function (e) {
      setTimeout(function () {
        _this.setState({
          focused: true
        });
      }, 0);
      var onFocus = _this.props.onFocus;

      if (typeof onFocus === 'function') {
        onFocus(e.target.value, e);
      }
    };

    _this.onBlur = function (e) {
      setTimeout(function () {
        _this.setState({
          focused: false
        });
      }, 200);
      var onBlur = _this.props.onBlur;

      if (typeof onBlur === 'function') {
        onBlur(e.target.value, e);
      }
    };

    _this.onClear = function (e) {
      _this.input.value = '';

      _this.setState({
        length: 0,
        value: ''
      });

      _this.input.focus();

      var onClear = _this.props.onClear;

      if (typeof onClear === 'function') {
        onClear(e.target.value, e);
      }
    };

    _this.onChange = function (e) {
      var onChange = _this.props.onChange;
      var value = e.target.value;
      var length = countSymbols(value);

      _this.setState({
        length: length,
        value: value
      });

      if (typeof onChange === 'function') {
        onChange(value, e);
      }
    };

    _this.state = {
      value: props.defaultValue || props.value || '',
      focused: false,
      lineHeight: null
    };
    return _this;
  }

  _createClass(Text, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var props = this.props,
          input = this.input;
      var autoFocus = props.autoFocus,
          autoHeight = props.autoHeight,
          onFocus = props.onFocus,
          maxLength = props.maxLength,
          _props$rows = props.rows,
          rows = _props$rows === void 0 ? 3 : _props$rows;

      if (autoFocus) {
        input.focus();
        input.setSelectionRange(input.value.length, input.value.length);

        if (typeof onFocus === 'function') {
          onFocus(input.value);
        }
      }

      if (autoHeight) {
        var hang = input.value.match(/\n/g);
        var lineHeight = input.scrollHeight / rows;

        if (hang && hang.length + 1 > rows) {
          lineHeight = input.scrollHeight / (hang.length + 1);
        }

        this.setState({
          lineHeight: lineHeight
        });
        input.style.height = input.scrollHeight + 'px';
      }

      if (maxLength) {
        var length = countSymbols(input.value);
        this.setState({
          length: length
        });
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var props = this.props,
          input = this.input,
          state = this.state;
      var autoHeight = props.autoHeight,
          _props$rows2 = props.rows,
          rows = _props$rows2 === void 0 ? 3 : _props$rows2;
      var lineHeight = state.lineHeight;

      if (autoHeight) {
        var hang = input.value.match(/\n/g);
        var row = rows;
        if (hang && hang.length + 1 > rows) row = hang.length + 1;
        input.style.height = row * lineHeight + 'px';
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      this.input.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.input.blur();
    }
  }, {
    key: "render",
    value: function render() {
      var _classnames,
          _this2 = this;

      var _this$props2 = this.props,
          className = _this$props2.className,
          disabled = _this$props2.disabled,
          readOnly = _this$props2.readOnly,
          onClear = _this$props2.onClear,
          autoHeight = _this$props2.autoHeight,
          maxLength = _this$props2.maxLength,
          clearable = _this$props2.clearable,
          rest = _objectWithoutProperties(_this$props2, ["className", "disabled", "readOnly", "onClear", "autoHeight", "maxLength", "clearable"]);

      var _this$state = this.state,
          focused = _this$state.focused,
          value = _this$state.value,
          length = _this$state.length;
      var cls = (0, _classnames3.default)(prefixCls, className, (_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls, "--disabled"), disabled), _defineProperty(_classnames, "".concat(prefixCls, "--clearable"), clearable), _defineProperty(_classnames, "".concat(prefixCls, "--readonly"), readOnly), _classnames));
      var IconClass = (0, _classnames3.default)("".concat(prefixCls, "__clear"), "".concat(prefixCls, "__clear--textarea"), _defineProperty({}, "".concat(prefixCls, "__clear--show"), focused && value && value.length > 0));
      return _react.default.createElement("div", {
        className: cls
      }, _react.default.createElement("textarea", _extends({}, rest, {
        ref: function ref(ele) {
          _this2.input = ele;
        },
        maxLength: maxLength,
        disabled: disabled,
        onChange: this.onChange,
        onFocus: this.onFocus,
        onBlur: this.onBlur,
        onCompositionStart: function onCompositionStart(e) {
          _this2.handleComposition(e);
        },
        onCompositionUpdate: function onCompositionUpdate(e) {
          _this2.handleComposition(e);
        },
        onCompositionEnd: function onCompositionEnd(e) {
          _this2.handleComposition(e);
        }
      })), clearable && _react.default.createElement(_icon.default, {
        type: "wrongRoundFill",
        className: IconClass,
        onClick: function onClick(e) {
          _this2.onClear(e);
        }
      }), maxLength && _react.default.createElement("div", {
        className: "".concat(prefixCls, "__length")
      }, "".concat(length, "/").concat(maxLength)));
    }
  }]);

  return Text;
}(_react.PureComponent);

exports.default = Text;
Text.defaultProps = {
  type: 'text',
  rows: 3,
  clearable: false,
  readOnly: false,
  disabled: false
};