"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _classnames4 = _interopRequireDefault(require("classnames"));

var _icon = _interopRequireDefault(require("../icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-input");

var Number =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Number, _PureComponent);

  function Number(props) {
    var _this2;

    _classCallCheck(this, Number);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Number).call(this, props));
    _this2.input = void 0;
    _this2.timeOut = false;
    _this2.modalContainer = void 0;

    _this2.onClick = function (e) {
      var _assertThisInitialize = _assertThisInitialized(_this2),
          onFocus = _assertThisInitialize.onFocus,
          onBlur = _assertThisInitialize.onBlur,
          onPaste = _assertThisInitialize.onPaste,
          showKeyBox = _assertThisInitialize.showKeyBox;

      e && e.preventDefault();
      e && e.stopPropagation();
      if (_this2.timeOut) return;
      _this2.timeOut = true;
      setTimeout(function () {
        _this2.timeOut = false;
      }, 250);
      var focused = _this2.state.focused;
      if (focused) return;
      onFocus(e);
      showKeyBox();
      document.addEventListener('click', onBlur);
      document.addEventListener('paste', onPaste);
    };

    _this2.onPaste = function (e) {
      var onPaste = _this2.props.onPaste;
      var value = _this2.state.value;
      e.preventDefault();
      e.stopPropagation();
      if (!(e.clipboardData && e.clipboardData.items)) return;
      var _e$clipboardData$item = e.clipboardData.items,
          items = _e$clipboardData$item === void 0 ? [] : _e$clipboardData$item;
      Object.keys(items).forEach(function (key) {
        var item = items[key];

        if (item.kind === 'string' && item.type === "text/plain") {
          item.getAsString(function (str) {
            if (onPaste) str = onPaste(str);
            value = value + str;

            _this2.setState({
              value: value
            });
          });
        }
      });
    };

    _this2.showKeyBox = function () {
      var modalContainer = document.createElement('div');
      _this2.modalContainer = modalContainer;
      modalContainer.setAttribute('class', "".concat(prefixCls, "__types-key-box"));
      document.body.appendChild(modalContainer);

      var _this = _assertThisInitialized(_this2);

      var type = _this2.props.type;

      var Com = _react.default.createElement("div", {
        className: "".concat(prefixCls, "__key-box")
      }, _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('', e);
        }
      }, _react.default.createElement("span", null, "-")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('1', e);
        }
      }, _react.default.createElement("span", null, "1")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('2', e);
        }
      }, _react.default.createElement("span", null, "2")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('3', e);
        }
      }, _react.default.createElement("span", null, "3")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('del', e);
        }
      }, _react.default.createElement(_icon.default, {
        type: "deletekey",
        fontSize: 20
      })), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('', e);
        }
      }, _react.default.createElement("span", null, "@")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('4', e);
        }
      }, _react.default.createElement("span", null, "4")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('5', e);
        }
      }, _react.default.createElement("span", null, "5")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('6', e);
        }
      }, _react.default.createElement("span", null, "6")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('.', e);
        }
      }, _react.default.createElement("span", {
        style: {
          fontSize: 20
        }
      }, ".")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('', e);
        }
      }, _react.default.createElement("span", null, "#")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('7', e);
        }
      }, _react.default.createElement("span", null, "7")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('8', e);
        }
      }, _react.default.createElement("span", null, "8")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('9', e);
        }
      }, _react.default.createElement("span", null, "9")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('enter', e);
        }
      }, _react.default.createElement("span", null, "\u8DF3\u8F6C")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('', e);
        }
      }, _react.default.createElement("span", null, "?!$")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp(type === 'card' ? 'x' : '', e);
        }
      }, _react.default.createElement("span", null, type === 'card' ? 'x' : '')), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('0', e);
        }
      }, _react.default.createElement("span", null, "0")), _react.default.createElement("p", {
        onClick: function onClick(e) {
          return _this.onKeyUp('blur', e);
        }
      }, _react.default.createElement(_icon.default, {
        type: "keyboard",
        fontSize: 20
      })));

      _reactDom.default.render(Com, modalContainer, function () {
        setTimeout(function () {
          return modalContainer.classList.add("action");
        }, 50);
      });
    };

    _this2.close = function () {
      var _assertThisInitialize2 = _assertThisInitialized(_this2),
          modalContainer = _assertThisInitialize2.modalContainer;

      modalContainer.classList.remove('action');
      setTimeout(function () {
        _reactDom.default.unmountComponentAtNode(modalContainer);

        modalContainer.parentNode && modalContainer.parentNode.removeChild(modalContainer);
      }, 250);
    };

    _this2.onChange = function (value) {
      var _this2$props = _this2.props,
          onChange = _this2$props.onChange,
          proxy = _this2$props.proxy;

      if (proxy) {
        value = proxy(value);
      }

      _this2.setState({
        value: value
      });

      if (onChange) onChange(value);
    };

    _this2.onEnter = function (e) {
      var onEnter = _this2.props.onEnter;

      _this2.onBlur(e);

      if (onEnter && typeof onEnter === 'function') onEnter(_this2.value);
    };

    _this2.onKeyUp = function (key, e) {
      e.nativeEvent.stopImmediatePropagation();
      e.preventDefault();
      e.stopPropagation();
      if (!key) return;
      var _this2$props2 = _this2.props,
          onKeyUp = _this2$props2.onKeyUp,
          _this2$props2$type = _this2$props2.type,
          type = _this2$props2$type === void 0 ? 'price' : _this2$props2$type;
      var value = _this2.state.value;

      switch (key) {
        case 'del':
          if (value.length > 0) value = value.substring(0, value.length - 1);
          break;

        case 'enter':
          _this2.onEnter(e);

          break;

        case 'blur':
          _this2.onBlur(e);

          break;

        case '.':
          if (type === 'price' && value.indexOf('.') === -1 && value.length > 0) value += key;
          break;

        case 'x':
          if (type === 'card' && value.indexOf('x') === -1 && value.length > 0) value += key;
          break;

        default:
          if (!length || length > value.length) value = "".concat(value).concat(key);
          value = value.replace(/^0+/, 0);
          value = value.replace(/^0(\d)/, '$1');
          break;
      }

      if (onKeyUp && typeof onKeyUp === 'function') onKeyUp(key);

      _this2.onChange(value);
    };

    _this2.onBlur = function (e) {
      var onBlur = _this2.props.onBlur;
      var target = e.target;

      _this2.setState({
        focused: false
      });

      document.removeEventListener('click', _this2.onBlur);
      document.removeEventListener('paste', _this2.onPaste);

      _this2.close();

      document.getElementsByTagName("html")[0].style.top = 0 + 'px';
      if (onBlur) onBlur(_this2.value, target);
    };

    _this2.onFocus = function (e) {
      var onFocus = _this2.props.onFocus;

      var _ref = e || {},
          _ref$target = _ref.target,
          target = _ref$target === void 0 ? _this2.input : _ref$target;

      var Rect = target.getBoundingClientRect();
      var y = Rect.y,
          height = Rect.height;
      var top = window.innerHeight - 200 - y - height;
      if (top < 0) document.getElementsByTagName("html")[0].style.top = "".concat(top - 15, "px");

      _this2.setState({
        focused: true
      });

      if (onFocus && typeof onFocus === 'function') onFocus(_this2.value, target);
    };

    _this2.onClear = function (e) {
      e.nativeEvent && e.nativeEvent.stopImmediatePropagation && e.nativeEvent.stopImmediatePropagation();
      e.preventDefault();
      e.stopPropagation();

      _this2.setState({
        value: ''
      });
    };

    var _value = props.defaultValue || props.value;

    if (props.type === 'card') {
      _value = _value.replace(/[^\d|x]/g, "");
    } else {
      if (props.type === 'number') _value = _value.replace(/\./g, "");
      _value = parseFloat(_value);
      if (isNaN(_value)) _value = '';
    }

    _value += '';
    _this2.state = {
      value: _value,
      focused: false
    };
    document.getElementsByTagName("html")[0].classList.add("".concat(prefixCls, "__common-input-document"));
    return _this2;
  }

  _createClass(Number, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.getElementsByTagName("html")[0].classList.remove("".concat(prefixCls, "__common-input-document"));
      this.close();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var props = this.props;
      var autoFocus = props.autoFocus;

      if (autoFocus) {
        this.onClick(null);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          className = _this$props.className,
          clearable = _this$props.clearable;
      var _this$state = this.state,
          focused = _this$state.focused,
          value = _this$state.value;
      var cls = (0, _classnames4.default)(prefixCls, className, _defineProperty({}, "".concat(prefixCls, "--clearable"), clearable));
      var IconClass = (0, _classnames4.default)("".concat(prefixCls, "__clear"), "".concat(prefixCls, "__clear--textarea"), _defineProperty({}, "".concat(prefixCls, "__clear--show"), focused && value && value.length > 0));
      var placeHolder = (0, _classnames4.default)("".concat(prefixCls, "__value"), _defineProperty({}, "".concat(prefixCls, "__value--hide"), !focused));
      return _react.default.createElement("div", {
        className: cls
      }, _react.default.createElement("p", {
        className: placeHolder,
        ref: function ref(input) {
          return _this3.input = input;
        },
        onClick: function onClick(e) {
          return _this3.onClick(e);
        }
      }, _react.default.createElement("span", null, value)), clearable && _react.default.createElement(_icon.default, {
        type: "wrongRoundFill",
        className: IconClass,
        onClick: function onClick(e) {
          _this3.onClear(e);
        }
      }));
    }
  }, {
    key: "value",
    get: function get() {
      var value = this.state.value;
      return value;
    }
  }]);

  return Number;
}(_react.PureComponent);

exports.default = Number;
Number.defaultProps = {
  type: 'Number',
  clearable: false
};