# Input 文本框

## 基本用法

```jsx
import { Input, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell icon="基本文本 ">
          <Input type={'text'} value={'test'} />
        </Cell>
        <Cell icon="基本文本 ">
          <Input type={'text'} placeholder="输入基本信息" value={''} />
        </Cell>
        <Cell icon="基本文本 ">
          <Input type={'text'} disabled value={'test'} />
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 多行文本

```jsx
import { Input, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell autoHeight icon="固定高度 ">
          <Input type={'textarea'} defaultValue={'test'} />
        </Cell>
        <Cell autoHeight icon="自动高度 ">
          <Input autoHeight type={'textarea'} placeholder="输入基本信息" />
        </Cell>
        <Cell autoHeight icon="清除按钮 ">
          <Input clearable type={'textarea'} defaultValue={'test'} />
        </Cell>
        <Cell autoHeight icon="固定行数 ">
          <Input clearable rows={2} type={'textarea'} defaultValue={'test'} />
        </Cell>
        <Cell autoHeight icon="显示长度 ">
          <Input maxLength={60} type={'textarea'} defaultValue={'test'} />
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 数字/金额/卡号

```jsx
import { Input, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell icon="数字 ">
          <Input type={'number'} value={'125'} />
        </Cell>
        <Cell icon="金额 ">
          <Input type={'price'} placeholder="输入基本信息" />
        </Cell>
        <Cell icon="卡号 ">
          <Input type={'card'} value={''} />
        </Cell>
      </aside>
    );
  }
}
ReactDOM.render(<Demo />, mountNode);
```

## 输入内容

```jsx
import { Input, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell>
          <Input type={'text'} placeholder="输入基本信息" value={''} />
        </Cell>
        <Cell autoHeight>
          <Input clearable maxLength={120} clearable rows={5} type={'textarea'} placeholder="输入基本信息" />
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 输入内容/自动高度

```jsx
import { Input, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    return (
      <aside>
        <Cell>
          <Input type={'text'} placeholder="输入基本信息" value={''} />
        </Cell>
        <Cell autoHeight>
          <Input autoHeight clearable maxLength={120} clearable rows={5} type={'textarea'} placeholder="输入基本信息" />
        </Cell>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性         | 类型                               | 默认值 | 说明                                                                                                                    |
| :----------- | :--------------------------------- | :----- | :---------------------------------------------------------------------------------------------------------------------- |
| type         | string                             | 'text' | 类型，可选值 `text`、`search`、`password`、`textarea`、`number`、`price`、`card`                                        |
| value        | number \| string                   | -      | 值                                                                                                                      |
| defaultValue | number \| string                   | -      | 初始值                                                                                                                  |
| disabled     | boolean                            | false  | 是否禁用                                                                                                                |  |
| rows         | number                             | 2      | 多行文本时的显示行数。type 为 text 类型时有效。                                                                         |
| autoHeight   | boolean                            | false  | 是否高度自适应                                                                                                          |
| maxLength    | number                             | -      | 输入字数上限                                                                                                            |  |
| clearable    | boolean                            | true   | 是否显示清除按钮。多行文本（type="text"且包含 rows 属性）时无效。必须为受控组件（属性包含 value、onChange）时方可生效。 |
| onChange     | (value?: number \| string) => void | -      | 值变化时触发的回调函数                                                                                                  |
