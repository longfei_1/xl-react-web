"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _classnames3 = _interopRequireDefault(require("classnames"));

var _util = _interopRequireDefault(require("../util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-popup");

var Popup =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Popup, _PureComponent);

  function Popup(_props) {
    var _this2;

    _classCallCheck(this, Popup);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Popup).call(this, _props));
    _this2.container = void 0;
    _this2.main = void 0;
    _this2.box = void 0;
    _this2.setProps = void 0;
    _this2.disableIosBounce = void 0;

    _this2.show = function (type) {
      var _this = _assertThisInitialized(_this2);

      var _assertThisInitialize = _assertThisInitialized(_this2),
          container = _assertThisInitialize.container;

      var _this2$props = _this2.props,
          children = _this2$props.children,
          direction = _this2$props.direction,
          width = _this2$props.width,
          height = _this2$props.height,
          mask = _this2$props.mask,
          open = _this2$props.open,
          style = _this2$props.style,
          animationType = _this2$props.animationType;
      var animationDuration = _this2.props.animationDuration;
      if (animationType === 'none') animationDuration = 1;
      var mainCls = (0, _classnames3.default)("".concat(prefixCls, "__main"), "".concat(prefixCls, "__").concat(direction));
      document.body.appendChild(container);
      _this2.disableIosBounce = _util.default.disableIosBounce(container);

      var Main =
      /*#__PURE__*/
      function (_PureComponent2) {
        _inherits(Main, _PureComponent2);

        function Main(props) {
          var _this3;

          _classCallCheck(this, Main);

          _this3 = _possibleConstructorReturn(this, _getPrototypeOf(Main).call(this, props));
          _this3.mainBox = void 0;
          var animationDuration = props.animationDuration,
              children = props.children,
              _props$mainClass = props.mainClass,
              mainClass = _props$mainClass === void 0 ? '' : _props$mainClass;

          _this.setProps = function (obj) {
            _this3.setState(obj);
          };

          _this3.state = {
            animationDuration: animationDuration,
            mainClass: mainClass,
            children: children
          };
          return _this3;
        }

        _createClass(Main, [{
          key: "componentDidMount",
          value: function componentDidMount() {
            var _this4 = this;

            this.setState({
              animationDuration: type ? 0 : animationDuration
            });
            setTimeout(function () {
              _this4.setState({
                mainClass: "".concat(prefixCls, "__").concat(direction, "--show")
              });
            });
            open && open();
            document.body.classList.add("".concat(prefixCls, "__ovhide"));
          }
        }, {
          key: "componentWillUnmount",
          value: function componentWillUnmount() {
            if (_this.disableIosBounce) _this.disableIosBounce();
          }
        }, {
          key: "render",
          value: function render() {
            var _this5 = this,
                _objectSpread2;

            var _this$state = this.state,
                mainClass = _this$state.mainClass,
                children = _this$state.children;
            var animationDuration = this.state.animationDuration;
            if (animationType === 'none') animationDuration = 1;
            var onMaskClick = this.props.onMaskClick;
            return _react.default.createElement(_react.Fragment, null, _react.default.createElement("div", {
              onClick: function onClick() {
                onMaskClick && onMaskClick();
              },
              style: _defineProperty({}, _util.default.transition, animationType === 'none' ? '' : "opacity ".concat(animationDuration, "ms,opacity ").concat(animationDuration, "ms")),
              className: (0, _classnames3.default)("".concat(prefixCls, "__mask"), mainClass ? "".concat(prefixCls, "__mask--show") : '', _defineProperty({}, "".concat(prefixCls, "__mask--opacity"), !mask))
            }), _react.default.createElement("div", {
              ref: function ref(_ref2) {
                return _this5.mainBox = _ref2;
              },
              style: _objectSpread({}, (_objectSpread2 = {}, _defineProperty(_objectSpread2, _util.default.transition, animationType === 'none' ? '' : "transform ".concat(animationDuration, "ms,opacity ").concat(animationDuration, "ms")), _defineProperty(_objectSpread2, "width", width), _defineProperty(_objectSpread2, "height", height), _objectSpread2), {}, style),
              className: (0, _classnames3.default)(mainCls, mainClass, _defineProperty({}, "".concat(prefixCls, "__center--").concat(animationType), direction === 'center'))
            }, children));
          }
        }]);

        return Main;
      }(_react.PureComponent);

      _reactDom.default.render(_react.default.createElement(Main, _this2.props, children), container);
    };

    _this2.close = function () {
      var _assertThisInitialize2 = _assertThisInitialized(_this2),
          container = _assertThisInitialize2.container;

      var _this2$props2 = _this2.props,
          animationType = _this2$props2.animationType,
          close = _this2$props2.close;
      var animationDuration = _this2.props.animationDuration;
      if (animationType === 'none') animationDuration = 1;

      _this2.setProps({
        mainClass: ''
      });

      if (container) {
        setTimeout(function () {
          container.parentNode.removeChild(container);
          _this2.container = null;
          close && close();
          if (_this2.disableIosBounce) _this2.disableIosBounce();
          document.body.classList.remove("".concat(prefixCls, "__ovhide"));
        }, animationDuration);
      }
    };

    return _this2;
  }

  _createClass(Popup, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.initEvent(true);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps.visible !== this.props.visible) this.initEvent(false);else if (this.props.visible && this.props.children !== prevProps.children) {
        this.setProps({
          children: this.props.children
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.disableIosBounce) this.disableIosBounce();
      this.close();
    }
  }, {
    key: "initEvent",
    value: function initEvent(type) {
      var _this$props = this.props,
          className = _this$props.className,
          visible = _this$props.visible;

      if (!this.container) {
        var cls = (0, _classnames3.default)(prefixCls, className);
        var container = this.container = document.createElement('div');
        container.setAttribute('class', cls);
        visible && this.show(type);
      } else {
        if (visible) this.show(false);else this.close();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement("div", null);
    }
  }]);

  return Popup;
}(_react.PureComponent);

exports.default = Popup;
Popup.defaultProps = {
  direction: 'bottom',
  animationType: 'fade-scale',
  mask: true,
  animationDuration: 250
};