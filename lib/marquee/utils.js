"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setAnimation = exports.getVertical = exports.getHorizontal = void 0;

var getHorizontal = function getHorizontal(distance, key) {
  return "\n  @-webkit-keyframes ".concat(key, " {\n    100% {\n      -webkit-transform: translate3d(").concat(distance, "px, 0, 0);\n      transform: translate3d(").concat(distance, "px, 0, 0);\n    }\n  }\n  @keyframes ").concat(key, " {\n    100% {\n      -webkit-transform: translate3d(").concat(distance, "px, 0, 0);\n      transform: translate3d(").concat(distance, "px, 0, 0);\n    }\n  }");
};

exports.getHorizontal = getHorizontal;

var getVertical = function getVertical(distance, key) {
  return "\n  @-webkit-keyframes ".concat(key, " {\n    100% {\n      -webkit-transform: translate3d(0, ").concat(distance, "px, 0);\n      transform: translate3d(0, ").concat(distance, "px, 0);\n    }\n  }\n  @keyframes ").concat(key, " {\n    100% {\n      -webkit-transform: translate3d(0, ").concat(distance, "px, 0);\n      transform: translate3d(0, ").concat(distance, "px, 0);\n    }\n  }");
};

exports.getVertical = getVertical;

var setAnimation = function setAnimation(duration, loop, delay, key) {
  var infinite = loop ? 'infinite' : 1;
  return "".concat(duration, "ms ").concat(key, " ").concat(delay, "ms linear ").concat(infinite);
};

exports.setAnimation = setAnimation;