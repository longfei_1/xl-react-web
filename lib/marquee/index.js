"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _utils = require("./utils");

var _util = _interopRequireDefault(require("../util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var prefixCls = "".concat("xl", "-marquee");
var count = 0;

var getCount = function getCount() {
  var time = new Date().getTime();
  count += 1;
  if (count > 50) count = 0;
  return "".concat(time).concat(count);
};

var Marquee =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Marquee, _PureComponent);

  function Marquee(props) {
    var _this;

    _classCallCheck(this, Marquee);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Marquee).call(this, props));
    _this.marqueeBox = void 0;
    _this.uuid = void 0;
    _this.loops = void 0;
    _this.loops2 = void 0;
    _this.inter = void 0;
    _this.uuid = "".concat(prefixCls, "--key-").concat(getCount());
    return _this;
  }

  _createClass(Marquee, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          delay = _this$props.delay,
          direction = _this$props.direction;
      if (direction === 'left' || direction === 'right') this.run(delay);else this.runVertical(delay);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var direction = this.props.direction;
      if (direction === 'left' || direction === 'right') this.run(50);else this.runVertical(50);
    }
  }, {
    key: "run",
    value: function run(delay) {
      var _this2 = this;

      var marqueeBox = this.marqueeBox,
          uuid = this.uuid;
      var _this$props2 = this.props,
          speed = _this$props2.speed,
          loop = _this$props2.loop,
          direction = _this$props2.direction;
      var headDom = document.querySelector('head');
      var styleDom = document.querySelector("#".concat(uuid));
      var boxWidth = marqueeBox.offsetWidth;
      var pWidth = marqueeBox.parentNode.offsetWidth;
      if (styleDom) styleDom.parentNode.removeChild(styleDom);
      this.uuid = "".concat(prefixCls, "--key-").concat(getCount());
      var styleLine = document.createElement('style');
      styleLine.setAttribute('id', this.uuid);
      styleLine.innerHTML = (0, _utils.getHorizontal)(-boxWidth, this.uuid);
      var start = 0;
      var end = pWidth;
      var duration = boxWidth * 1000 / speed;
      var animation = (0, _utils.setAnimation)(duration, loop, 0, this.uuid);

      if (direction === 'right') {
        start = pWidth - boxWidth;
        animation = (0, _utils.setAnimation)(duration, loop, 0, this.uuid);
        styleLine.innerHTML = (0, _utils.getHorizontal)(pWidth, this.uuid);
        end = -boxWidth;
      }

      _util.default.setTranslateInfo(marqueeBox, {
        x: start
      });

      marqueeBox.style.opacity = 1;
      if (this.loops) clearTimeout(this.loops);
      if (this.loops2) clearTimeout(this.loops2);
      if (pWidth > boxWidth + 1) return;
      headDom.appendChild(styleLine);
      this.loops = setTimeout(function () {
        marqueeBox.style.animation = animation;
        marqueeBox.style.webkitTransform = animation;

        if (loop) {
          _this2.loops2 = setTimeout(function () {
            _util.default.setTranslateInfo(marqueeBox, {
              x: end
            });
          }, duration - 50);
        }
      }, delay);
    }
  }, {
    key: "slide",
    value: function slide(boxWidth, pWidth, marqueeBox) {
      var _this3 = this;

      var _this$props3 = this.props,
          speed = _this$props3.speed,
          loop = _this$props3.loop,
          direction = _this$props3.direction;
      if (typeof loop === 'boolean') loop = 3000;
      if (speed < 300) speed = 300;
      var start = 0;

      if (direction === 'down') {
        start = -boxWidth;
      }

      _util.default.setTranslateInfo(marqueeBox, {
        y: start
      });

      if (this.inter) clearInterval(this.inter);
      if (this.loops2) clearTimeout(this.loops2);
      this.inter = setInterval(function () {
        var y = _util.default.getTranslateInfo(marqueeBox, 2);

        if (direction === 'up') y = y - pWidth;else y = y + pWidth;

        _util.default.setTranslateInfo(marqueeBox, {
          y: y,
          time: speed
        });

        if ((y <= -boxWidth && direction === 'up' || y >= 0 && direction === 'down') && loop) {
          _this3.loops2 = setTimeout(function () {
            _util.default.setTranslateInfo(marqueeBox, {
              y: start,
              time: 0
            });
          }, speed + 50);
        }
      }, loop);
    }
  }, {
    key: "runVertical",
    value: function runVertical(delay) {
      var marqueeBox = this.marqueeBox,
          uuid = this.uuid;
      var _this$props4 = this.props,
          speed = _this$props4.speed,
          loop = _this$props4.loop,
          direction = _this$props4.direction,
          type = _this$props4.type;
      var headDom = document.querySelector('head');
      var styleDom = document.querySelector("#".concat(uuid));
      var boxWidth = marqueeBox.offsetHeight / 2;
      var pWidth = marqueeBox.parentNode.offsetHeight;

      if (type === 'slide') {
        marqueeBox.style.opacity = 1;
        this.slide(boxWidth, pWidth, marqueeBox);
        return;
      }

      if (styleDom) styleDom.parentNode.removeChild(styleDom);
      this.uuid = "".concat(prefixCls, "--key-").concat(getCount());
      var styleLine = document.createElement('style');
      styleLine.setAttribute('id', this.uuid);
      styleLine.innerHTML = (0, _utils.getVertical)(-boxWidth, this.uuid);
      var start = 0;
      var duration = boxWidth * 1000 / speed;
      var animation = (0, _utils.setAnimation)(duration, loop, 0, this.uuid);

      if (direction === 'down') {
        start = -boxWidth - pWidth;
        animation = (0, _utils.setAnimation)(duration, loop, 0, this.uuid);
        styleLine.innerHTML = (0, _utils.getVertical)(-pWidth, this.uuid);
      }

      _util.default.setTranslateInfo(marqueeBox, {
        y: start
      });

      marqueeBox.style.opacity = 1;
      if (this.loops) clearTimeout(this.loops);
      if (this.loops2) clearTimeout(this.loops2);
      if (pWidth > boxWidth + 1) return;
      headDom.appendChild(styleLine);
      this.loops = setTimeout(function () {
        marqueeBox.style.animation = animation;
        marqueeBox.style.webkitTransform = animation;
      }, delay);
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var _this$props5 = this.props,
          children = _this$props5.children,
          className = _this$props5.className,
          style = _this$props5.style,
          direction = _this$props5.direction,
          onClick = _this$props5.onClick,
          hover = _this$props5.hover;
      var cls = (0, _classnames2.default)(prefixCls, className, {});
      var clsBox = (0, _classnames2.default)("".concat(prefixCls, "__box"), "".concat(prefixCls, "__box--").concat(direction), _defineProperty({}, "".concat(prefixCls, "__box--hover"), hover));
      return _react.default.createElement("div", {
        className: cls,
        style: style,
        onClick: onClick
      }, _react.default.createElement("div", {
        className: clsBox,
        ref: function ref(_ref) {
          return _this4.marqueeBox = _ref;
        }
      }, children, (direction === 'up' || direction === 'down') && children));
    }
  }]);

  return Marquee;
}(_react.PureComponent);

exports.default = Marquee;
Marquee.defaultProps = {
  direction: 'left',
  type: 'scroll',
  speed: 30,
  delay: 100,
  loop: true,
  hover: false
};