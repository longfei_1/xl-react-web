import { ReactNode } from 'react';
export default interface PropsType {
    disabled?: boolean;
    hasArrow?: boolean;
    headerNoBorder?: boolean;
    footerNoBorder?: boolean;
    autoHeight?: boolean;
    icon?: ReactNode;
    title?: ReactNode;
    option?: ReactNode;
}
