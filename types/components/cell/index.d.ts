import React, { PureComponent, HTMLAttributes } from 'react';
import PropsType from './PropsType';
export declare type HTMLDivProps = Omit<HTMLAttributes<HTMLDivElement>, 'title'>;
export interface CellProps extends HTMLDivProps, PropsType {
    className?: string;
}
export default class Cell extends PureComponent<CellProps, {}> {
    inner: any;
    extra: any;
    MOVE_ING: any;
    TouchInfo: any;
    static defaultProps: {
        hasArrow: boolean;
        headerNoBorder: boolean;
        footerNoBorder: boolean;
        autoHeight: boolean;
        option: null;
        disabled: boolean;
    };
    start: (e: any) => void;
    move: (e: any) => void;
    end: () => void;
    render(): React.ReactNode;
}
