import React, { PureComponent } from 'react';
import { TextBase } from './PropsType';
export default class Text extends PureComponent<TextBase, any> {
    static defaultProps: {
        type: string;
        rows: number;
        clearable: boolean;
        readOnly: boolean;
        disabled: boolean;
    };
    private input;
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(): void;
    handleComposition: (e: any) => void;
    onFocus: (e: any) => void;
    onBlur: (e: any) => void;
    onClear: (e: any) => void;
    onChange: (e: any) => void;
    focus(): void;
    blur(): void;
    render(): React.ReactNode;
}
