import React from 'react';
export interface BaseProps {
    type?: string | undefined;
    placeholder?: string;
    disabled?: boolean;
    autoFocus?: boolean;
    className?: string;
    readOnly?: boolean;
    style?: React.CSSProperties;
}
export interface InputBase extends BaseProps {
    type?: 'text' | 'search' | 'password';
    value?: string;
    clearable?: boolean;
    onFocus?: (value?: string, e?: any) => void;
    onClear?: (value?: string, e?: any) => void;
    onBlur?: (value?: string, e?: any) => void;
    onChange?: (value?: string, e?: any) => void;
    onCompositionStart?: (value?: any, e?: any) => void;
    onCompositionUpdate?: (value?: any, e?: any) => void;
    onCompositionEnd?: (value?: any, e?: any) => void;
}
export interface TextBase extends BaseProps {
    type?: 'textarea';
    value?: string;
    defaultValue?: string;
    rows?: number;
    autoHeight?: boolean;
    clearable?: boolean;
    maxLength?: number;
    onFocus?: (value?: string, e?: any) => void;
    onBlur?: (value?: string, e?: any) => void;
    onChange?: (value?: string, e?: any) => void;
    onClear?: (value?: string, e?: any) => void;
    onCompositionStart?: (value?: any, e?: any) => void;
    onCompositionUpdate?: (value?: any, e?: any) => void;
    onCompositionEnd?: (value?: any, e?: any) => void;
}
export interface NumberBase extends BaseProps {
    type?: 'number' | 'price' | 'card';
    value?: string;
    clearable?: boolean;
    onFocus?: (value?: string, e?: any) => void;
    onClear?: (value?: string, e?: any) => void;
    onBlur?: (value?: string, e?: any) => void;
    onChange?: (value?: string, e?: any) => void;
    onKeyUp?: (value?: any, e?: any) => void;
    onEnter?: (value?: any, e?: any) => void;
    onPaste?: (value?: any, e?: any) => void;
    proxy?: (value?: any) => any;
}
