import { PureComponent } from 'react';
import { InputBase } from './PropsType';
export default class Base extends PureComponent<InputBase, any> {
    static defaultProps: {
        type: string;
        clearable: boolean;
        readOnly: boolean;
        disabled: boolean;
    };
    private input;
    private onBlurTimeout;
    private blurFromClear;
    constructor(props: any);
    componentDidMount(): void;
    UNSAFE_componentWillReceiveProps(nextProps: Readonly<InputBase>): void;
    componentWillUnmount(): void;
    onFocus: (e: any) => void;
    focus: () => void;
    blur: () => void;
    onBlur: (e: any) => void;
    onChange: (e: any) => void;
    onClear: () => void;
    handleComposition: (e: any) => void;
    render(): JSX.Element;
}
