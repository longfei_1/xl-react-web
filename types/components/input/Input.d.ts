import { PureComponent } from 'react';
import { InputBase, TextBase, NumberBase } from './PropsType';
export declare type InputProps = InputBase | NumberBase | TextBase;
export default class Input extends PureComponent<InputProps, {}> {
    static defaultProps: {
        type: string;
    };
    private input;
    focus(): void;
    blur(): void;
    render(): JSX.Element;
}
