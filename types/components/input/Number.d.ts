import React, { PureComponent } from 'react';
import { NumberBase } from './PropsType';
export default class Number extends PureComponent<NumberBase, any> {
    static defaultProps: {
        type: string;
        clearable: boolean;
    };
    private input;
    private timeOut;
    private modalContainer;
    constructor(props: any);
    componentWillUnmount(): void;
    componentDidMount(): void;
    get value(): any;
    onClick: (e: any) => void;
    onPaste: (e: any) => void;
    showKeyBox: () => void;
    close: () => void;
    onChange: (value: any) => void;
    onEnter: (e: any) => void;
    onKeyUp: (key: any, e: any) => void;
    onBlur: (e: any) => void;
    onFocus: (e: any) => void;
    onClear: (e: any) => void;
    render(): React.ReactNode;
}
