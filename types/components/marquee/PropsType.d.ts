import React from 'react';
export default interface PropsType {
    direction: 'left' | 'right' | 'down' | 'up';
    type?: 'scroll' | 'slide';
    height?: number | string;
    width?: number | string;
    delay: number;
    loop: any;
    hover?: boolean;
    speed: number;
    onClick?: (e?: any) => void;
    className?: string;
    style?: React.CSSProperties;
}
