import React, { PureComponent } from 'react';
import PropsType from './PropsType';
export default class Marquee extends PureComponent<PropsType, {}> {
    marqueeBox: any;
    uuid: any;
    loops: any;
    loops2: any;
    inter: any;
    static defaultProps: {
        direction: string;
        type: string;
        speed: number;
        delay: number;
        loop: boolean;
        hover: boolean;
    };
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(): void;
    run(delay: any): void;
    slide(boxWidth: any, pWidth: any, marqueeBox: any): void;
    runVertical(delay: any): void;
    render(): React.ReactNode;
}
