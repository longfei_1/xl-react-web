export declare const getHorizontal: (distance: number, key: string) => string;
export declare const getVertical: (distance: number, key: string) => string;
export declare const setAnimation: (duration: number, loop: boolean, delay: number, key: string) => string;
