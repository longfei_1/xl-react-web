/// <reference types="node" />
import * as scroll from './scroll';
import * as device from './device';
declare const _default: {
    TOUCH_EVENT_NAME: {
        start: string;
        move: string;
        end: string;
        option: {
            passive: boolean;
        };
    } | {
        start: string;
        move: string;
        end: string;
        option: {
            passive?: undefined;
        };
    };
    IS_IN_JEST: NodeJS.Process;
    disableIosBounce: (element: any) => () => void;
    getVendorPrefix: () => string | undefined;
    transform: string;
    transition: string;
    setTranslateInfo: (dom: any, option: any) => void;
    checkInParent: (son: any, parent: any) => any;
    isArray: (input: any) => boolean;
    getBodyWidth: (type?: boolean) => any;
    getBodyHeight: (type?: boolean) => any;
    isInClient: (pageX: any, pageY: any) => boolean;
    getTranslateInfo: (dom: any, type: any) => number | {
        x: number;
        y: number;
    };
    device: typeof device;
    scroll: typeof scroll;
};
export default _default;
