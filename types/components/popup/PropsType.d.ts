export default interface PropsType {
    className?: string;
    visible?: boolean;
    mask?: boolean;
    style?: object;
    direction: 'top' | 'left' | 'bottom' | 'right' | 'center';
    animationType: 'slide' | 'fade' | 'fade-scale' | 'fade-slide' | 'none';
    animationDuration: number;
    width: string | number;
    height: string | number;
    close: () => {};
    open: () => {};
    onMaskClick: (item: any) => {};
}
