import { PureComponent } from 'react';
import PropsType from './PropsType';
export default class Popup extends PureComponent<PropsType, any> {
    container: any;
    main: any;
    box: any;
    setProps: any;
    disableIosBounce: any;
    static defaultProps: {
        direction: string;
        animationType: string;
        mask: boolean;
        animationDuration: number;
    };
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: any): void;
    componentWillUnmount(): void;
    initEvent(type: any): void;
    show: (type: any) => void;
    close: () => void;
    render(): JSX.Element;
}
