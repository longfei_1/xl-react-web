import { PureComponent } from 'react';
import PropsType from './PropsType';
export default class Picker extends PureComponent<PropsType, any> {
    contain: any;
    list: any;
    target: any;
    TouchInfo: any;
    height: any;
    hasBind: boolean;
    static defaultProps: {
        renderKey: string;
        preventDefault: boolean;
        disable: boolean;
        keyName: string;
    };
    constructor(props: any);
    componentDidMount(): void;
    componentDidUpdate(prevProps: any): void;
    initEvent(): void;
    moveTo(index: any, time?: number): void;
    getIndex(): number;
    render(): JSX.Element;
}
