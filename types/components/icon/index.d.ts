import React, { PureComponent, HTMLAttributes } from 'react';
import BasePropsType from './PropsType';
export declare const addIcon: (Icon: React.ReactNode) => void;
export declare type IconPropsType = BasePropsType & HTMLAttributes<HTMLElement>;
export default class Icon extends PureComponent<IconPropsType, {}> {
    static defaultProps: {
        theme: string;
        size: string;
    };
    constructor(props: any);
    render(): React.ReactNode;
}
