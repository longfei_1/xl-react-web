# Icon 图标

## 基本用法

```jsx
import { Icon } from 'wxl';

const ICONS = [
  'add',
  'wrongRoundFill',
  'wrong',
  'deletekey',
  'keyboard',
  'arrowDownOne',
  'arrowDownOneLeft',
  'userDefault'
];

class Demo extends React.Component {
  render() {
    return (
      <div className="list-grid">
        {ICONS.map(item => {
          return (
            <div className="column">
              <Icon type={item} theme="primary" size="lg" />
            </div>
          );
        })}
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 样式主题

```jsx
import { Icon } from 'wxl';

const ICONS = [
  'add',
  'wrongRoundFill',
  'wrong',
  'deletekey',
  'keyboard',
  'arrowDownOne',
  'arrowDownOneLeft',
  'userDefault'
];

class Demo extends React.Component {
  render() {
    return (
      <div className="list-grid">
        <div className="column">
          <Icon type="wrongRoundFill" theme="default" size="lg" />
        </div>
        <div className="column">
          <Icon type="wrongRoundFill" theme="success" size="lg" />
        </div>
        <div className="column">
          <Icon type="wrongRoundFill" theme="warning" size="lg" />
        </div>
        <div className="column">
          <Icon type="wrongRoundFill" theme="danger" size="lg" />
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 样式大小

```jsx
import { Icon } from 'wxl';

const ICONS = [
  'add',
  'wrongRoundFill',
  'wrong',
  'deletekey',
  'keyboard',
  'arrowDownOne',
  'arrowDownOneLeft',
  'userDefault'
];

class Demo extends React.Component {
  render() {
    return (
      <div className="list-grid">
        <div className="column">
          <Icon type="wrongRoundFill" theme="success" size="sm" />
        </div>
        <div className="column">
          <Icon type="wrongRoundFill" theme="success" size="md" />
        </div>
        <div className="column">
          <Icon type="wrongRoundFill" theme="success" size="lg" />
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性     | 类型   | 默认值  | 说明                 |
| :------- | :----- | :------ | :------------------- |
| type     | string | -       | icon 名称            |
| theme    | string | default | 主题                 |
| size     | string | md      | 大小                 |
| fontSize | number | -       | 大小                 |
| viewBox  | string | -       | svg viewBox 大小设置 |
