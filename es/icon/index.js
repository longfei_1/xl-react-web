function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { PureComponent } from 'react';
import classnames from 'classnames';
import ReactDOM from 'react-dom';
import * as ICONS from './icons'; // import './style'

const ICON_CACHE = new Set();
let iconContainer;
export const addIcon = function (Icon) {
  if (ICON_CACHE.has(Icon)) return;
  ICON_CACHE.add(Icon);

  if (!iconContainer) {
    iconContainer = document.createElement("div");
    document.body.insertBefore(iconContainer, document.body.firstElementChild);
  }

  const container = document.createElement("div");
  container.style.height = '0px';
  container.style.overflow = "hidden";
  document.body.appendChild(container);
  const {
    render
  } = ReactDOM; // @ts-ignore

  render(React.createElement("svg", null, React.createElement(Icon, null)), container);
  iconContainer.appendChild(container);
};
const prefixCls = `${__PREFIX__}-icon`;
export default class Icon extends PureComponent {
  constructor(props) {
    super(props);
    const {
      type
    } = props;
    const Icon = ICONS[type];
    if (!ICON_CACHE.has(Icon) && Icon) addIcon(Icon);
  }

  render() {
    const _this$props = this.props,
          {
      children,
      type,
      theme,
      size,
      className,
      style = {},
      fontSize
    } = _this$props,
          rest = _objectWithoutProperties(_this$props, ["children", "type", "theme", "size", "className", "style", "fontSize"]);

    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--${type}`]: !!type,
      [`${prefixCls}--${theme}`]: !!theme,
      [`${prefixCls}--${size}`]: !!size
    });
    if (fontSize) style.fontSize = fontSize + 'px';

    const props = _objectSpread({
      className: cls,
      type,
      style: _objectSpread({}, style)
    }, rest);

    if (children) {
      return React.createElement("i", props, children);
    } else {
      return React.createElement("i", props, React.createElement("svg", {
        width: "1em",
        height: "1em",
        fill: "currentColor",
        viewBox: "0 0 32 32"
      }, React.createElement("use", {
        xlinkHref: `#icon-${type}`
      })));
    }
  }

}
Icon.defaultProps = {
  theme: 'default',
  size: 'md'
};