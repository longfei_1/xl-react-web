const u = navigator.userAgent; // app = navigator.appVersion;

export const IS_IOS = (() => !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/))();
export const IS_ANDROID = (() => u.indexOf('Android') > -1 || u.indexOf('Linux') > -1)();