# Swiper 列表项

## 基本用法 (移动端查看，效果会好些)

```jsx
import { Picker, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    const dataOne = ['北京', '上海', '广东', '湖南', '湖北', '浙江', '河南', '河北', '江西'];
    return (
      <div style={{ padding: 10 }}>
        <Picker className={'picker-test'} data={dataOne} />
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 禁用滚动 (移动端查看，效果会好些)

```jsx
import { Picker, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    const dataTwo = [
      { name: '第一项', id: 0 },
      { name: '第二项', id: 1 },
      { name: '第三项', id: 2 },
      { name: '第四项', id: 3 },
      { name: '第五项', id: 4 }
    ];
    return (
      <div style={{ padding: 10 }}>
        <Picker disable data={dataTwo} />
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 指定渲染的 key/index

```jsx
import { Picker, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    const dataThree = [
      { title: '第一项', key: 0 },
      { title: '第二项', key: 1 },
      { title: '第三项', key: 2 },
      { title: '第四项', key: 3 },
      { title: '第五项', key: 4 }
    ];
    return (
      <div style={{ padding: 10 }}>
        <Picker
          keyValue={3}
          onChange={data => {
            console.log(data);
          }}
          className={'test_123'}
          renderKey="title"
          keyName="key"
          data={dataThree}
        />
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 指定渲染的 value/index

```jsx
import { Picker, Cell } from 'wxl';

class Demo extends React.Component {
  render() {
    const dataThree = [
      { title: '第一项', key: 0 },
      { title: '第二项', key: 1 },
      { title: '第三项', key: 2 },
      { title: '第四项', key: 3 },
      { title: '第五项', key: 4 }
    ];
    return (
      <div style={{ padding: 10 }}>
        <Picker
          value="第三项"
          onChange={data => {
            console.log(data);
          }}
          className={'test_123'}
          renderKey="title"
          data={dataThree}
        />
      </div>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性                 | 类型    | 默认值 | 说明                           |
| :------------------- | :------ | :----- | :----------------------------- |
| direction            | string  | left   | 方向 left/right                |
| className            | string  | -      | 类名称                         |
| activeIndex          | number  | 1      | 初始位置                       |
| loop                 | boolean | true   | 是否循环                       |
| bounce               | boolean | false  | 是否有反弹效果(loop 为 false)  |
| touchSlide           | boolean | false  | 不能拖拽(直接切换)             |
| overflow             | boolean | false  | 超出是否展示                   |
| preventDefault       | boolean | false  | 横向拖拽禁止竖直拖拽(反之亦然) |
| able                 | boolean | false  | 禁止拖拽                       |
| autoPlay             | boolean | false  | 自动播放                       |
| showPagination       | boolean | false  | 显示 pagination                |
| gap                  | number  | 0      | 缝隙的值                       |
| gapPadding           | number  | 0      | 两边的距离(loop 为 false)      |
| autoPlayIntervalTime | number  | 4000   | 自动轮播间隔时间               |
| animationDuration    | number  | 300    | 自动轮播滑动时间               |
| onChange             | -       | -      | 切换回调函数                   |
