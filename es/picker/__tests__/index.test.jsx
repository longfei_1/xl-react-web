import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Picker from '../index';

const prefixCls = `.${__PREFIX__}-picker`;

describe('Swiper', () => {
  // Picker 常规展示
  it('renders  Picker', () => {
    jest.useFakeTimers();
    const dataOne = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const dataTwo = [
      { name: '1', id: 0 },
      { name: '2', id: 1 },
      { name: '3', id: 2 },
      { name: '4', id: 3 },
      { name: '5', id: 4 }
    ];
    const dataThree = [
      { title: '1', key: 0 },
      { title: '2', key: 1 },
      { title: '3', key: 2 },
      { title: '4', key: 3 },
      { title: '5', key: 4 }
    ];
    const wrapper = render(
      <div>
        <Picker data={dataOne} />
        <Picker className={'test_123'} data={dataOne} />
        <Picker className={'test_123'} data={dataTwo} />
        <Picker className={'test_123'} renderKey="title" keyName="key" data={dataThree} />
      </div>
    );
    jest.advanceTimersByTime(300);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  // touch move
  it('touch  Picker', () => {
    jest.useFakeTimers();
    const dataThree = [
      { title: '1', key: 0 },
      { title: '2', key: 1 },
      { title: '3', key: 2 },
      { title: '4', key: 3 },
      { title: '5', key: 4 }
    ];
    const onChange = jest.fn();
    const wrapper = mount(
      <Picker onChange={onChange} className={'test_123'} renderKey="title" value="2" keyName="key" data={dataThree} />
    );
    const map = {};
    const target = wrapper.getDOMNode();
    target.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });
    jest.advanceTimersByTime(1000);
    expect(wrapper.state('index')).toEqual(1);
    const instance = wrapper.find(prefixCls);
    map.touchstart({
      touches: [
        {
          pageY: 10
        }
      ]
    });
    map.touchmove({
      touches: [
        {
          pageY: 10 + 2 * 24
        }
      ]
    });
    map.touchend({});
    jest.advanceTimersByTime(300);
    expect(wrapper.state('index')).toEqual(0);
    expect(onChange).toBeCalled();
    map.touchstart({
      touches: [
        {
          pageY: 300
        }
      ]
    });
    map.touchmove({
      touches: [
        {
          pageY: 300 - 3 * 24
        }
      ]
    });
    map.touchend({});
    jest.advanceTimersByTime(300);
    expect(wrapper.state('index')).toEqual(3);
    expect(onChange).toBeCalledWith({ title: '4', key: 3 });
  });

  it('change props  Picker', () => {
    jest.useFakeTimers();
    const dataThree = [
      { title: '1', key: 0 },
      { title: '2', key: 1 },
      { title: '3', key: 2 },
      { title: '4', key: 3 },
      { title: '5', key: 4 }
    ];
    const onChange = jest.fn();
    const wrapper = mount(
      <Picker onChange={onChange} className={'test_123'} renderKey="title" value="2" keyName="key" data={dataThree} />
    );
    const map = {};
    const target = wrapper.getDOMNode();
    target.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });
    jest.advanceTimersByTime(1000);
    expect(wrapper.state('index')).toEqual(1);
    wrapper.setProps({
      data: [
        { title: 'test', key: 0 },
        { title: 'ddd', key: 1 },
        { title: 'list', key: 2 },
        { title: 'lisdst', key: 3 }
      ],
      value: 'lisdst'
    });
    jest.advanceTimersByTime(100);
    expect(wrapper.state('index')).toEqual(3);
    const instance = wrapper.find(prefixCls);
    map.touchstart({
      touches: [
        {
          pageY: 10
        }
      ]
    });
    map.touchmove({
      touches: [
        {
          pageY: 10 + 2 * 24
        }
      ]
    });
    map.touchend({});
    jest.advanceTimersByTime(300);
    expect(wrapper.state('index')).toEqual(1);
    expect(onChange).toBeCalledWith({ title: 'ddd', key: 1 });
  });



});
