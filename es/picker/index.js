function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { PureComponent } from 'react';
import classnames from 'classnames';
import Util from '../util';
const prefixCls = `${__PREFIX__}-picker`;
export default class Picker extends PureComponent {
  constructor(props) {
    super(props);
    this.contain = void 0;
    this.list = void 0;
    this.target = void 0;
    this.TouchInfo = void 0;
    this.height = void 0;
    this.hasBind = false;
    this.state = {
      index: 0,
      position: 0
    };
  }

  componentDidMount() {
    this.initEvent();
  }

  componentDidUpdate(prevProps) {
    if (this.props, prevProps !== this.props) this.initEvent();
  }

  initEvent() {
    const {
      contain,
      list
    } = this;
    const {
      start,
      move,
      end,
      option
    } = Util.TOUCH_EVENT_NAME;
    const activeIndex = this.getIndex();
    this.moveTo(activeIndex);
    setTimeout(() => {
      if (this.hasBind) return;
      this.hasBind = true;
      contain.addEventListener(start, e => {
        this.TouchInfo = {};
        let touch = e.touches && e.touches[0];
        if (!touch) touch = e;
        const {
          pageY
        } = touch;
        this.TouchInfo = {
          sY: pageY
        };
        if (this.props.disable) return;
        document.addEventListener(move, touchmove, option);
        document.addEventListener(end, touchend, option);
      }, option);
    }, 0);

    const _this = this;

    function touchmove(e) {
      const {
        data,
        preventDefault
      } = _this.props;
      let touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      const {
        sY
      } = _this.TouchInfo;
      const {
        pageX,
        pageY,
        clientY
      } = touch;
      const inBody = Util.isInClient(pageX, clientY);

      if (!inBody) {
        touchend();
        return;
      }

      if (preventDefault && e.preventDefault) e.preventDefault();
      let my = pageY - sY;
      let slideY;
      slideY = Util.getTranslateInfo(list, 2);
      const max = _this.height * 2;
      const start = -_this.height * 3;
      const last = -(data.length + 2) * _this.height;
      if (slideY > start) my = (start + max - slideY) * my / max;
      if (slideY < last) my = (max + slideY - last) * my / max;
      slideY += my;

      _this.setState({
        position: slideY
      });

      Util.setTranslateInfo(list, {
        y: slideY
      });
      _this.TouchInfo = _objectSpread({}, _this.TouchInfo, {}, {
        sY: pageY
      });
    }

    function touchend() {
      const {
        data,
        onChange
      } = _this.props;
      let slideY;
      slideY = Util.getTranslateInfo(list, 2);
      const start = -_this.height * 3;
      const last = -(data.length + 2) * _this.height;
      slideY = Math.round(slideY / _this.height) * _this.height;
      if (slideY > start) slideY = start;
      if (slideY < last) slideY = last;
      const index = -slideY / _this.height - 3;
      onChange && onChange(data[index]);

      _this.setState({
        index,
        position: slideY
      });

      Util.setTranslateInfo(list, {
        y: slideY,
        time: 150
      }); // @ts-ignore

      document.removeEventListener(move, touchmove, option); // @ts-ignore

      document.removeEventListener(end, touchend, option);
    }
  }

  moveTo(index, time = 0) {
    const {
      list,
      target
    } = this;
    this.height = target.offsetHeight;
    if (Util.IS_IN_JEST) this.height = 24;
    this.setState({
      index,
      position: -this.height * (index + 3)
    });
    index = index + 3;
    Util.setTranslateInfo(list, {
      y: -this.height * index,
      time
    });
  }

  getIndex() {
    const {
      value = '',
      data,
      keyName = '',
      keyValue = '',
      renderKey = ''
    } = this.props;
    let activeIndex = 0;
    data.some((item, index) => {
      if (typeof item === 'object') {
        if (item[keyName] === keyValue || item[renderKey] === value) {
          activeIndex = index;
          return true;
        }
      } else {
        if (item === value) {
          activeIndex = index;
          return true;
        }
      }
    });
    return activeIndex;
  }

  render() {
    const {
      className,
      data,
      keyName,
      renderKey
    } = this.props;
    const {
      position
    } = this.state;
    const cls = classnames(prefixCls, className);
    const contain = classnames(`${prefixCls}__contain`);
    return React.createElement("div", {
      ref: dom => {
        this.contain = dom;
      },
      className: cls
    }, React.createElement("div", {
      className: `${prefixCls}__mask ${prefixCls}__mask--top`
    }), React.createElement("div", {
      ref: dom => {
        this.target = dom;
      },
      className: `${prefixCls}__mid`
    }), React.createElement("div", {
      className: `${prefixCls}__mask ${prefixCls}__mask--bottom`
    }), React.createElement("div", {
      className: contain,
      ref: dom => {
        this.list = dom;
      }
    }, data.map((item, index) => {
      let key = item;

      if (typeof item === 'object') {
        key = item[keyName] != null ? item[keyName] : item[renderKey] ? item[renderKey] : index;
        item = renderKey ? item[renderKey] : '';
      }

      const rotateX = Math.abs(index * this.height + this.height * 3 + position) / this.height;
      let style = {};

      if (rotateX && rotateX <= 2) {
        style = {
          [Util.transform]: `rotateX(${rotateX * 25}deg)`
        };
      }

      return React.createElement("div", {
        key: key,
        style: style
      }, React.createElement("p", null, item));
    })));
  }

}
Picker.defaultProps = {
  renderKey: 'name',
  preventDefault: false,
  disable: false,
  keyName: 'id'
};