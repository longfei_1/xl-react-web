function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { PureComponent } from 'react';
import Base from './Base';
import Text from './Text';
import Number from './Number';
export default class Input extends PureComponent {
  constructor(...args) {
    super(...args);
    this.input = void 0;
  }

  focus() {
    if (this.input) {
      this.input.focus();
    }
  }

  blur() {
    if (this.input) {
      this.input.blur();
    }
  }

  render() {
    const {
      type
    } = this.props;

    switch (type) {
      case 'price':
      case 'number':
      case 'card':
        return React.createElement(Number, _extends({}, this.props, {
          type: type,
          ref: ele => {
            this.input = ele;
          }
        }));

      case 'textarea':
        return React.createElement(Text, _extends({}, this.props, {
          type: type,
          ref: ele => {
            this.input = ele;
          }
        }));

      default:
        return React.createElement(Base, _extends({}, this.props, {
          type: type,
          ref: ele => {
            this.input = ele;
          }
        }));
    }
  }

}
Input.defaultProps = {
  type: 'text'
};