function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { PureComponent } from 'react';
import classnames from 'classnames';
import Icon from "../icon";
const prefixCls = `${__PREFIX__}-input`;
const regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]|\n/g;

const countSymbols = (text = '') => {
  return text.replace(regexAstralSymbols, '_').length;
};

export default class Text extends PureComponent {
  constructor(props) {
    super(props);
    this.input = void 0;

    this.handleComposition = e => {
      const {
        onCompositionStart,
        onCompositionUpdate,
        onCompositionEnd,
        onChange
      } = this.props;

      if (e.type === 'compositionstart') {
        if (typeof onCompositionStart === 'function') {
          onCompositionStart(e);
        }
      }

      if (e.type === 'compositionupdate') {
        if (typeof onCompositionUpdate === 'function') {
          onCompositionUpdate(e);
        }
      }

      if (e.type === 'compositionend') {
        const {
          value
        } = e.target;

        if (typeof onCompositionEnd === 'function') {
          onCompositionEnd(e);
        }

        if (typeof onChange === 'function') {
          onChange(value);
        }
      }
    };

    this.onFocus = e => {
      setTimeout(() => {
        this.setState({
          focused: true
        });
      }, 0);
      const {
        onFocus
      } = this.props;

      if (typeof onFocus === 'function') {
        onFocus(e.target.value, e);
      }
    };

    this.onBlur = e => {
      setTimeout(() => {
        this.setState({
          focused: false
        });
      }, 200);
      const {
        onBlur
      } = this.props;

      if (typeof onBlur === 'function') {
        onBlur(e.target.value, e);
      }
    };

    this.onClear = e => {
      this.input.value = '';
      this.setState({
        length: 0,
        value: ''
      });
      this.input.focus();
      const {
        onClear
      } = this.props;

      if (typeof onClear === 'function') {
        onClear(e.target.value, e);
      }
    };

    this.onChange = e => {
      const {
        onChange
      } = this.props;
      const {
        value
      } = e.target;
      const length = countSymbols(value);
      this.setState({
        length,
        value
      });

      if (typeof onChange === 'function') {
        onChange(value, e);
      }
    };

    this.state = {
      value: props.defaultValue || props.value || '',
      focused: false,
      lineHeight: null
    };
  }

  componentDidMount() {
    const {
      props,
      input
    } = this;
    const {
      autoFocus,
      autoHeight,
      onFocus,
      maxLength,
      rows = 3
    } = props;

    if (autoFocus) {
      input.focus();
      input.setSelectionRange(input.value.length, input.value.length);

      if (typeof onFocus === 'function') {
        onFocus(input.value);
      }
    }

    if (autoHeight) {
      const hang = input.value.match(/\n/g);
      let lineHeight = input.scrollHeight / rows;

      if (hang && hang.length + 1 > rows) {
        lineHeight = input.scrollHeight / (hang.length + 1);
      }

      this.setState({
        lineHeight
      });
      input.style.height = input.scrollHeight + 'px';
    }

    if (maxLength) {
      const length = countSymbols(input.value);
      this.setState({
        length
      });
    }
  }

  componentDidUpdate() {
    const {
      props,
      input,
      state
    } = this;
    const {
      autoHeight,
      rows = 3
    } = props;
    const {
      lineHeight
    } = state;

    if (autoHeight) {
      let hang = input.value.match(/\n/g);
      let row = rows;
      if (hang && hang.length + 1 > rows) row = hang.length + 1;
      input.style.height = row * lineHeight + 'px';
    }
  }

  focus() {
    this.input.focus();
  }

  blur() {
    this.input.blur();
  }

  render() {
    const _this$props = this.props,
          {
      className,
      disabled,
      readOnly,
      onClear,
      autoHeight,
      maxLength,
      clearable
    } = _this$props,
          rest = _objectWithoutProperties(_this$props, ["className", "disabled", "readOnly", "onClear", "autoHeight", "maxLength", "clearable"]);

    const {
      focused,
      value,
      length
    } = this.state;
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--clearable`]: clearable,
      [`${prefixCls}--readonly`]: readOnly
    });
    const IconClass = classnames(`${prefixCls}__clear`, `${prefixCls}__clear--textarea`, {
      [`${prefixCls}__clear--show`]: focused && value && value.length > 0
    });
    return React.createElement("div", {
      className: cls
    }, React.createElement("textarea", _extends({}, rest, {
      ref: ele => {
        this.input = ele;
      },
      maxLength: maxLength,
      disabled: disabled,
      onChange: this.onChange,
      onFocus: this.onFocus,
      onBlur: this.onBlur,
      onCompositionStart: e => {
        this.handleComposition(e);
      },
      onCompositionUpdate: e => {
        this.handleComposition(e);
      },
      onCompositionEnd: e => {
        this.handleComposition(e);
      }
    })), clearable && React.createElement(Icon, {
      type: "wrongRoundFill",
      className: IconClass,
      onClick: e => {
        this.onClear(e);
      }
    }), maxLength && React.createElement("div", {
      className: `${prefixCls}__length`
    }, `${length}/${maxLength}`));
  }

}
Text.defaultProps = {
  type: 'text',
  rows: 3,
  clearable: false,
  readOnly: false,
  disabled: false
};