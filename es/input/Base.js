function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { PureComponent } from 'react';
import classnames from 'classnames';
import Icon from '../icon';
const prefixCls = `${__PREFIX__}-input`;
export default class Base extends PureComponent {
  constructor(props) {
    super(props);
    this.input = void 0;
    this.onBlurTimeout = void 0;
    this.blurFromClear = void 0;

    this.onFocus = e => {
      this.setState({
        focused: true
      });
      const {
        onFocus
      } = this.props;

      if (typeof onFocus === 'function') {
        onFocus(e.target.value);
      }
    };

    this.focus = () => {
      this.input.focus();
    };

    this.blur = () => {
      this.input.blur();
    };

    this.onBlur = e => {
      const {
        onBlur
      } = this.props;
      const {
        value
      } = e.target;
      this.onBlurTimeout = setTimeout(() => {
        if (!this.blurFromClear && document.activeElement !== this.input) {
          this.setState({
            focused: false
          });

          if (typeof onBlur === 'function') {
            onBlur(value);
          }
        }

        this.blurFromClear = false;
      }, 200);
    };

    this.onChange = e => {
      const {
        onChange
      } = this.props;

      if (!this.state.focused) {
        this.setState({
          focused: true
        });
      }

      this.setState({
        value: e.target.value
      });

      if (onChange) {
        onChange(e.target.value);
      }
    };

    this.onClear = () => {
      const {
        isOnComposition
      } = this.state;
      const {
        onChange,
        onClear
      } = this.props;
      this.blurFromClear = true;
      this.setState({
        value: ''
      });
      !isOnComposition && this.focus();
      typeof onChange === 'function' && onChange('');
      typeof onClear === 'function' && onClear('');
    };

    this.handleComposition = e => {
      const {
        onCompositionStart,
        onCompositionUpdate,
        onCompositionEnd,
        onChange
      } = this.props;

      if (e.type === 'compositionstart') {
        this.setState({
          isOnComposition: true
        });

        if (typeof onCompositionStart === 'function') {
          onCompositionStart(e);
        }
      }

      if (e.type === 'compositionupdate') {
        if (typeof onCompositionUpdate === 'function') {
          onCompositionUpdate(e);
        }
      }

      if (e.type === 'compositionend') {
        const {
          value
        } = e.target; // composition is end

        this.setState({
          isOnComposition: false
        });

        if (typeof onCompositionEnd === 'function') {
          onCompositionEnd(e);
        }

        if (typeof onChange === 'function') {
          onChange(value);
        }
      }
    };

    this.state = {
      focused: false,
      isOnComposition: false,
      value: props.defaultValue || props.value || ''
    };
  }

  componentDidMount() {
    const {
      autoFocus,
      onFocus
    } = this.props;
    const {
      focused
    } = this.state;

    if (autoFocus || focused) {
      this.input.focus();

      if (typeof onFocus === 'function') {
        onFocus(this.input.value);
      }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  componentWillUnmount() {
    if (this.onBlurTimeout) {
      clearTimeout(this.onBlurTimeout);
      this.onBlurTimeout = null;
    }
  }

  render() {
    const _this$props = this.props,
          {
      className,
      disabled,
      clearable,
      readOnly,
      type,
      onClear
    } = _this$props,
          rest = _objectWithoutProperties(_this$props, ["className", "disabled", "clearable", "readOnly", "type", "onClear"]);

    const {
      value,
      focused
    } = this.state;
    const showClearIcon = clearable && 'value' in this.props;
    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--focus`]: focused,
      [`${prefixCls}--clearable`]: showClearIcon,
      [`${prefixCls}--readonly`]: readOnly
    });
    const valueProps = {};

    if ('value' in this.props) {
      valueProps.value = value;
    }

    const IconClass = classnames(`${prefixCls}__clear`, {
      [`${prefixCls}__clear--show`]: focused && value && value.length > 0
    });
    return React.createElement("div", {
      className: cls
    }, React.createElement("input", _extends({}, rest, valueProps, {
      autoComplete: "off",
      ref: ref => this.input = ref,
      type: type,
      disabled: disabled,
      onChange: this.onChange,
      onBlur: this.onBlur,
      onFocus: this.onFocus,
      onCompositionStart: e => {
        this.handleComposition(e);
      },
      onCompositionUpdate: e => {
        this.handleComposition(e);
      },
      onCompositionEnd: e => {
        this.handleComposition(e);
      }
    })), React.createElement(Icon, {
      type: "wrongRoundFill",
      className: IconClass,
      onClick: () => this.onClear()
    }));
  }

}
Base.defaultProps = {
  type: 'text',
  clearable: true,
  readOnly: false,
  disabled: false
};