function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Fragment, PureComponent } from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import Util from '../util';
const prefixCls = `${__PREFIX__}-popup`;
export default class Popup extends PureComponent {
  constructor(_props) {
    super(_props);
    this.container = void 0;
    this.main = void 0;
    this.box = void 0;
    this.setProps = void 0;
    this.disableIosBounce = void 0;

    this.show = type => {
      const _this = this;

      const {
        container
      } = this;
      const {
        children,
        direction,
        width,
        height,
        mask,
        open,
        style,
        animationType
      } = this.props;
      let {
        animationDuration
      } = this.props;
      if (animationType === 'none') animationDuration = 1;
      const mainCls = classnames(`${prefixCls}__main`, `${prefixCls}__${direction}`);
      document.body.appendChild(container);
      this.disableIosBounce = Util.disableIosBounce(container);

      class Main extends PureComponent {
        constructor(props) {
          super(props);
          this.mainBox = void 0;
          const {
            animationDuration,
            children,
            mainClass = ''
          } = props;

          _this.setProps = obj => {
            this.setState(obj);
          };

          this.state = {
            animationDuration,
            mainClass,
            children
          };
        }

        componentDidMount() {
          this.setState({
            animationDuration: type ? 0 : animationDuration
          });
          setTimeout(() => {
            this.setState({
              mainClass: `${prefixCls}__${direction}--show`
            });
          });
          open && open();
          document.body.classList.add(`${prefixCls}__ovhide`);
        }

        componentWillUnmount() {
          if (_this.disableIosBounce) _this.disableIosBounce();
        }

        render() {
          const {
            mainClass,
            children
          } = this.state;
          let {
            animationDuration
          } = this.state;
          if (animationType === 'none') animationDuration = 1;
          const {
            onMaskClick
          } = this.props;
          return React.createElement(Fragment, null, React.createElement("div", {
            onClick: () => {
              onMaskClick && onMaskClick();
            },
            style: {
              [Util.transition]: animationType === 'none' ? '' : `opacity ${animationDuration}ms,opacity ${animationDuration}ms`
            },
            className: classnames(`${prefixCls}__mask`, mainClass ? `${prefixCls}__mask--show` : '', {
              [`${prefixCls}__mask--opacity`]: !mask
            })
          }), React.createElement("div", {
            ref: ref => this.mainBox = ref,
            style: _objectSpread({}, {
              [Util.transition]: animationType === 'none' ? '' : `transform ${animationDuration}ms,opacity ${animationDuration}ms`,
              width,
              height
            }, {}, style),
            className: classnames(mainCls, mainClass, {
              [`${prefixCls}__center--${animationType}`]: direction === 'center'
            })
          }, children));
        }

      }

      ReactDOM.render(React.createElement(Main, this.props, children), container);
    };

    this.close = () => {
      const {
        container
      } = this;
      const {
        animationType,
        close
      } = this.props;
      let {
        animationDuration
      } = this.props;
      if (animationType === 'none') animationDuration = 1;
      this.setProps({
        mainClass: ''
      });

      if (container) {
        setTimeout(() => {
          container.parentNode.removeChild(container);
          this.container = null;
          close && close();
          if (this.disableIosBounce) this.disableIosBounce();
          document.body.classList.remove(`${prefixCls}__ovhide`);
        }, animationDuration);
      }
    };
  }

  componentDidMount() {
    this.initEvent(true);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.visible !== this.props.visible) this.initEvent(false);else if (this.props.visible && this.props.children !== prevProps.children) {
      this.setProps({
        children: this.props.children
      });
    }
  }

  componentWillUnmount() {
    if (this.disableIosBounce) this.disableIosBounce();
    this.close();
  }

  initEvent(type) {
    const {
      className,
      visible
    } = this.props;

    if (!this.container) {
      const cls = classnames(prefixCls, className);
      const container = this.container = document.createElement('div');
      container.setAttribute('class', cls);
      visible && this.show(type);
    } else {
      if (visible) this.show(false);else this.close();
    }
  }

  render() {
    return React.createElement("div", null);
  }

}
Popup.defaultProps = {
  direction: 'bottom',
  animationType: 'fade-scale',
  mask: true,
  animationDuration: 250
};