# Popup 弹出框

## 基本用法 (移动端查看，效果会好些)

```jsx
import { Popup, Cell } from 'wxl';

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oneVisible: false,
      twoVisible: false
    };
  }
  switchVisible = attr => {
    this.setState({
      [`${attr}Visible`]: !this.state[`${attr}Visible`]
    });
  };
  render() {
    const { oneVisible, twoVisible } = this.state;
    return (
      <aside>
        <Cell
          onClick={() => {
            this.switchVisible('one');
          }}
          hasArrow
          title="点击弹出"
        />
        <Cell
          onClick={() => {
            this.switchVisible('two');
          }}
          hasArrow
          title="点击背景关闭"
        />
        <Popup visible={oneVisible}>
          <div style={{ height: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('one')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup visible={twoVisible} onMaskClick={() => this.switchVisible('two')}>
          <div style={{ height: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('two')}>关闭弹框</span>
          </div>
        </Popup>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 设置方向

```jsx
import { Popup, Cell } from 'wxl';

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oneVisible: false,
      twoVisible: false,
      threeVisible: false,
      fourVisible: false
    };
  }
  switchVisible = attr => {
    this.setState({
      [`${attr}Visible`]: !this.state[`${attr}Visible`]
    });
  };
  render() {
    const { oneVisible, twoVisible, threeVisible, fourVisible } = this.state;
    return (
      <aside>
        <Cell
          onClick={() => {
            this.switchVisible('one');
          }}
          hasArrow
          title="上面滑出"
        />
        <Cell
          onClick={() => {
            this.switchVisible('two');
          }}
          hasArrow
          title="左侧滑出"
        />
        <Cell
          onClick={() => {
            this.switchVisible('three');
          }}
          hasArrow
          title="右侧滑出"
        />
        <Cell
          onClick={() => {
            this.switchVisible('four');
          }}
          hasArrow
          title="底部滑出"
        />
        <Popup direction="top" visible={oneVisible}>
          <div style={{ height: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('one')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup direction="left" visible={twoVisible} onMaskClick={() => this.switchVisible('two')}>
          <div style={{ width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('two')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup direction="right" visible={threeVisible} onMaskClick={() => this.switchVisible('three')}>
          <div style={{ width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('three')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup visible={fourVisible} onMaskClick={() => this.switchVisible('four')}>
          <div style={{ height: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('four')}>关闭弹框</span>
          </div>
        </Popup>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## 中间弹框（效果）

```jsx
import { Popup, Cell } from 'wxl';

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oneVisible: false,
      twoVisible: false,
      threeVisible: false,
      fourVisible: false,
      fiveVisible: false
    };
  }
  switchVisible = attr => {
    this.setState({
      [`${attr}Visible`]: !this.state[`${attr}Visible`]
    });
  };
  render() {
    const { oneVisible, twoVisible, threeVisible, fourVisible, fiveVisible } = this.state;
    return (
      <aside>
        <Cell
          onClick={() => {
            this.switchVisible('one');
          }}
          hasArrow
          title="宽高不固定"
        />
        <Cell
          onClick={() => {
            this.switchVisible('five');
          }}
          hasArrow
          title="无动效"
        />
        <Cell
          onClick={() => {
            this.switchVisible('four');
          }}
          hasArrow
          title="动效(fade-scale)"
        />
        <Cell
          onClick={() => {
            this.switchVisible('two');
          }}
          hasArrow
          title="动效(fade)"
        />
        <Cell
          onClick={() => {
            this.switchVisible('three');
          }}
          hasArrow
          title="动效(fade-slide)"
        />

        <Popup direction="center" visible={oneVisible}>
          <div style={{ padding: '10px' }}>
            <span onClick={() => this.switchVisible('one')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          animationType="none"
          visible={fiveVisible}
          onMaskClick={() => this.switchVisible('five')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('five')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          animationDuration={1000}
          animationType="fade"
          visible={twoVisible}
          onMaskClick={() => this.switchVisible('two')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('two')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          animationType="fade-slide"
          direction="center"
          visible={threeVisible}
          onMaskClick={() => this.switchVisible('three')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('three')}>关闭弹框</span>
          </div>
        </Popup>
        <Popup
          direction="center"
          visible={fourVisible}
          animationType="fade-scale"
          onMaskClick={() => this.switchVisible('four')}
        >
          <div style={{ height: 250, width: 250, padding: '10px' }}>
            <span onClick={() => this.switchVisible('four')}>关闭弹框</span>
          </div>
        </Popup>
      </aside>
    );
  }
}

ReactDOM.render(<Demo />, mountNode);
```

## API

| 属性              | 类型          | 默认值           | 说明                                                              |
| :---------------- | :------------ | :--------------- | :---------------------------------------------------------------- |
| direction         | string        | bottom           | 方向 left/right/bottom/top/center                                 |
| className         | string        | -                | 类名称                                                            |
| mask              | boolean       | false            | 背景透明                                                          |
| style             | object        | -                | 内部添加样式                                                      |
| animationType     | string        | slide/fade-scale | 动效(剧中时候支持 fade/fade-scale/fade-slide/none，滑出只能 silde |
| animationDuration | number        | 250              | 动效时间                                                          |
| width             | string/number | -                | 内容宽度                                                          |
| height            | string/number | -                | 内容高度                                                          |  |
| close             | -             | -                | 关闭回调函数                                                      |
| open              | -             | -                | 显示回调函数                                                      |
| onMaskClick       | -             | -                | 点击 mask 回调函数                                                |
