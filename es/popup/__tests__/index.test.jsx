import React from 'react';
import { render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Popup from '../index';

const prefixCls = `.${__PREFIX__}-popup`;

describe('Popup', () => {
  // Picker 常规展示
  it('renders  Popup', () => {
    jest.useFakeTimers();
    const info = 'Popup_first';
    const wrapper = mount(
      <Popup visible={true}>
        <div className={'container'}>{info}</div>
      </Popup>
    );
    jest.advanceTimersByTime(300);
    const pop = document.querySelector(prefixCls);
    const container = pop.querySelector('.container');
    const mask = pop.querySelector(`${prefixCls}__mask`);
    const main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__bottom,xl-popup__bottom--show');
    const innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info);
    wrapper.setProps({
      visible: false
    });
  });

  it('visible  Popup', () => {
    jest.useFakeTimers();
    const info = 'Popup_Two';
    const wrapper = mount(
      <Popup visible={false} direction="top">
        <div className={'container'}>{info}</div>
      </Popup>
    );
    jest.advanceTimersByTime(300);
    wrapper.setProps({
      visible: true
    });
    jest.advanceTimersByTime(300);
    let pop = document.querySelector(prefixCls);
    const container = pop.querySelector('.container');
    const mask = pop.querySelector(`${prefixCls}__mask`);
    const main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__top,xl-popup__top--show');
    const innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info);
    wrapper.setProps({
      visible: false
    });
    jest.advanceTimersByTime(300);
    pop = document.querySelector(prefixCls);
    expect(pop).toBeNull();
  });

  it('event  Popup', () => {
    jest.useFakeTimers();
    const info = 'Popup_Two';
    const open = jest.fn();
    const close = jest.fn();
    const onMaskClick = jest.fn();
    const wrapper = mount(
      <Popup onMaskClick={onMaskClick} close={close} open={open} visible={false} direction="right">
        <div className={'container'}>{info}</div>
      </Popup>
    );
    jest.advanceTimersByTime(300);
    wrapper.setProps({
      visible: true
    });
    jest.advanceTimersByTime(300);
    expect(open).toBeCalled();
    let pop = document.querySelector(prefixCls);
    const container = pop.querySelector('.container');
    const mask = pop.querySelector(`${prefixCls}__mask`);
    const main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__right,xl-popup__right--show');
    const innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info);
    mask.click()
    expect(onMaskClick).toBeCalled();
    wrapper.setProps({
      visible: false
    });
    jest.advanceTimersByTime(300);
    expect(close).toBeCalled();
    pop = document.querySelector(prefixCls);
    expect(pop).toBeNull();
  });

  it('change children  Popup', () => {
    jest.useFakeTimers();
    const info = 'Popup_Two';
    const info2 = 'Popup_Three';
    const open = jest.fn();
    const close = jest.fn();
    const onMaskClick = jest.fn();
    const wrapper = mount(
      <Popup onMaskClick={onMaskClick} close={close} open={open} visible={false} direction="left">
        <div className={'container'}>{info}</div>
      </Popup>
    );
    jest.advanceTimersByTime(300);
    wrapper.setProps({
      visible: true
    });
    jest.advanceTimersByTime(300);
    expect(open).toBeCalled();
    let pop = document.querySelector(prefixCls);
    let container = pop.querySelector('.container');
    let mask = pop.querySelector(`${prefixCls}__mask`);
    let main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__left,xl-popup__left--show');
    let innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info);
    wrapper.setProps({
      children: <div className={'container'}>{info2}</div>
    });
    container = pop.querySelector('.container');
    innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info2);
    mask.click()
    expect(onMaskClick).toBeCalled();
    wrapper.setProps({
      visible: false
    });
    jest.advanceTimersByTime(300);
    expect(close).toBeCalled();
    pop = document.querySelector(prefixCls);
    expect(pop).toBeNull();
    wrapper.setProps({
      visible: true
    });
    jest.advanceTimersByTime(300);
    expect(open).toBeCalled();
    pop = document.querySelector(prefixCls);
    container = pop.querySelector('.container');
    mask = pop.querySelector(`${prefixCls}__mask`);
    main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__left,xl-popup__left--show');
    innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info2);
    wrapper.setProps({
      visible: false
    });
  });

  it('center  Popup', () => {
    jest.useFakeTimers();
    const info = 'Popup_Two';
    const open = jest.fn();
    const close = jest.fn();
    const onMaskClick = jest.fn();
    const wrapper = mount(
      <Popup onMaskClick={onMaskClick} close={close} open={open} visible={false} direction="center">
        <div className={'container'}>{info}</div>
      </Popup>
    );
    jest.advanceTimersByTime(300);
    wrapper.setProps({
      visible: true
    });
    jest.advanceTimersByTime(300);
    expect(open).toBeCalled();
    let pop = document.querySelector(prefixCls);
    const container = pop.querySelector('.container');
    const mask = pop.querySelector(`${prefixCls}__mask`);
    const main = pop.querySelector(`${prefixCls}__main`);
    expect(Array.from(mask.classList).join()).toEqual('xl-popup__mask,xl-popup__mask--show');
    expect(Array.from(main.classList).join()).toEqual('xl-popup__main,xl-popup__center,xl-popup__center--show,xl-popup__center--fade-scale');
    const innerHTML = container.innerHTML;
    expect(innerHTML).toEqual(info);
    mask.click()
    expect(onMaskClick).toBeCalled();
    wrapper.setProps({
      visible: false
    });
    jest.advanceTimersByTime(300);
    expect(close).toBeCalled();
    pop = document.querySelector(prefixCls);
    expect(pop).toBeNull();
  });

});
