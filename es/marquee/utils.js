export const getHorizontal = (distance, key) => {
  return `
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(${distance}px, 0, 0);
      transform: translate3d(${distance}px, 0, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(${distance}px, 0, 0);
      transform: translate3d(${distance}px, 0, 0);
    }
  }`;
};
export const getVertical = (distance, key) => {
  return `
  @-webkit-keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, ${distance}px, 0);
      transform: translate3d(0, ${distance}px, 0);
    }
  }
  @keyframes ${key} {
    100% {
      -webkit-transform: translate3d(0, ${distance}px, 0);
      transform: translate3d(0, ${distance}px, 0);
    }
  }`;
};
export const setAnimation = (duration, loop, delay, key) => {
  const infinite = loop ? 'infinite' : 1;
  return `${duration}ms ${key} ${delay}ms linear ${infinite}`;
};