import React, { PureComponent } from 'react';
import classnames from 'classnames';
import { getVertical, getHorizontal, setAnimation } from './utils';
import Util from '../util';
const prefixCls = `${__PREFIX__}-marquee`;
let count = 0;

const getCount = () => {
  const time = new Date().getTime();
  count += 1;
  if (count > 50) count = 0;
  return `${time}${count}`;
};

export default class Marquee extends PureComponent {
  constructor(props) {
    super(props);
    this.marqueeBox = void 0;
    this.uuid = void 0;
    this.loops = void 0;
    this.loops2 = void 0;
    this.inter = void 0;
    this.uuid = `${prefixCls}--key-${getCount()}`;
  }

  componentDidMount() {
    const {
      delay,
      direction
    } = this.props;
    if (direction === 'left' || direction === 'right') this.run(delay);else this.runVertical(delay);
  }

  componentDidUpdate() {
    const {
      direction
    } = this.props;
    if (direction === 'left' || direction === 'right') this.run(50);else this.runVertical(50);
  }

  run(delay) {
    const {
      marqueeBox,
      uuid
    } = this;
    const {
      speed,
      loop,
      direction
    } = this.props;
    const headDom = document.querySelector('head');
    const styleDom = document.querySelector(`#${uuid}`);
    const boxWidth = marqueeBox.offsetWidth;
    const pWidth = marqueeBox.parentNode.offsetWidth;
    if (styleDom) styleDom.parentNode.removeChild(styleDom);
    this.uuid = `${prefixCls}--key-${getCount()}`;
    const styleLine = document.createElement('style');
    styleLine.setAttribute('id', this.uuid);
    styleLine.innerHTML = getHorizontal(-boxWidth, this.uuid);
    let start = 0;
    let end = pWidth;
    const duration = boxWidth * 1000 / speed;
    let animation = setAnimation(duration, loop, 0, this.uuid);

    if (direction === 'right') {
      start = pWidth - boxWidth;
      animation = setAnimation(duration, loop, 0, this.uuid);
      styleLine.innerHTML = getHorizontal(pWidth, this.uuid);
      end = -boxWidth;
    }

    Util.setTranslateInfo(marqueeBox, {
      x: start
    });
    marqueeBox.style.opacity = 1;
    if (this.loops) clearTimeout(this.loops);
    if (this.loops2) clearTimeout(this.loops2);
    if (pWidth > boxWidth + 1) return;
    headDom.appendChild(styleLine);
    this.loops = setTimeout(() => {
      marqueeBox.style.animation = animation;
      marqueeBox.style.webkitTransform = animation;

      if (loop) {
        this.loops2 = setTimeout(() => {
          Util.setTranslateInfo(marqueeBox, {
            x: end
          });
        }, duration - 50);
      }
    }, delay);
  }

  slide(boxWidth, pWidth, marqueeBox) {
    let {
      speed,
      loop,
      direction
    } = this.props;
    if (typeof loop === 'boolean') loop = 3000;
    if (speed < 300) speed = 300;
    let start = 0;

    if (direction === 'down') {
      start = -boxWidth;
    }

    Util.setTranslateInfo(marqueeBox, {
      y: start
    });
    if (this.inter) clearInterval(this.inter);
    if (this.loops2) clearTimeout(this.loops2);
    this.inter = setInterval(() => {
      let y = Util.getTranslateInfo(marqueeBox, 2);
      if (direction === 'up') y = y - pWidth;else y = y + pWidth;
      Util.setTranslateInfo(marqueeBox, {
        y: y,
        time: speed
      });

      if ((y <= -boxWidth && direction === 'up' || y >= 0 && direction === 'down') && loop) {
        this.loops2 = setTimeout(() => {
          Util.setTranslateInfo(marqueeBox, {
            y: start,
            time: 0
          });
        }, speed + 50);
      }
    }, loop);
  }

  runVertical(delay) {
    const {
      marqueeBox,
      uuid
    } = this;
    const {
      speed,
      loop,
      direction,
      type
    } = this.props;
    const headDom = document.querySelector('head');
    const styleDom = document.querySelector(`#${uuid}`);
    const boxWidth = marqueeBox.offsetHeight / 2;
    const pWidth = marqueeBox.parentNode.offsetHeight;

    if (type === 'slide') {
      marqueeBox.style.opacity = 1;
      this.slide(boxWidth, pWidth, marqueeBox);
      return;
    }

    if (styleDom) styleDom.parentNode.removeChild(styleDom);
    this.uuid = `${prefixCls}--key-${getCount()}`;
    const styleLine = document.createElement('style');
    styleLine.setAttribute('id', this.uuid);
    styleLine.innerHTML = getVertical(-boxWidth, this.uuid);
    let start = 0;
    const duration = boxWidth * 1000 / speed;
    let animation = setAnimation(duration, loop, 0, this.uuid);

    if (direction === 'down') {
      start = -boxWidth - pWidth;
      animation = setAnimation(duration, loop, 0, this.uuid);
      styleLine.innerHTML = getVertical(-pWidth, this.uuid);
    }

    Util.setTranslateInfo(marqueeBox, {
      y: start
    });
    marqueeBox.style.opacity = 1;
    if (this.loops) clearTimeout(this.loops);
    if (this.loops2) clearTimeout(this.loops2);
    if (pWidth > boxWidth + 1) return;
    headDom.appendChild(styleLine);
    this.loops = setTimeout(() => {
      marqueeBox.style.animation = animation;
      marqueeBox.style.webkitTransform = animation;
    }, delay);
  }

  render() {
    const {
      children,
      className,
      style,
      direction,
      onClick,
      hover
    } = this.props;
    const cls = classnames(prefixCls, className, {});
    const clsBox = classnames(`${prefixCls}__box`, `${prefixCls}__box--${direction}`, {
      [`${prefixCls}__box--hover`]: hover
    });
    return React.createElement("div", {
      className: cls,
      style: style,
      onClick: onClick
    }, React.createElement("div", {
      className: clsBox,
      ref: ref => this.marqueeBox = ref
    }, children, (direction === 'up' || direction === 'down') && children));
  }

}
Marquee.defaultProps = {
  direction: 'left',
  type: 'scroll',
  speed: 30,
  delay: 100,
  loop: true,
  hover: false
};