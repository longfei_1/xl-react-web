function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { PureComponent } from 'react';
import classnames from 'classnames';
import Icon from '../icon';
import Util from '../util';
const prefixCls = `${__PREFIX__}-cell`;
export default class Cell extends PureComponent {
  constructor(...args) {
    super(...args);
    this.inner = void 0;
    this.extra = void 0;
    this.MOVE_ING = void 0;
    this.TouchInfo = void 0;

    this.start = e => {
      if (!this.props.option) return;
      this.TouchInfo = {};
      let touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      const {
        pageX,
        pageY
      } = touch;
      const maxMove = this.extra.offsetWidth;
      this.TouchInfo = {
        sX: pageX,
        maxMove,
        move: null,
        sY: pageY
      };
      this.MOVE_ING = true;
    };

    this.move = e => {
      if (!this.props.option || !this.MOVE_ING) return;
      let touch = e.touches && e.touches[0];
      if (!touch) touch = e;
      const {
        sX,
        sY,
        maxMove,
        move
      } = this.TouchInfo;
      const {
        pageX,
        pageY
      } = touch;
      let mx = pageX - sX;
      let my = pageY - sY;
      let slideX;

      if (Math.abs(mx) > Math.abs(my) && move === null || move) {
        this.TouchInfo.move = true;
        slideX = Util.getTranslateInfo(this.inner, 1);
        slideX = slideX + mx;
        if (slideX < -maxMove) slideX = -maxMove;else if (slideX > 0) slideX = 0;
        Util.setTranslateInfo(this.inner, {
          x: slideX
        });
        document.body.classList.add(`${prefixCls}__ovhide`);
      } else {
        this.TouchInfo.move = false;
      }

      this.TouchInfo = _objectSpread({}, this.TouchInfo, {}, {
        sX: pageX,
        sY: pageY
      });
    };

    this.end = () => {
      if (!this.props.option) return;
      document.body.classList.remove(`${prefixCls}__ovhide`);
      const {
        maxMove
      } = this.TouchInfo;
      let slideX = Util.getTranslateInfo(this.inner, 1);
      if (slideX < -maxMove / 2) slideX = -maxMove;else slideX = 0;
      this.TouchInfo.move = null;

      if (slideX != 0) {
        const resetDom = e => {
          if (!Util.checkInParent(e.target, this.inner)) {
            Util.setTranslateInfo(this.inner, {
              x: 0,
              time: 150
            });
          }

          document.body.removeEventListener('touchstart', resetDom);
        };

        document.body.addEventListener('touchstart', resetDom);
      }

      Util.setTranslateInfo(this.inner, {
        x: slideX,
        time: 150
      });
      this.MOVE_ING = false;
    };
  }

  render() {
    const _this$props = this.props,
          {
      children,
      className,
      hasArrow,
      icon,
      title,
      headerNoBorder,
      footerNoBorder,
      autoHeight,
      disabled,
      onClick,
      option
    } = _this$props,
          rest = _objectWithoutProperties(_this$props, ["children", "className", "hasArrow", "icon", "title", "headerNoBorder", "footerNoBorder", "autoHeight", "disabled", "onClick", "option"]);

    const cls = classnames(prefixCls, className, {
      [`${prefixCls}--disabled`]: disabled,
      [`${prefixCls}--link`]: !disabled && !!onClick,
      [`${prefixCls}--arrow`]: hasArrow,
      [`${prefixCls}--hnb`]: !headerNoBorder,
      [`${prefixCls}--f-n-b`]: footerNoBorder
    });
    const titleCls = classnames(`${prefixCls}__title`, {
      [`${prefixCls}__title--label`]: !!children
    });
    const headerCls = classnames(`${prefixCls}__header`, {
      [`${prefixCls}__header--nb`]: !headerNoBorder
    });
    const footerCls = classnames(`${prefixCls}__footer`, {
      [`${prefixCls}__footer--nb`]: footerNoBorder
    });
    const innerCls = classnames(`${prefixCls}__inner`, {
      [`${prefixCls}__inner--autoHeight`]: autoHeight
    });
    const iconRender = icon && React.createElement("div", {
      className: `${prefixCls}__icon`
    }, icon);
    const titleRender = title && React.createElement("div", {
      className: titleCls
    }, title);
    const contentRender = children && React.createElement("div", {
      className: `${prefixCls}__content`
    }, children);
    const arrowRender = hasArrow && React.createElement(Icon, {
      type: 'arrowDownOneLeft'
    });
    return React.createElement("div", _extends({
      className: cls,
      onClick: onClick
    }, rest), React.createElement("div", {
      className: innerCls,
      ref: dom => {
        this.inner = dom;
      },
      onTouchStart: this.start,
      onMouseDown: this.start,
      onTouchMove: this.move,
      onMouseMove: this.move,
      onTouchEnd: this.end,
      onMouseUp: this.end
    }, React.createElement("div", {
      className: headerCls
    }, iconRender), React.createElement("div", {
      className: `${prefixCls}__body`
    }, titleRender, contentRender), React.createElement("div", {
      className: footerCls
    }, arrowRender), option && React.createElement("div", {
      ref: ref => this.extra = ref,
      className: `${prefixCls}__option`
    }, option)));
  }

}
Cell.defaultProps = {
  hasArrow: false,
  headerNoBorder: false,
  footerNoBorder: false,
  autoHeight: false,
  option: null,
  disabled: false
};