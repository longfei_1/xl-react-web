import React, { lazy, PureComponent, Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import marked from 'marked';
import components from '@site/site.config';
import Format from '@site/utils/format';
import Demo from './demo';
import './style';

let docModule;

function getModule(name, list) {
  list = [].concat(list);
  list.forEach(item => {
    if (item.name && Format.camel2Dash(item.name) === name) {
      docModule = item.module;
    } else if (item.list) {
      getModule(name, item.list);
    }
  });
  return docModule;
}

class Page extends PureComponent {
  constructor(props) {
    super(props);
    this.components = new Map();
    this.nodeList = [];
    this.state = {
      htmlDoc: null
    };
  }

  componentDidMount() {
    this.getDoc();
  }

  componentDidUpdate(prevProps, prevState) {
    const { match } = prevProps;
    const { component } = match.params;
    if (component !== this.props.match.params.component) {
      this.getDoc();
    }
    this.renderDOM();
  }

  renderDOM() {
    for (const [id, component] of this.components) {
      const div = document.getElementById(id);
      this.nodeList.push(div);
      if (div instanceof HTMLElement) {
        ReactDOM.render(component, div);
      }
    }
  }

  getDoc() {
    const { match } = this.props;
    const { component } = match.params;
    const module = getModule(component, components);
    require(`@/src/components/${component}/style`)
    module().then(res => {
      let document = res.default;
      this.components.clear();
      document = document
        .replace(/## 自定义 Iconfont 图标\s?([^]+)/g, '') // 排除无法展示示例的情况
        .replace(/## API\s?([^]+)/g, '') // 排除API显示
        .replace(/##\s?([^]+?)((?=##)|$)/g, (match, p1) => {
          const id = parseInt(Math.random() * 1e9, 10).toString(36);
          this.components.set(id, React.createElement(Demo, this.props, p1));
          return `<div id=${id}></div>`;
        });
      const htmlDoc = marked(document, {
        renderer: new marked.Renderer()
      });
      this.setState({
        htmlDoc
      });
    });
  }

  render() {
    const { htmlDoc } = this.state;
    if (!htmlDoc) {
      return <span />;
    }
    return (
      <div className="components-page-phone">
        <main dangerouslySetInnerHTML={{ __html: htmlDoc }} />
      </div>
    );
  }
}

export default withRouter(Page);
