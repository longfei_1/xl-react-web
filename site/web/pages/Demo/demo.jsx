import React, { lazy, PureComponent, Suspense } from 'react';
import * as wxl from '@/src/components';
import ReactDOM from 'react-dom';
import { transform } from '@babel/standalone';
import classnames from 'classnames';


class Page extends React.Component {
  constructor(props) {
    super(props);
    this.containerId = `${parseInt(Math.random() * 1e9, 10).toString(36)}`;
    this.document = props.children.match(/([^]*)\n?(```[^]+```)/);
    this.title = String(this.document[1]);
    this.source = this.document[2].match(/```(.*)\n?([^]+)```/);
  }

  componentDidMount() {
    this.renderSource(this.source[2]);
  }

  componentWillUnmount() {
    if (this.containerElem) {
      ReactDOM.unmountComponentAtNode(this.containerElem);
    }
  }

  renderSource(value) {
    const args = ['context', 'React', 'ReactDOM', 'wxl'];
    const argv = [this, React, ReactDOM, wxl];
    value = value.replace(/import\s+\{\s*(.*)\s*\}\s+from\s+'wxl';/, 'const { $1 } = wxl;').replace(
      /ReactDOM.render\(\s?([^]+?)(,\s?mountNode\s?\))/g,
      `
          ReactDOM.render(
           $1,
            document.getElementById('${this.containerId}'),
          )
        `
    );
    const { code } = transform(value, {
      presets: ['es2015', 'react'],
      plugins: ['proposal-class-properties']
    });
    args.push(code);
    new Function(...args)(...argv);
    this.source[2] = value;
  }

  render() {
    return (
      <div>
        <h2>{this.title}</h2>
        <div
          id={this.containerId}
          ref={elem => {
            this.containerElem = elem;
          }}
        />
      </div>
    );
  }
}

export default Page;
