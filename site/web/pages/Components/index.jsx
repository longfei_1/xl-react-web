import React, { lazy, PureComponent, Suspense } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import components from '@site/site.config';
import Header from '@site/web/components/Header';
import SideBar from '@site/web/components/SideBar';
import Markdown from '@site/web/components/Markdown';
import './style.scss';

const isComponentPage = page => ['quick-start', 'change-log'].indexOf(page) === -1;

class Page extends PureComponent {
  render() {
    const { match } = this.props;
    const containerCls = classnames('main-container', {
      'no-simulator': !isComponentPage(match.params.component)
    });

    return (
      <div className="components-page">
        <Header />
        <main>
          <SideBar />
          {isComponentPage(match.params.component) && (
            <div className="simulator">
              <iframe src={`${window.location.href.replace('components', 'demo')}`} title="simulator" frameBorder="0" />
            </div>
          )}
          <div className={containerCls}>
            <Markdown {...this.props} />
          </div>
        </main>
      </div>
    );
  }
}

export default withRouter(Page);
