import React, { Component } from 'react';
import classnames from 'classnames';
import Format from '@site/utils/format';
import components from '@site/site.config';
import marked from 'marked';
import hljs from 'highlight.js';
import 'highlight.js/styles/github-gist.css';
import './style.scss';

let docModule;
function getModule(name, list) {
  list = [].concat(list);
  list.forEach(item => {
    // if (item.name) console.log(Format.camel2Dash(item.name), name);
    if (item.name && Format.camel2Dash(item.name) === name) {
      docModule = item.module;
    } else if (item.list) {
      getModule(name, item.list);
    }
  });
  return docModule;
}

export default class Markdown extends Component {
  constructor() {
    super();
    this.state = {
      docHtml: null
    };
  }

  componentDidMount() {
    this.getDoc();
  }

  componentDidUpdate(prevProps, prevState) {
    const { match } = prevProps;
    const { component } = match.params;
    if (component !== this.props.match.params.component) {
      this.getDoc();
    }
  }

  getDoc() {
    const { match } = this.props;
    const { component } = match.params;
    const module = getModule(component, components);
    module().then(res => {
      const document = res.default;
      const renderer = new marked.Renderer();
      renderer.table = (header, body) => {
        return `<div class="grid-container"><table class="grid"><thead>${header}</thead><tbody>${body}</tbody></table></div>`;
      };
      renderer.code = (code, language) => {
        const validLang = !!(language && hljs.getLanguage(language));
        const highlighted = validLang ? hljs.highlight(language, code).value : code;
        return `<pre><code class="hljs ${language}">${highlighted}</code></pre>`;
      };
      const docHtml = marked(document, { renderer });
      this.setState({
        docHtml
      });
    });
  }

  render() {
    const { docHtml } = this.state;
    const { className } = this.props;
    if (!docHtml) {
      return <span />;
    }
    return <div className={classnames(className, 'markdown')} dangerouslySetInnerHTML={{ __html: docHtml }} />;
  }
}
