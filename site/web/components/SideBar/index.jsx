import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import components from '@site/site.config';
import { Icon } from 'wxl';
import Format from '@site/utils/format';
import './style.scss';

class SideBar extends PureComponent {
  constructor(props) {
    super(props);
    const { match } = props;
    clearList(components,match.params.component)
    this.state = {
      components
    };
  }

  render() {
    const { match ,history} = this.props;
    const { components = {} } = this.state;
    return (
      <div className="side-bar">
        <div className="menu">
          {renderMenu(components, item => {
            if (item.list) {
              item.isShow = !item.isShow;
            } else {
              clearList(components);
              item.select = true;
              history.push(`/components/${Format.camel2Dash(item.name)}`)
            }
            this.setState({ components: [].concat(components) });
          })}
        </div>
      </div>
    );
  }
}

function clearList(list, name) {
  list.forEach(item => {
    if (item.list) clearList(item.list,name);
    if (item.select) item.select = false;
    if (item.name && Format.camel2Dash(item.name) === name) item.select = true;
  });
}

function renderMenu(list, click, paddingLeft) {
  if (!paddingLeft) paddingLeft = 24;

  return list.map(item => {
    return (
      <div className="menu-item-child" key={item.description}>
        <p
          className={classnames('menu-item', {
            'has-com': item.list,
            'menu-item-show': item.list && !item.isShow,
            'menu-item-select': item.select
          })}
          style={{ paddingLeft: `${paddingLeft}px` }}
          onClick={() => {
            click(item);
          }}
        >
          {item.list ? (
            <span>{item.description}</span>
          ) : (
            <span>
              {item.name}&nbsp;{item.description}
            </span>
          )}
          {item.list && <Icon type="arrowDownOne" />}
        </p>
        {item.list && !item.isShow && renderMenu(item.list, click, paddingLeft + 12)}
      </div>
    );
  });
}

export default withRouter(SideBar);
