import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
// import Loadable from 'react-loadable';
import './style.scss';


class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={require('@site/web/pages/Index').default} />
        <Route path="/components/:component" component={require('@site/web/pages/Components').default} />
        <Route path="/design/:page" component={require('@site/web/pages/Design').default} />
        <Route path="*" component={require('@site/web/pages/NotFoundPage').default} />
      </Switch>
    );
  }
}

export default withRouter(App);
