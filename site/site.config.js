module.exports = [
  {
    name: 'QuickStart',
    description: '快速上手',
    md: true,
    module: () => import('@/README.md')
  },
  {
    name: 'ChangeLog',
    md: true,
    description: '更新日志',
    module: () => import('@/CHANGELOG.md')
  },
  {
    description: '组件',
    list: [
      {
        description: '表单数据',
        list: [
          {
            name: 'Input',
            description: '文本框',
            module: () => import('@com/input/demo.md')
          }
        ]
      },
      {
        description: '操作反馈',
        list: [
          {
            name: 'Picker',
            description: '滑动选择器',
            module: () => import('@com/picker/demo.md')
          },
          {
            name: 'Popup',
            description: '弹出框',
            module: () => import('@com/popup/demo.md')
          },
        ]
      },
      {
        description: '数据展示',
        list: [
          {
            name: 'Cell',
            description: '列表项',
            module: () => import('@com/cell/demo.md')
          },
          {
            name: 'Icon',
            description: '图标',
            module: () => import('@com/icon/demo.md')
          },
          {
            name: 'Marquee',
            description: '跑马灯',
            module: () => import('@com/marquee/demo.md')
          },
          {
            name: 'Swiper',
            description: '轮播图',
            module: () => import('@com/swiper/demo.md')
          }
        ]
      },
      {
        description: '其他',
        list: [
          {
            name: 'Gif',
            description: '帧动画',
            module: () => import('@com/gif/demo.md')
          },
        ]
      }
    ]
  }
];
