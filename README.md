
<h1 align="center">Wxl</h1>

## Version 版本

- 稳定版：
- 开发版：


## Install 安装


使用npm安装：
```bash
npm install wxl --save
```

或者通过cdn引入umd模块：
```html
<link rel="stylesheet" href="https://test.com/wxl.css">
<script type="text/javascript" src="https://test.com/wxl.min.js"></script>
```

## Usage 使用

### 全组件引入

```js
import { Button, Cell } from 'wxl';
import 'wxl/dist/wxl.min.css';
```

### 按需加载

- 方法一（推荐）

> 使用 [babel-plugin-import](https://github.com/ant-design/babel-plugin-import) 自动加载Sass文件

```js
  // .babelrc or babel-loader option
  {
    "plugins": [
      ['import', {
        libraryName: 'wxl',
        style: true,
      }],
    ]
  }
```

```js
import { Button, Cell } from 'wxl';
```

- 方法二：

```js
import Button from 'wxl/lib/Button';
import 'wxl/lib/Button/style';
```

### 定制主题

通过修改css变量定义达到定制主题的效果

```js
document.documentElement.style.setProperty('--theme-primary', '#108ee9');
```

变量名可参考 [default.scss](https://gitee.com/longfei_1/xl-react-web/blob/master/src/components/style/default.scss)


## Changelog 更新日志

[CHANGELOG.md](https://gitee.com/longfei_1/xl-react-web/blob/master/CHANGELOG.md)

## License

MIT
